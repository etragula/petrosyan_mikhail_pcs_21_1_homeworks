package specs;

import com.github.tomakehurst.wiremock.client.WireMock;
import env.core.Environment;
import env.wiremock.WiremockSystem;
import io.github.adven27.concordion.extensions.exam.core.AbstractSpecs;
import io.github.adven27.concordion.extensions.exam.core.ExamExtension;
import io.github.adven27.concordion.extensions.exam.db.DbPlugin;
import io.github.adven27.concordion.extensions.exam.ws.WsPlugin;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import ru.pcs.exam.ExamBankSpringApiApplication;

import java.util.Collections;

@Slf4j
public class Specs extends AbstractSpecs {

    public static final TestEnvironment ENV;

    private static ConfigurableApplicationContext SUT;


    static {
        ENV = new TestEnvironment();
        ENV.up();
    }


    @Override
    protected ExamExtension init() {
        final String property = System.getProperty("env.test.db.port");
        if (property == null) {
            System.setProperty("env.test.db.port", "5433");
        }
        return new ExamExtension(
                new DbPlugin(
                        "org.postgresql.Driver",
                        "jdbc:postgresql://localhost:5433/postgres",
                        "a19188759",
                        "Petrosyan7998"
                ),
                new WsPlugin(8081)
        );
    }

    @Override
    protected void startSut() {
        SpringApplication app = new SpringApplication(ExamBankSpringApiApplication.class);
        SUT = app.run();
    }

    @Override
    protected void stopSut() {
        SUT.stop();
    }

    public static class TestEnvironment extends Environment {

        private WireMock wireMock;

        public TestEnvironment() {
            super(Collections.singletonMap(
                    "stubs", new WiremockSystem(wms -> null)
            ));

        }

        public WireMock ses() {
            if (wireMock == null) {
                wireMock = new WireMock(Integer.parseInt(System.getProperty("env.wiremock.port")));
            }
            return wireMock;
        }
    }
}
