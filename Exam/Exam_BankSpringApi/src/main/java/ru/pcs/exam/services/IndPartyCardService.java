package ru.pcs.exam.services;

import ru.pcs.exam.dtos.IndPartyCardDto;

public interface IndPartyCardService {

    IndPartyCardDto createCard(String accountNum);

    IndPartyCardDto confirmCard(String cardNum);
}
