package ru.pcs.exam.erros;


public class IndPartyCardNotFoundException extends RuntimeException {
    public IndPartyCardNotFoundException(String number) {
        super("Card with number - " + number + " was not found!");
    }
}
