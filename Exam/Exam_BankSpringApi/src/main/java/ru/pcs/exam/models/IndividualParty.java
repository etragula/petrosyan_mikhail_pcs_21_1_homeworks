package ru.pcs.exam.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "accounts")
@EqualsAndHashCode(exclude = {"accounts"})
public class IndividualParty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    private String phone;

    private String password;

    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToMany(mappedBy = "individualParty")
    private List<IndividualPartyAccount> accounts;

    public enum Role {
        USER, ADMIN
    }
}
