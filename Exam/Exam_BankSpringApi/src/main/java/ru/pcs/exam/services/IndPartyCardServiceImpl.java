package ru.pcs.exam.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.pcs.exam.dtos.IndPartyCardDto;
import ru.pcs.exam.erros.IndPartyAccountNotFoundException;
import ru.pcs.exam.erros.IndPartyCardNotFoundException;
import ru.pcs.exam.models.IndividualParty;
import ru.pcs.exam.models.IndividualPartyAccount;
import ru.pcs.exam.models.IndividualPartyCard;
import ru.pcs.exam.repositories.IndividualPartyCardRepository;
import ru.pcs.exam.repositories.IndividualPartyRepository;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class IndPartyCardServiceImpl implements IndPartyCardService {

    private final IndividualPartyCardRepository cardRepository;
    private final IndividualPartyRepository partyRepository;

    @Override
    public IndPartyCardDto createCard(String accountNum) {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == null) throw new AuthorizationServiceException("Authorization problem, try again!");
        final Long indPartyId = Long.valueOf((String) principal);

        final IndividualParty individualParty = partyRepository.findById(indPartyId).orElseThrow(
                () -> new AuthorizationServiceException("Authorization problem, try again!"));

        final IndividualPartyAccount individualPartyAccount = getExactAccount(individualParty, accountNum);
        final IndividualPartyCard individualPartyCard;
        if (individualPartyAccount != null) {
            individualPartyCard = IndividualPartyCard.builder()
                    .cardStatus(IndividualPartyCard.CardStatus.NOT_CONFIRMED)
                    .cardNumber(UUID.randomUUID().toString())
                    .account(individualPartyAccount)
                    .build();
            cardRepository.save(individualPartyCard);
        } else throw new IndPartyAccountNotFoundException(accountNum + " has card or");
        log.info("Card was created for account  - " + accountNum);
        return IndPartyCardDto.from(individualPartyCard);
    }

    @Override
    public IndPartyCardDto confirmCard(String cardNum) {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == null) throw new AuthorizationServiceException("Authorization problem, try again!");
        final Long indPartyId = Long.valueOf((String) principal);

        final IndividualParty individualParty = partyRepository.findById(indPartyId).orElseThrow(
                () -> new AuthorizationServiceException("Authorization problem, try again!"));
        final IndividualPartyCard partyCard;
        if (doesCardExist(individualParty.getAccounts(), cardNum)) {
            partyCard = cardRepository.getIndividualPartyCardByCardNumber(cardNum);
            partyCard.setCardStatus(IndividualPartyCard.CardStatus.ACTIVE);
            cardRepository.save(partyCard);
        } else throw new IndPartyCardNotFoundException(cardNum);
        log.info("Card was confirmed for account");
        return IndPartyCardDto.from(partyCard);
    }

    private boolean doesCardExist(List<IndividualPartyAccount> accounts, String cardNum) {
        for (IndividualPartyAccount account : accounts) {
            if (!account.getCards().isEmpty() && account.getCards().get(0).getCardNumber().equals(cardNum)) {
                return true;
            }
        }
        return false;
    }

    private IndividualPartyAccount getExactAccount(IndividualParty individualParty, String accountNum) {
        for (IndividualPartyAccount account : individualParty.getAccounts()) {
            if (account.getNumber().equals(accountNum) && account.getCards().isEmpty()) return account;
        }
        return null;
    }
}
