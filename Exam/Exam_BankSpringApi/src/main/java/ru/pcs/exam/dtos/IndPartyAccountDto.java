package ru.pcs.exam.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.exam.models.IndividualPartyAccount;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IndPartyAccountDto {

    private String number;
    private Double balance;
    private IndPartyDto individualParty;
    private List<IndPartyCardDto> cards;

    public static IndPartyAccountDto from(IndividualPartyAccount indPartyAccount) {
        return IndPartyAccountDto.builder()
                .number(indPartyAccount.getNumber())
                .balance(indPartyAccount.getBalance())
                .individualParty(IndPartyDto.from(indPartyAccount.getIndividualParty()))
                .cards(IndPartyCardDto.from(indPartyAccount.getCards()))
                .build();
    }

    public static List<IndPartyAccountDto> from(List<IndividualPartyAccount> individualPartyAccounts) {
        return individualPartyAccounts.stream().map(IndPartyAccountDto::from).collect(Collectors.toList());
    }
}
