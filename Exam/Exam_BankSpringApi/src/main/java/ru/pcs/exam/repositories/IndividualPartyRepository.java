package ru.pcs.exam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.exam.models.IndividualParty;

import java.util.Optional;

public interface IndividualPartyRepository extends JpaRepository<IndividualParty, Long> {

    boolean existsIndividualPartiesByPhone(String phone);

    Optional<IndividualParty> findByPhone(String phone);
}