package ru.pcs.exam.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.exam.models.IndividualPartyCard;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IndPartyCardDto {
    private String cardNumber;
    private IndividualPartyCard.CardStatus cardStatus;

    public static IndPartyCardDto from(IndividualPartyCard card) {
        return IndPartyCardDto.builder()
                .cardNumber(card.getCardNumber())
                .cardStatus(card.getCardStatus())
                .build();
    }


    public static List<IndPartyCardDto> from(List<IndividualPartyCard> cards) {
        if (cards == null) return Collections.emptyList();
        return cards.stream().map(IndPartyCardDto::from).collect(Collectors.toList());
    }

}
