package ru.pcs.exam.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.exam.validation.NoDigits;
import ru.pcs.exam.validation.NotSameNames;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@NotSameNames(message = "Имя и фамилия не могут быть одинаковыми.")
public class SignUpForm {
    @NotBlank(message = "Укажите имя.")
    @NoDigits(message = "Имя не должно содержать цифр.")
    private String firstName;
    @NoDigits(message = "Фамилия не должна содержать цифр.")
    @NotBlank(message = "Укажите фамилию.")
    private String lastName;
    @NotBlank(message = "Укажите номер телефона.")
    @Size(max = 12, min = 12, message = "Номер телефона должен начинатся с '+7' и содержать 11 цифр")
    private String phone;
    @NotBlank(message = "Укажите пароль.")
    @Size(min = 7, message = "Пароль должен содержать больше 7 символов")
    private String password;
}
