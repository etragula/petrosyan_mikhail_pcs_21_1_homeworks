package ru.pcs.exam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.exam.models.IndividualPartyCard;

import javax.transaction.Transactional;

public interface IndividualPartyCardRepository extends JpaRepository<IndividualPartyCard, Long> {

    IndividualPartyCard getIndividualPartyCardByCardNumber(String cardNumber);

    @Transactional
    void deleteIndividualPartyCardByCardNumber(String cardNumber);
}