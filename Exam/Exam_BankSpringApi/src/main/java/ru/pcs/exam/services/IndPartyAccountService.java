package ru.pcs.exam.services;

import ru.pcs.exam.dtos.IndPartyAccountDto;

import java.util.List;

public interface IndPartyAccountService {

    IndPartyAccountDto create();

    void delete(String accountNum);

    List<IndPartyAccountDto> getAllAccounts();
}
