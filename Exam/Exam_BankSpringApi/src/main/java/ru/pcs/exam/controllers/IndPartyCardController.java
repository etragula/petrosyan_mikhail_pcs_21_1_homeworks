package ru.pcs.exam.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.pcs.exam.dtos.IndPartyCardDto;
import ru.pcs.exam.erros.IndPartyAccountNotFoundException;
import ru.pcs.exam.erros.IndPartyCardNotFoundException;
import ru.pcs.exam.services.IndPartyCardService;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/card")
@RequiredArgsConstructor
public class IndPartyCardController {

    private final IndPartyCardService cardService;

    @PostMapping("/{accountNum}")
    @ResponseStatus(HttpStatus.CREATED)
    public IndPartyCardDto createCard(@PathVariable String accountNum) {
        log.info("Creating card for account with number - " + accountNum);
        return cardService.createCard(accountNum);
    }

    @PostMapping("/confirm/{cardNum}")
    @ResponseStatus(HttpStatus.CREATED)
    public IndPartyCardDto confirmCard(@PathVariable String cardNum) {
        log.info("Confirmation for card with number - " + cardNum);
        return cardService.confirmCard(cardNum);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({IndPartyAccountNotFoundException.class, IndPartyCardNotFoundException.class})
    public Map<String, String> handleIndPartyAccountNotFoundException(RuntimeException ex) {
        Map<String, String> errors = new HashMap<>();
        errors.put("error", ex.getMessage());
        return errors;
    }
}
