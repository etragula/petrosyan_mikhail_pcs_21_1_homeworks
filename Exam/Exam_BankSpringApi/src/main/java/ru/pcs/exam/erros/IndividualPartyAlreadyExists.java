package ru.pcs.exam.erros;

public class IndividualPartyAlreadyExists extends RuntimeException {
    public IndividualPartyAlreadyExists(String phone) {
        super("IndividualParty with " + phone + " already exists!");
    }
}
