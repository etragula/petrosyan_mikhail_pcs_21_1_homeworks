package ru.pcs.exam.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.exam.models.IndividualPartyAccount;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface IndividualPartyAccountRepository extends JpaRepository<IndividualPartyAccount, Long> {

    List<IndividualPartyAccount> findAllByIndividualPartyId(Long individualParty_id);

    @Transactional
    void deleteIndividualPartyAccountByNumber(String number);

    Optional<IndividualPartyAccount> findByNumber(String number);

}