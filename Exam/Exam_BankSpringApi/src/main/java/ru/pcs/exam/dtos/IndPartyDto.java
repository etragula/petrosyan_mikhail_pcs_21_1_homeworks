package ru.pcs.exam.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.exam.models.IndividualParty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IndPartyDto {
    private String firstName;
    private String lastName;
    private String phone;

    static IndPartyDto from(IndividualParty individualParty) {
        return IndPartyDto.builder()
                .firstName(individualParty.getFirstName())
                .lastName(individualParty.getLastName())
                .phone(individualParty.getPhone())
                .build();
    }
}
