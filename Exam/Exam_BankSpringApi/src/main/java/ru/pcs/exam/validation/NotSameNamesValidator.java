package ru.pcs.exam.validation;

import ru.pcs.exam.dtos.SignUpForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotSameNamesValidator implements ConstraintValidator<NotSameNames, SignUpForm> {
    @Override
    public boolean isValid(SignUpForm s, ConstraintValidatorContext constraintValidatorContext) {
        return !s.getFirstName().equalsIgnoreCase(s.getLastName());
    }
}
