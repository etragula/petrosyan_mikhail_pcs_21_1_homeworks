package ru.pcs.exam.services;

import ru.pcs.exam.dtos.SignUpForm;
import ru.pcs.exam.models.IndividualParty;

public interface IndPartyService {

    void createIndParty(SignUpForm form);

    IndividualParty getIndPartyById(Long indPartyId);

    void changeRoleToAdmin(String phone);
}
