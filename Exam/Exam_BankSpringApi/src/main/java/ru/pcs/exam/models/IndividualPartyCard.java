package ru.pcs.exam.models;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "account")
@EqualsAndHashCode(exclude = "account")
public class IndividualPartyCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "account_id")
    private IndividualPartyAccount account;
    private String cardNumber;
    private CardStatus cardStatus;

    public enum CardStatus {
        NOT_CONFIRMED, ACTIVE, BLOCKED, EXPIRED
    }

}
