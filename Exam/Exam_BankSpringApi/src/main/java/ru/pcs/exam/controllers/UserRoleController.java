package ru.pcs.exam.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import ru.pcs.exam.services.IndPartyService;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/role")
@RequiredArgsConstructor
public class UserRoleController {

    private final IndPartyService indPartyService;

    @PostMapping("/{phone}")
    public Map<String, String> changeRoleToAdmin(@PathVariable String phone) {
        log.info("Changing role to Admin for " + phone);
        indPartyService.changeRoleToAdmin(phone);
        return Collections.singletonMap("result", "User - " + phone + " changed to admin.");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(UsernameNotFoundException.class)
    public Map<String, String> handleUsernameNotFoundException(UsernameNotFoundException ex) {
        Map<String, String> errors = new HashMap<>();
        errors.put("error", ex.getMessage());
        return errors;
    }
}
