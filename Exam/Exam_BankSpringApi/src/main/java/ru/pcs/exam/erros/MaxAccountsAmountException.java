package ru.pcs.exam.erros;

public class MaxAccountsAmountException extends RuntimeException {
    public MaxAccountsAmountException() {
        super("You already have 3 accounts, if you need more you can by Premium Subscription!");
    }
}
