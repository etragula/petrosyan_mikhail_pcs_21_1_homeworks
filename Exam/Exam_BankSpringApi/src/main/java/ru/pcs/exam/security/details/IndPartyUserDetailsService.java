package ru.pcs.exam.security.details;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.pcs.exam.repositories.IndividualPartyRepository;

@Service
@RequiredArgsConstructor
public class IndPartyUserDetailsService implements UserDetailsService {

    private final IndividualPartyRepository individualPartyRepository;

    @Override
    public UserDetails loadUserByUsername(String phone) throws UsernameNotFoundException {
        return new IndPartyUserDetails(individualPartyRepository.findByPhone(phone).orElseThrow(
                () -> new UsernameNotFoundException("IndParty was not found!")
        ));
    }
}
