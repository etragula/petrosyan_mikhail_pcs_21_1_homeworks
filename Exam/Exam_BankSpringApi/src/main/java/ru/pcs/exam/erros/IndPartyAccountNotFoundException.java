package ru.pcs.exam.erros;


public class IndPartyAccountNotFoundException extends RuntimeException {
    public IndPartyAccountNotFoundException(String number) {
        super("Account with number - " + number + " was not found!");
    }
}
