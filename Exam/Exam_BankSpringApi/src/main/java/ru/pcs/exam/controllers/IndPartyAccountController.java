package ru.pcs.exam.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.pcs.exam.dtos.IndPartyAccountDto;
import ru.pcs.exam.erros.IndPartyAccountNotFoundException;
import ru.pcs.exam.erros.MaxAccountsAmountException;
import ru.pcs.exam.services.IndPartyAccountService;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class IndPartyAccountController {

    private final IndPartyAccountService accountService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('ADMIN')")
    public IndPartyAccountDto createAccount() {
        return accountService.create();
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<IndPartyAccountDto> getAccounts() {
        return accountService.getAllAccounts();
    }

    @DeleteMapping("/{accountNum}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('ADMIN')")
    public Map<String, String> deleteAccount(@PathVariable("accountNum") String accountNum) {
        accountService.delete(accountNum);
        return Collections.singletonMap("result", "Account - " + accountNum + " was deleted.");

    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({MaxAccountsAmountException.class,
            IndPartyAccountNotFoundException.class, AuthorizationServiceException.class})
    public Map<String, String> handleAccountExceptions(RuntimeException ex) {
        Map<String, String> errors = new HashMap<>();
        errors.put("error", ex.getMessage());
        return errors;
    }
}
