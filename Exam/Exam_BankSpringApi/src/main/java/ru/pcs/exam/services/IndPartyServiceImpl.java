package ru.pcs.exam.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.pcs.exam.dtos.SignUpForm;
import ru.pcs.exam.erros.IndividualPartyAlreadyExists;
import ru.pcs.exam.models.IndividualParty;
import ru.pcs.exam.repositories.IndividualPartyRepository;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class IndPartyServiceImpl implements IndPartyService {

    private final IndividualPartyRepository partyRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void createIndParty(SignUpForm form) {
        if (partyRepository.existsIndividualPartiesByPhone(form.getPhone())) {
            log.error("IndParty already exists!");
            throw new IndividualPartyAlreadyExists(form.getPhone());
        } else {
            partyRepository.save(IndividualParty.builder()
                    .firstName(form.getFirstName())
                    .lastName(form.getLastName())
                    .phone(form.getPhone())
                    .password(passwordEncoder.encode(form.getPassword()))
                    .role(IndividualParty.Role.USER)
                    .build());
        }
        log.info("IndParty was successfully saved!");
    }

    @Override
    public IndividualParty getIndPartyById(Long indPartyId) {
        return partyRepository.getById(indPartyId);
    }

    @Override
    public void changeRoleToAdmin(String phone) {
        final Optional<IndividualParty> partyOptional = partyRepository.findByPhone(phone);
        if (partyOptional.isPresent()) {
            final IndividualParty individualParty = partyOptional.get();
            individualParty.setRole(IndividualParty.Role.ADMIN);
            partyRepository.save(individualParty);
        } else throw new UsernameNotFoundException("User with " + phone + "does not exist!");
    }
}
