package ru.pcs.exam.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.pcs.exam.dtos.IndPartyAccountDto;
import ru.pcs.exam.erros.IndPartyAccountNotFoundException;
import ru.pcs.exam.erros.MaxAccountsAmountException;
import ru.pcs.exam.models.IndividualParty;
import ru.pcs.exam.models.IndividualPartyAccount;
import ru.pcs.exam.repositories.IndividualPartyAccountRepository;
import ru.pcs.exam.repositories.IndividualPartyRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class IndPartyAccountServiceImpl implements IndPartyAccountService {

    private final IndividualPartyAccountRepository accountRepository;
    private final IndividualPartyRepository partyRepository;

    @Override
    public IndPartyAccountDto create() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == null) throw new AuthorizationServiceException("Authorization problem, try again!");
        final Long indPartyId = Long.valueOf((String) principal);

        final IndividualParty individualParty = partyRepository.findById(indPartyId).orElseThrow(
                () -> new AuthorizationServiceException("Authorization problem, try again!"));

        if (accountRepository.findAllByIndividualPartyId(indPartyId).size() >= 3)
            throw new MaxAccountsAmountException();

        final IndividualPartyAccount partyAccount = IndividualPartyAccount.builder()
                .number(UUID.randomUUID().toString())
                .balance(0.0)
                .individualParty(individualParty)
                .build();
        accountRepository.save(partyAccount);
        log.info("Account was created number - " + partyAccount.getNumber());
        return IndPartyAccountDto.from(partyAccount);
    }

    @Override
    public List<IndPartyAccountDto> getAllAccounts() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == null) throw new AuthorizationServiceException("Authorization problem, try again!");
        final Long indPartyId = Long.valueOf((String) principal);

        final IndividualParty individualParty = partyRepository.findById(indPartyId).orElseThrow(
                () -> new AuthorizationServiceException("Authorization problem, try again!"));

        return IndPartyAccountDto.from(individualParty.getAccounts());
    }

    @Override
    public void delete(String accountNum) {
        final Optional<IndividualPartyAccount> partyAccount = accountRepository.findByNumber(accountNum);
        if (partyAccount.isPresent() && partyAccount.get().getCards().isEmpty()) {
            accountRepository.deleteIndividualPartyAccountByNumber(accountNum);
        } else {
            log.info("Account can't be deleted because there are active cards!");
            throw new IndPartyAccountNotFoundException(accountNum);
        }
    }


}
