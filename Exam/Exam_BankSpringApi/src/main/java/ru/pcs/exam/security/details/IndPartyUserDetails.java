package ru.pcs.exam.security.details;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.pcs.exam.models.IndividualParty;

import java.util.Collection;
import java.util.Collections;

public class IndPartyUserDetails implements UserDetails {

    private final IndividualParty individualParty;

    public IndPartyUserDetails(IndividualParty individualParty) {
        this.individualParty = individualParty;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(individualParty.getRole().name()));
    }

    @Override
    public String getPassword() {
        return individualParty.getPassword();
    }

    @Override
    public String getUsername() {
        return individualParty.getPhone();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public IndividualParty getIndividualParty() {
        return individualParty;
    }
}
