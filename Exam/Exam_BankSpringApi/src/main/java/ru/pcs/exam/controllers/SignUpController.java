package ru.pcs.exam.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import ru.pcs.exam.dtos.SignUpForm;
import ru.pcs.exam.erros.IndividualPartyAlreadyExists;
import ru.pcs.exam.services.IndPartyService;

import javax.validation.Valid;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/signUp")
@RequiredArgsConstructor
public class SignUpController {

    private final IndPartyService indPartyService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, String> signUp(@Valid @RequestBody SignUpForm form) {
        log.info("Creating indParty {}, {}, {}", form.getFirstName(), form.getLastName(), form.getPhone());
        indPartyService.createIndParty(form);
        return Collections.singletonMap("result", "IndividualParty was created!");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getFieldErrors()
                .forEach(error -> errors.put(error.getField(), error.getDefaultMessage()));
        ex.getBindingResult().getGlobalErrors()
                .forEach(error -> errors.put(error.getObjectName(), error.getDefaultMessage()));
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IndividualPartyAlreadyExists.class)
    public Map<String, String> handleIndividualPartyAlreadyExists(IndividualPartyAlreadyExists ex) {
        Map<String, String> errors = new HashMap<>();
        errors.put("error", ex.getMessage());
        return errors;
    }
}
