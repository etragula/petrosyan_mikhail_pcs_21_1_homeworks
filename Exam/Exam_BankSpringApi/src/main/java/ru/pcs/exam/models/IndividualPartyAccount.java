package ru.pcs.exam.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "{individualParty, cards}")
@EqualsAndHashCode(exclude = {"cards"})
public class IndividualPartyAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ind_party_id")
    private IndividualParty individualParty;

    private String number;

    private Double balance;

    @OneToMany(mappedBy = "account")
    private List<IndividualPartyCard> cards;
}
