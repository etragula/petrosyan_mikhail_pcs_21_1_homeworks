package ru.pcs.attestation.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pcs.attestation.dtos.AccountDto;
import ru.pcs.attestation.services.AccountService;

@Controller
@RequestMapping("/signUp")
@RequiredArgsConstructor
public class SignUpController {

    private final AccountService accountService;

    @PostMapping
    public ResponseEntity signUp(@RequestBody AccountDto accountDto) {
        accountService.save(accountDto);
        return ResponseEntity.ok().build();
    }
}