package ru.pcs.attestation.services;

import ru.pcs.attestation.dtos.AccountDto;

public interface AccountService {
    void save(AccountDto account);
}
