package ru.pcs.attestation.services;

import ru.pcs.attestation.dtos.ProductDto;

import java.util.List;

public interface ProductService {
    List<ProductDto> getAllProducts();

    void save(ProductDto productDto);
}
