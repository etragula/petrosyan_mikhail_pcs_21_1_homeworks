package ru.pcs.attestation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.attestation.models.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
