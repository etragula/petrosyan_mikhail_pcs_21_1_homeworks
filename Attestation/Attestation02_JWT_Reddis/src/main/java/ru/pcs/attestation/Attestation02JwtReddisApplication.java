package ru.pcs.attestation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Attestation02JwtReddisApplication {

    public static void main(String[] args) {
        SpringApplication.run(Attestation02JwtReddisApplication.class, args);
    }

}
