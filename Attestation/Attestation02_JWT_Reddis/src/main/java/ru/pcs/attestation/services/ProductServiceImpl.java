package ru.pcs.attestation.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.attestation.dtos.ProductDto;
import ru.pcs.attestation.models.Product;
import ru.pcs.attestation.repositories.ProductRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public List<ProductDto> getAllProducts() {
        return ProductDto.from(productRepository.findAll());
    }

    @Override
    public void save(ProductDto productDto) {
        Product product = Product.builder()
                .name(productDto.getName())
                .price(productDto.getPrice())
                .amount(productDto.getAmount())
                .build();

        productRepository.save(product);
    }
}
