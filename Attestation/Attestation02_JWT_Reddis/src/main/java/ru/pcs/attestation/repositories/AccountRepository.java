package ru.pcs.attestation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.attestation.models.Account;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Optional<Account> findAccountByEmail(String email);

    Optional<Account> findByToken(String token);
}
