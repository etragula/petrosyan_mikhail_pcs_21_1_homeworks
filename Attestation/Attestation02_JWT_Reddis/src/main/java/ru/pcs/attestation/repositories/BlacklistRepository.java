package ru.pcs.attestation.repositories;

public interface BlacklistRepository {
    void save(String token);

    boolean exists(String token);
}
