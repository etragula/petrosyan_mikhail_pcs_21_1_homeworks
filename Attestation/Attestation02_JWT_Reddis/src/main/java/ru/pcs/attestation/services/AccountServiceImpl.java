package ru.pcs.attestation.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.pcs.attestation.dtos.AccountDto;
import ru.pcs.attestation.models.Account;
import ru.pcs.attestation.repositories.AccountRepository;

import java.util.Locale;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    public void save(AccountDto accountDto) {
        final Account account = Account.builder()
                .firstName(accountDto.getFirstName())
                .lastName(accountDto.getLastName())
                .email(accountDto.getEmail().toLowerCase(Locale.ROOT))
                .password(passwordEncoder.encode(accountDto.getPassword()))
                .role(Account.Role.USER)
                .state(Account.State.NOT_CONFIRMED)
                .build();

        accountRepository.save(account);
    }
}
