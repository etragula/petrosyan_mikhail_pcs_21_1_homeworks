package ru.pcs.attestation.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.attestation.models.Product;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDto {
    @NotBlank
    private String name;
    @NotNull
    @Positive
    private Float price;
    @NotNull
    @PositiveOrZero
    private Integer amount;

    public static ProductDto from(Product product) {
        return ProductDto.builder()
                .name(product.getName())
                .amount(product.getAmount())
                .price(product.getPrice())
                .build();
    }

    public static List<ProductDto> from(List<Product> products) {
        return products.stream().map(ProductDto::from).collect(Collectors.toList());
    }
}
