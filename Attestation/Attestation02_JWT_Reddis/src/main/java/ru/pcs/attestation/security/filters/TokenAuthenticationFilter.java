package ru.pcs.attestation.security.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.pcs.attestation.dtos.SignInForm;
import ru.pcs.attestation.models.Account;
import ru.pcs.attestation.security.details.AccountUserDetails;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;


@Slf4j
public class TokenAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    public static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json; charset=UTF-8";
    public static final String TOKEN = "token";
    private static final String JWT_SECRET_KEY = "my_secret_key";

    private final ObjectMapper objectMapper;

    public TokenAuthenticationFilter(AuthenticationManager authenticationManager, ObjectMapper objectMapper) {
        super(authenticationManager);
        this.objectMapper = objectMapper;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try {
            SignInForm signInForm = objectMapper.readValue(request.getReader(), SignInForm.class);
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    signInForm.getEmail(), signInForm.getPassword());
            return super.getAuthenticationManager().authenticate(token);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException {
        AccountUserDetails details = (AccountUserDetails) authResult.getPrincipal();
        final Account account = details.getAccount();
        final String token = JWT.create()
                .withSubject(account.getId().toString())
                .withClaim("role", account.getRole().name())
                .withClaim("state", account.getState().name())
                .withExpiresAt(new Date((System.currentTimeMillis()) + 2629800000L))
                .sign(Algorithm.HMAC256(JWT_SECRET_KEY));
        response.setContentType(APPLICATION_JSON_CHARSET_UTF_8);
        objectMapper.writeValue(response.getWriter(), Collections.singletonMap(TOKEN, token));
    }
}
