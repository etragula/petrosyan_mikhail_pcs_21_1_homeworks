package attestation.reflection;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public class Cleaner {

    void cleanUp(Object object, Set<String> fieldsToCleanUp, Set<String> fieldsToOutput) {
        boolean wasMapProcessed = false;
        boolean wereFieldsCleaned = false;
        boolean wereFieldsPrinted = false;

        if (object instanceof Map<?, ?>) {
            wasMapProcessed = processMapObject(object, fieldsToCleanUp, fieldsToOutput);
        } else {
            wereFieldsCleaned = cleanFields(object, fieldsToCleanUp);
            wereFieldsPrinted = printFields(object, fieldsToOutput);
        }

        if (!wereFieldsCleaned && !wereFieldsPrinted && !wasMapProcessed) {
            throw new IllegalArgumentException();
        }
    }

    private boolean processMapObject(Object object, Set<String> fieldsToCleanUp, Set<String> fieldsToOutput) {
        Set<?> keySet = ((Map<?, ?>) object).keySet();
        boolean mapWasProcessed = false;

        for (String toClean : fieldsToCleanUp) {
            if (keySet.contains(toClean)) {
                ((Map<?, ?>) object).remove(toClean);
                mapWasProcessed = true;
            }
        }

        for (String toOutput : fieldsToOutput) {
            if (keySet.contains(toOutput)) {
                System.out.println(toOutput + " - " + ((Map<?, ?>) object).get(toOutput).toString());
                mapWasProcessed = true;
            }
        }
        return mapWasProcessed;
    }

    private boolean cleanFields(Object object, Set<String> fieldsToCleanUp) {
        Field[] objectFields = getObjectFields(object);
        boolean wasFieldChanged = false;

        for (Field field : objectFields) {

            if (fieldsToCleanUp.contains(field.getName())) {
                if (field.getType().isPrimitive()) {
                    changeValueToDefault(object, field, 0);
                } else {
                    changeValueToDefault(object, field, null);
                }
                wasFieldChanged = true;
            }
        }

        return wasFieldChanged;
    }

    private boolean printFields(Object object, Set<String> fieldsToOutput) {
        Field[] objectFields = getObjectFields(object);
        boolean wasFieldPrint = false;
        Consumer<Object> consumer;

        for (Field field : objectFields) {

            if (fieldsToOutput.contains(field.getName())) {
                if (field.getType().isPrimitive()) {
                    consumer = val -> System.out.println(field.getName() + " - " + String.valueOf(val));
                } else {
                    consumer = val -> System.out.println(field.getName() + " - " + val.toString());
                }
                printFieldValue(object, field, consumer);
                wasFieldPrint = true;
            }
        }
        return wasFieldPrint;
    }

    private Field[] getObjectFields(Object object) {
        Class<?> objectClass = object.getClass();
        return objectClass.getDeclaredFields();
    }

    private void changeValueToDefault(Object object, Field field, Object value) {
        try {
            field.setAccessible(true);
            field.set(object, value);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void printFieldValue(Object object, Field field, Consumer<Object> consumer) {
        try {
            field.setAccessible(true);
            Object value = field.get(object);
            consumer.accept(value);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        } catch (NullPointerException e) {
            System.out.println(field.getName() + " - " + null);
        }
    }
}
