package attestation.reflection;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class App {
    public static void main(String[] args) {
        Cleaner cleaner = new Cleaner();

        cleaner.cleanUp(
                new User("Mike", 22, 170),
                new HashSet<>(Arrays.asList("name", "ae", "height")),
                new HashSet<>(Arrays.asList("name", "age", "height"))
        );

        Map<String, Integer> map = new HashMap<>();
        map.put("item", 1);
        map.put("item1", 2);
        map.put("item2", 3);

        cleaner.cleanUp(
                map,
                new HashSet<>(Arrays.asList("item", "age", "height")),
                new HashSet<>(Arrays.asList("item2", "item1", "it3em"))
        );
    }

    private static class User {

        private String name;
        private int age;
        private Integer height;

        public User(String name, int age, Integer height) {
            this.name = name;
            this.age = age;
            this.height = height;
        }
    }
}
