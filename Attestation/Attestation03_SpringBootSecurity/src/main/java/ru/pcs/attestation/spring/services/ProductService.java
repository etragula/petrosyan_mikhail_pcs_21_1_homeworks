package ru.pcs.attestation.spring.services;

import ru.pcs.attestation.spring.dto.ProductDto;

import java.util.List;

public interface ProductService {
    void save(ProductDto productDto);

    List<ProductDto> getAllProducts();
}
