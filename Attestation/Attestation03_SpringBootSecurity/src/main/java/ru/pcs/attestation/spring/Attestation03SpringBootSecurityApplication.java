package ru.pcs.attestation.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Attestation03SpringBootSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(Attestation03SpringBootSecurityApplication.class, args);
    }

}
