package ru.pcs.attestation.spring.services;

import ru.pcs.attestation.spring.dto.AccountDto;

public interface AccountService {

    void save(AccountDto accountDto);

    void confirmUser(String email);
}
