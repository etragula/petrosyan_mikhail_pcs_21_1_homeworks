package ru.pcs.attestation.spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.attestation.spring.models.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
