package ru.pcs.attestation.spring.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pcs.attestation.spring.dto.AccountDto;
import ru.pcs.attestation.spring.services.AccountService;

import javax.validation.Valid;

@Controller
@RequestMapping("/signUp")
@RequiredArgsConstructor
public class SignUpController {

    private final AccountService accountService;

    @GetMapping
    public String getSignUpPage(Authentication authentication, Model model) {
        if (authentication != null) return "redirect:/profile";
        model.addAttribute("accountDto", new AccountDto());
        return "signUp";
    }

    @PostMapping
    public String signUp(@Valid AccountDto account, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("accountDto", account);
            return "signUp";
        }
        accountService.save(account);
        return "redirect:/signIn";
    }
}
