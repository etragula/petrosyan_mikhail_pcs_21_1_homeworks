package ru.pcs.attestation.spring.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pcs.attestation.spring.services.AccountService;


@Controller
@RequestMapping("/signIn")
@RequiredArgsConstructor
public class SignInController {

    public final AccountService accountService;

    @GetMapping
    public String getSignUpPage(Authentication authentication) {
        if (authentication != null) return "redirect:/profile";
        return "signIn";
    }

    @PostMapping("/confirm")
    public String confirm(String email) {
        accountService.confirmUser(email);
        return "signIn";
    }
}
