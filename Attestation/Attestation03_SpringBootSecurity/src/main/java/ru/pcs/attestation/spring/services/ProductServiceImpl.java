package ru.pcs.attestation.spring.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.attestation.spring.dto.ProductDto;
import ru.pcs.attestation.spring.models.Product;
import ru.pcs.attestation.spring.repositories.ProductRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public void save(ProductDto productDto) {
        productRepository.save(Product.builder()
                .name(productDto.getName())
                .price(productDto.getPrice())
                .amount(productDto.getAmount())
                .build());
    }

    @Override
    public List<ProductDto> getAllProducts() {
        return ProductDto.from(productRepository.findAll());
    }
}
