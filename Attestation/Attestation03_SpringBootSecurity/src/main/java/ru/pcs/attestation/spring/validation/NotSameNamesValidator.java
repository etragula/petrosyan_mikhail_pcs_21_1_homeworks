package ru.pcs.attestation.spring.validation;

import ru.pcs.attestation.spring.dto.AccountDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotSameNamesValidator implements ConstraintValidator<NotSameNames, AccountDto> {
    @Override
    public boolean isValid(AccountDto accountDto, ConstraintValidatorContext constraintValidatorContext) {
        return !accountDto.getFirstName().equalsIgnoreCase(accountDto.getLastName());
    }
}
