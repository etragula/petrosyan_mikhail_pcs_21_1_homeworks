package ru.pcs.attestation.spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.attestation.spring.models.Account;

import java.util.Optional;

public interface AccountsRepository extends JpaRepository<Account, Long> {
    Optional<Account> findAccountByEmail(String email);
}
