package web.lesson.pcs.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import web.lesson.pcs.dto.AccountDto;
import web.lesson.pcs.dto.SignUpForm;
import web.lesson.pcs.repositories.AccountRepository;
import web.lesson.pcs.repositories.AccountRepositoryJdbcImpl;
import web.lesson.pcs.services.AccountService;
import web.lesson.pcs.services.AccountServiceImpl;
import web.lesson.pcs.services.SignUpService;
import web.lesson.pcs.services.SignUpServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

@WebServlet("/accounts/json")
public class AccountsJsonServlet extends HttpServlet {

    private AccountService accountService;
    private SignUpService signUpService;
    private ObjectMapper mapper;

    @Override
    public void init(ServletConfig config) {
        final ServletContext servletContext = config.getServletContext();
        final DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        AccountRepository accountRepository = new AccountRepositoryJdbcImpl(dataSource);
        this.accountService = new AccountServiceImpl(accountRepository);
        this.signUpService = new SignUpServiceImpl(accountRepository);
        this.mapper = new ObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<AccountDto> accountDtos = accountService.searchByEmail(req.getParameter("email"));
        resp.setContentType("application/json");
        String accountsJson = mapper.writeValueAsString(accountDtos);
        resp.getWriter().println(accountsJson);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String body = req.getReader().readLine();
        SignUpForm account = mapper.readValue(body, SignUpForm.class);
        signUpService.signUp(SignUpForm.builder()
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .eMail("")
                .password("")
                .build());
        resp.getWriter().println(mapper.writeValueAsString(accountService.getAll()));
    }
}
