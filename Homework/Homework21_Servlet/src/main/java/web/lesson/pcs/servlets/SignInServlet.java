package web.lesson.pcs.servlets;

import web.lesson.pcs.dto.SignInForm;
import web.lesson.pcs.repositories.AccountRepository;
import web.lesson.pcs.repositories.AccountRepositoryJdbcImpl;
import web.lesson.pcs.services.SignInService;
import web.lesson.pcs.services.SignInServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {

    private SignInService signInService;

    @Override
    public void init(ServletConfig config) {
        final ServletContext servletContext = config.getServletContext();
        final DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        AccountRepository accountRepository = new AccountRepositoryJdbcImpl(dataSource);
        this.signInService = new SignInServiceImpl(accountRepository);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/jsp/sign-in.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SignInForm signInForm = SignInForm.builder()
                .eMail(req.getParameter("eMail"))
                .password(req.getParameter("password"))
                .build();

        if (signInService.isAuthenticated(signInForm)) {
            HttpSession session = req.getSession(true);
            session.setAttribute("isAuthenticated", true);
            session.setAttribute("logIn", req.getParameter("eMail"));

            resp.sendRedirect("/profile");
        } else {
            resp.sendRedirect("/signIn?error");
        }
    }
}
