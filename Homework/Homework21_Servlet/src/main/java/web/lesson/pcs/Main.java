package web.lesson.pcs;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import web.lesson.pcs.dto.SignUpForm;
import web.lesson.pcs.repositories.AccountRepository;
import web.lesson.pcs.repositories.AccountRepositoryJdbcImpl;
import web.lesson.pcs.services.SignUpService;
import web.lesson.pcs.services.SignUpServiceImpl;

import java.io.IOException;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(ClassLoader.getSystemResourceAsStream("application.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.user"));
        config.setPassword(properties.getProperty("db.password"));
        config.setDriverClassName(properties.getProperty("db.driver-class_name"));
        config.setMaximumPoolSize(
                Integer.parseInt(properties.getProperty("db.hikari-pool-size")));
        HikariDataSource dataSource = new HikariDataSource(config);
        AccountRepository accountRepository = new AccountRepositoryJdbcImpl(dataSource);
        SignUpService signUpService = new SignUpServiceImpl(accountRepository);

        SignUpForm form = SignUpForm.builder()
                .firstName("Ivan")
                .lastName("Ivanov")
                .eMail("ivan-ivanov@mail.ru")
                .password("ivan1332")
                .build();
        signUpService.signUp(form);

        System.out.println(accountRepository.findAll());
    }
}
