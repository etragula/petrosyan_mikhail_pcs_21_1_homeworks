package web.lesson.pcs.services;

import web.lesson.pcs.dto.SignUpForm;

public interface SignUpService {
    void signUp(SignUpForm form);

}
