package web.lesson.pcs.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import web.lesson.pcs.dto.FileDto;
import web.lesson.pcs.repositories.FileInfoRepository;
import web.lesson.pcs.repositories.FileInfoRepositoryImpl;
import web.lesson.pcs.services.FilesService;
import web.lesson.pcs.services.FilesServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet("/files")
@MultipartConfig
public class FilesServlet extends HttpServlet {

    private FilesService filesService;
    private ObjectMapper mapper;

    @Override
    public void init(ServletConfig config) {
        final ServletContext servletContext = config.getServletContext();
        final DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        FileInfoRepository fileInfoRepository = new FileInfoRepositoryImpl(dataSource);
        this.filesService = new FilesServiceImpl(fileInfoRepository);
        this.filesService.setStoragePath((String) servletContext.getAttribute("storagePath"));
        this.mapper = new ObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String storageFileName = req.getParameter("fileName");
        FileDto fileDto = filesService.getFileByStorageName(storageFileName);
        resp.setHeader("Content-Disposition", "filename=\"" + fileDto.getOrFileName() + "\"");
        resp.setContentType(fileDto.getMimeType());
        resp.setContentLength(Math.toIntExact(fileDto.getSize()));
        resp.flushBuffer();
        filesService.writeFile(fileDto, resp.getOutputStream());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        final String originalFileName = req.getParameter("name");
        final String eMail = req.getSession().getAttribute("logIn").toString();
        System.out.println("Lets try to find file with name - " + originalFileName);

        final String fileJson = mapper.writeValueAsString(filesService.getFileByOriginalName(originalFileName, eMail));
        resp.getWriter().println(fileJson);
    }

}
