package web.lesson.pcs.services;

import web.lesson.pcs.dto.SignInForm;
import web.lesson.pcs.repositories.AccountRepository;

public class SignInServiceImpl implements SignInService {

    private final AccountRepository accountRepository;

    public SignInServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public boolean isAuthenticated(SignInForm signInForm) {
        return accountRepository
                .findByEmail(signInForm.getEMail())
                .map(account -> account.getPassword().equals(signInForm.getPassword()))
                .orElse(false);
    }
}
