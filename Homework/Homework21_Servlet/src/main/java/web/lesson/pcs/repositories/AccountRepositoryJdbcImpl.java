package web.lesson.pcs.repositories;

import web.lesson.pcs.models.Account;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class AccountRepositoryJdbcImpl implements AccountRepository {

    public static final String SQL_FIND_ALL = "select * from account order by id;";
    public static final String SQL_FIND_BY_EMAIL = "select * from account where email=?;";
    public static final String SQL_LIKE_EMAIL = "select * from account where email like ?;";
    public static final String SQL_INSERT = "insert into account(first_name, last_name, eMail, password) values (?, ?, ?, ?);";

    private final DataSource dataSource;

    private static final Function<ResultSet, Account> AccountMapper = resultSet -> {
        try {
            return new Account(
                    resultSet.getLong("id"),
                    resultSet.getString("first_name"),
                    resultSet.getString("last_name"),
                    resultSet.getString("eMail"),
                    resultSet.getString("password")
            );
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    public AccountRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Account save(Account account) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, account.getFirstName());
            statement.setString(2, account.getLastName());
            statement.setString(3, account.getEMail());
            statement.setString(4, account.getPassword());
            final int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) throw new SQLException("Can't insert account.");
            final ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                account.setId(generatedKeys.getLong("id"));
                return account;
            } else throw new SQLException("Can't insert account.");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Account> findByEmail(String email) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_EMAIL)) {
            statement.setString(1, email);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(AccountMapper.apply(resultSet));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Account> searchByEmail(String email) {
        List<Account> accounts = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_LIKE_EMAIL)) {
            statement.setString(1, "%" + email + "%");
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    accounts.add(AccountMapper.apply(resultSet));
                }
                return accounts;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Account> findAll() {
        List<Account> accounts = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    accounts.add(AccountMapper.apply(resultSet));
                }
                return accounts;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
