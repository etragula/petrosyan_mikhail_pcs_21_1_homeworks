package web.lesson.pcs.servlets;

import web.lesson.pcs.dto.AccountDto;
import web.lesson.pcs.repositories.AccountRepository;
import web.lesson.pcs.repositories.AccountRepositoryJdbcImpl;
import web.lesson.pcs.services.AccountService;
import web.lesson.pcs.services.AccountServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

@WebServlet("/accounts")
public class AccountServlet extends HttpServlet {

    private AccountService accountService;

    @Override
    public void init(ServletConfig config) {
        final ServletContext servletContext = config.getServletContext();
        final DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        AccountRepository accountRepository = new AccountRepositoryJdbcImpl(dataSource);
        this.accountService = new AccountServiceImpl(accountRepository);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<AccountDto> accounts = accountService.getAll();
        req.setAttribute("accounts", accounts);
        req.getRequestDispatcher("jsp/accounts.jsp").forward(req, resp);
    }
}
