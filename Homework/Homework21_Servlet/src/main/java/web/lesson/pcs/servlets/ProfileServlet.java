package web.lesson.pcs.servlets;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {

    private static final int COLOR_COOKIE_MAX_AGE = 60 * 60 * 25 * 365;

    @Override
    public void init(ServletConfig config) {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pageColour = req.getParameter("color");
        if (pageColour != null) {
            req.setAttribute("color", pageColour);
            Cookie cookie = new Cookie("pageColor", pageColour);
            cookie.setMaxAge(COLOR_COOKIE_MAX_AGE);
            resp.addCookie(cookie);
        }
        req.getRequestDispatcher("jsp/profile.jsp").forward(req, resp);
    }
}
