package web.lesson.pcs.repositories;

import web.lesson.pcs.models.FileInfo;

import java.util.List;
import java.util.Optional;

public interface FileInfoRepository {

    void save(FileInfo fileInfo);

    Optional<FileInfo> findByStorageName(String storageFileName);

    List<FileInfo> findByOriginalName(String originalFileName, String eMail);
}
