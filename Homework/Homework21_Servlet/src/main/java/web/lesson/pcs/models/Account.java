package web.lesson.pcs.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Account {
    private Long id;

    private String firstName;
    private String lastName;

    private String eMail;
    private String password;
}
