package web.lesson.pcs.servlets;

import web.lesson.pcs.dto.SignUpForm;
import web.lesson.pcs.repositories.AccountRepository;
import web.lesson.pcs.repositories.AccountRepositoryJdbcImpl;
import web.lesson.pcs.services.SignUpService;
import web.lesson.pcs.services.SignUpServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

public class SignUpServlet extends HttpServlet {

    private SignUpService signUpService;

    @Override
    public void init(ServletConfig config) {
        final ServletContext servletContext = config.getServletContext();
        final DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        AccountRepository accountRepository = new AccountRepositoryJdbcImpl(dataSource);
        this.signUpService = new SignUpServiceImpl(accountRepository);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/jsp/sign-up.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SignUpForm signUpForm = SignUpForm.builder()
                .firstName(req.getParameter("firstName"))
                .lastName(req.getParameter("lastName"))
                .eMail(req.getParameter("eMail"))
                .password(req.getParameter("password"))
                .build();

        signUpService.signUp(signUpForm);

        resp.sendRedirect("/signIn");
    }
}
