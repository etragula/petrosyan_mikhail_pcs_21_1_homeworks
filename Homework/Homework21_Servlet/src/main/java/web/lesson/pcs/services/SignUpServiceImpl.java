package web.lesson.pcs.services;

import web.lesson.pcs.dto.SignUpForm;
import web.lesson.pcs.models.Account;
import web.lesson.pcs.repositories.AccountRepository;

import java.util.Locale;

public class SignUpServiceImpl implements SignUpService {

    private final AccountRepository accountRepository;

    public SignUpServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void signUp(SignUpForm form) {
        accountRepository.save(
                Account.builder()
                        .firstName(form.getFirstName())
                        .lastName(form.getLastName())
                        .eMail(form.getEMail().toLowerCase(Locale.ROOT))
                        .password(form.getPassword())
                        .build());
    }
}
