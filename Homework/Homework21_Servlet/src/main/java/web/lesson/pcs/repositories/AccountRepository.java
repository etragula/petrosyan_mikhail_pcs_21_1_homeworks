package web.lesson.pcs.repositories;

import web.lesson.pcs.models.Account;

import java.util.List;
import java.util.Optional;

public interface AccountRepository {

    List<Account> findAll();

    Account save(Account account);

    Optional<Account> findByEmail(String email);

    List<Account> searchByEmail(String email);
}
