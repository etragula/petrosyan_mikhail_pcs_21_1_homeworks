package web.lesson.pcs.services;

import web.lesson.pcs.dto.AccountDto;
import web.lesson.pcs.repositories.AccountRepository;

import java.util.List;

import static web.lesson.pcs.dto.AccountDto.from;

public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;


    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public List<AccountDto> getAll() {
        return from(accountRepository.findAll());
    }

    @Override
    public List<AccountDto> searchByEmail(String email) {
        return from(accountRepository.searchByEmail(email));
    }
}
