package web.lesson.pcs.services;

import web.lesson.pcs.dto.SignInForm;

public interface SignInService {
    boolean isAuthenticated(SignInForm signInForm);
}
