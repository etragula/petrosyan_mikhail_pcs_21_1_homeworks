package web.lesson.pcs.services;

import web.lesson.pcs.dto.FileDto;

import java.io.OutputStream;
import java.util.List;

public interface FilesService {
    void upload(FileDto fileDto);

    void setStoragePath(String storagePath);

    FileDto getFileByStorageName(String storageFileName);

    List<FileDto> getFileByOriginalName(String storageFileName, String eMail);

    void writeFile(FileDto file, OutputStream outputStream);
}
