package web.lesson.pcs.services;

import web.lesson.pcs.dto.AccountDto;

import java.util.List;

public interface AccountService {

    List<AccountDto> getAll();

    List<AccountDto> searchByEmail(String email);

}
