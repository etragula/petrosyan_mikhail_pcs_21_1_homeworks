package web.lesson.pcs.servlets;

import web.lesson.pcs.dto.FileDto;
import web.lesson.pcs.repositories.FileInfoRepository;
import web.lesson.pcs.repositories.FileInfoRepositoryImpl;
import web.lesson.pcs.services.FilesService;
import web.lesson.pcs.services.FilesServiceImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.List;

@WebServlet("/download")
public class FileDownloadServlet extends HttpServlet {

    private FilesService filesService;
    private String storagePath;

    @Override
    public void init(ServletConfig config) {
        final ServletContext servletContext = config.getServletContext();
        final DataSource dataSource = (DataSource) servletContext.getAttribute("dataSource");
        FileInfoRepository fileInfoRepository = new FileInfoRepositoryImpl(dataSource);
        this.filesService = new FilesServiceImpl(fileInfoRepository);
        this.storagePath = (String) servletContext.getAttribute("storagePath");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        String parameter = req.getParameter("name");
        String fileName = parameter.substring(0, parameter.indexOf('.'));
        final List<FileDto> files = filesService.getFileByOriginalName(fileName, req.getSession().getAttribute("logIn").toString());
        final FileDto fileDto = files.get(0);
        if (fileDto != null) {
            String filePath = storagePath + fileDto.getStorageFileName();
            response.setContentType(fileDto.getMimeType());
            PrintWriter out = response.getWriter();
            parameter = URLEncoder.encode(parameter, "UTF8");
            response.setHeader("Content-Disposition", "inline; filename=\"" + parameter + "\"");
            FileInputStream fileInputStream = new FileInputStream(filePath);
            int i;
            while ((i = fileInputStream.read()) != -1) {
                out.write(i);
            }
            fileInputStream.close();
            out.close();
        }
    }
}
