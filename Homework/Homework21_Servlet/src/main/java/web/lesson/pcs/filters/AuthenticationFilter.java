package web.lesson.pcs.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebFilter("/*")
public class AuthenticationFilter implements Filter {

    private static final List<String> PROTECTED_URIS = Arrays.asList(
            "/profile", "/accounts", "/filesUpload", "/searchFile"
    );

    private static final String DEFAULT_AUTHENTICATED_ATTRIBUTE_NAME = "isAuthenticated";

    private static final List<String> NO_ACCESS_AFTER_SIGN_IN = Arrays.asList(
            "/signIn", "/signUp"
    );

    private static final String DEFAULT_REDIRECT_PAGE = "/profile";
    private static final String DEFAULT_SIGN_IN_PAGE = "/signIn";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        final String requestURI = request.getRequestURI();

        if (PROTECTED_URIS.contains(requestURI)) {
            if (isAuthenticated(request)) {
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            } else {
                response.sendRedirect(DEFAULT_SIGN_IN_PAGE);
                return;
            }
        }
        if (isAuthenticated(request) && NO_ACCESS_AFTER_SIGN_IN.contains(requestURI)) {
            response.sendRedirect(DEFAULT_REDIRECT_PAGE);
            return;
        }

        filterChain.doFilter(request, response);
    }


    public boolean isAuthenticated(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) return false;
        Boolean result = (Boolean) session.getAttribute(DEFAULT_AUTHENTICATED_ATTRIBUTE_NAME);
        return result != null && result;
    }

    @Override
    public void destroy() {

    }
}
