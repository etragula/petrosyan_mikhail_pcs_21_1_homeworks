<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title style="color: ${color}">SignIn </title>
</head>
<body>
<h1 style="color: ${color}">Sign In Page</h1>
<h2>Please Enter Your Data:</h2>
<form action="/signIn" method="post">
    <label for="email">Enter eMail:</label>
    <input type="email" id="eMail" name="eMail" placeholder="Your eMail">
    <br>
    <label for="password">Enter password:</label>
    <input type="password" id="password" name="password" placeholder="Your password">
    <br>
    <input type="submit" name="Sign Up!">
</form>
</body>
</html>