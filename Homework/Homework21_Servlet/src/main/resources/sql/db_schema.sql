create table account
(
    id         bigserial primary key,
    first_name varchar(40),
    last_name  varchar(40),
    eMail      varchar(40),
    password   varchar(40)
);

create table file_info
(
    id                 bigserial primary key,
    original_file_name varchar(100),
    storage_file_name  varchar(100),
    size               bigint,
    mime_type          varchar(100),
    description        varchar(100),
    eMail        varchar(100)
)