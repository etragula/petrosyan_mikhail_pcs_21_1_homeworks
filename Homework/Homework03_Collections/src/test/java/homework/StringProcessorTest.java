package homework;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static homework.StringProcessor.getLettersFromWord;
import static org.junit.jupiter.api.Assertions.assertEquals;

class StringProcessorTest {

    @Test
    void validateGetLettersFromWord() {
        String word = "3aaa1 bb2b";

        Map<Character, Integer> map = new HashMap<>();
        Map<Character, Integer> lettersFromWord = getLettersFromWord(word);

        map.put('a', 3);
        map.put('b', 3);

        assertEquals(map, lettersFromWord);
    }
}
