package homework;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class StringProcessor {

    private StringProcessor() {
    }

    public static Map<Character, Integer> getLettersFromWord(String word) {
        Map<Character, Integer> characterMap = new HashMap<>();
        char[] charArray = word.toLowerCase().toCharArray();

        for (char c : charArray) {
            Integer value = characterMap.get(c);

            if (value != null) characterMap.put(c, ++value);
            else if (Character.isAlphabetic(c)) characterMap.put(c, 1);
        }

        return characterMap;
    }

    public static void printLetters(Map<Character, Integer> map) {
        TreeMap<Character, Integer> sortedCharacterMap = new TreeMap<>(map);

        if (map.isEmpty()) {
            System.out.println("Нет букв в слове!");
            return;
        }

        for (Map.Entry<Character, Integer> entry : sortedCharacterMap.entrySet()) {
            System.out.printf("%1c - %3d\n", entry.getKey(), entry.getValue());
        }
    }
}
