package homework;

import java.util.Scanner;

import static homework.StringProcessor.getLettersFromWord;
import static homework.StringProcessor.printLetters;

public class App {

    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Введите слово: ");

            String wordToProcess = scanner.nextLine();

            printLetters(
                    getLettersFromWord(wordToProcess)
            );
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }
}
