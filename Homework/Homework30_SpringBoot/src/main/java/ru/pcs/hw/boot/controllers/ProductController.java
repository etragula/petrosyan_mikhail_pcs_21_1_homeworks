package ru.pcs.hw.boot.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pcs.hw.boot.dto.ProductDto;
import ru.pcs.hw.boot.services.ProductService;

import javax.validation.Valid;

@Controller
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping
    public String getProductPage(Model model) {
        model.addAttribute("productDto", new ProductDto());
        return "add-product";
    }

    @PostMapping
    public String addProduct(@Valid ProductDto productDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("productDto", productDto);
            return "add-product";
        }
        productService.addProduct(productDto);
        return "redirect:/product";
    }
}
