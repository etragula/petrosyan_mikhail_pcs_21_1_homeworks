package ru.pcs.hw.boot.services;

import org.springframework.stereotype.Service;
import ru.pcs.hw.boot.dto.ProductDto;

@Service
public interface ProductService {

    void addProduct(ProductDto productDto);
}
