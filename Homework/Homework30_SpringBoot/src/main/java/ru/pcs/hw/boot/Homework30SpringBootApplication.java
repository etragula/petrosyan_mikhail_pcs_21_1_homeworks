package ru.pcs.hw.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homework30SpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Homework30SpringBootApplication.class, args);
	}

}
