package ru.pcs.hw.boot.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IntegerValidation implements ConstraintValidator<IsInteger, Number> {
    @Override
    public boolean isValid(Number number, ConstraintValidatorContext constraintValidatorContext) {
        if (number != null)
            return number.floatValue() % 1 == 0;
        else return false;
    }
}
