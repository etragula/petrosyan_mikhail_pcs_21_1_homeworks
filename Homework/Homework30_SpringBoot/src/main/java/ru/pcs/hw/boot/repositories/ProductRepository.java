package ru.pcs.hw.boot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.hw.boot.models.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
