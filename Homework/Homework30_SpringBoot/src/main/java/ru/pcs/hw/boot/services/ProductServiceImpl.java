package ru.pcs.hw.boot.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.hw.boot.dto.ProductDto;
import ru.pcs.hw.boot.models.Product;
import ru.pcs.hw.boot.repositories.ProductRepository;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public void addProduct(ProductDto productDto) {
        productRepository.save(Product.builder()
                .name(productDto.getName())
                .amount(productDto.getAmount().intValue())
                .price(productDto.getPrice())
                .build());
    }
}
