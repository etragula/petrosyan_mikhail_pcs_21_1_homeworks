package ru.pcs.hw.boot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.hw.boot.validators.IsInteger;
import ru.pcs.hw.boot.validators.NoDigits;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDto {
    @NotBlank
    @NoDigits
    private String name;
    @NotNull
    @Positive
    private Float price;
    @NotNull
    @IsInteger
    @PositiveOrZero
    private Number amount;
}
