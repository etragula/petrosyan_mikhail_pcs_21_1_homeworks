package homework;


import homework.generator.IdGenerator;
import homework.generator.IdGeneratorImpl;
import homework.model.User;
import homework.repository.UsersRepository;
import homework.repository.UsersRepositoryImpl;
import homework.service.UsersServiceImp;

import java.util.UUID;

public class App {

    public static void main(String[] args) {
        IdGenerator idGenerator = new IdGeneratorImpl("Homework/Homework05_Repository/ids.txt");
        UsersRepository usersRepository = new UsersRepositoryImpl("Homework/Homework05_Repository/users.txt", idGenerator);
        UsersServiceImp usersServiceImp = new UsersServiceImp(usersRepository);

        String eMail = UUID.randomUUID() + "@mail.ru";
        String password = "qwerty12345";

        usersServiceImp.signUp(eMail, password);

        for (User user : usersRepository.findAll()) {
            System.out.println(user.toString());
        }

    }
}
