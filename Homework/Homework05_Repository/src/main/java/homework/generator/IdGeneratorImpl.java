package homework.generator;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class IdGeneratorImpl implements IdGenerator {

    private final String fileName;

    public IdGeneratorImpl(String fileName) {
        this.fileName = fileName;
    }


    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public Integer next() {
        try (Scanner scanner = new Scanner(new FileInputStream(fileName))) {
            int lastId = scanner.nextInt();
            writeNewLastId(++lastId);
            return lastId;
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void clean() {
        writeNewLastId(0);
    }

    private void writeNewLastId(int lastId) {
        Path path = Paths.get(fileName);
        try {
            Files.write(path, ("" + lastId).getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }
}
