package homework.generator;

import java.util.Iterator;

public interface IdGenerator extends Iterator<Integer> {
    void clean();
}
