package homework.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class User {

    private Integer id;
    private String eMail;
    private char[] password;

    public User(String eMail, char[] password) {
        this.eMail = eMail;
        this.password = password;
    }
}
