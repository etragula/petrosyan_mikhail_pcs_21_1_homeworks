package homework.repository;

import homework.generator.IdGenerator;
import homework.model.User;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.nio.file.StandardOpenOption.APPEND;

public class UsersRepositoryImpl implements UsersRepository {

    private final String fileName;
    private final IdGenerator idGenerator;

    public UsersRepositoryImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    @Override
    public void save(User user) {
        user.setId(idGenerator.next());

        String newUserInfo =
                user.getId() + "|" +
                        user.getEMail() + "|" +
                        new String(user.getPassword()) + "\n";

        addNewUserIntoFile(newUserInfo);
    }

    @Override
    public List<User> findAll() {
        return getAllUsersFromFile();
    }

    @Override
    public int count() {
        return findAll().size();
    }

    @Override
    public void delete(User user) {
        List<User> users = findAll();
        users.removeIf(user1 -> user1.getEMail().equals(user.getEMail()));

        cleanTheFile();
        idGenerator.clean();

        users.forEach(this::save);
    }

    @Override
    public Optional<User> findByEmail(String eMail) {
        List<User> users = findAll();

        for (User user : users)
            if (user.getEMail().equals(eMail)) return Optional.of(user);

        return Optional.empty();
    }

    @Override
    public boolean existByEmail(String eMail) {
        List<User> users = findAll();

        for (User user : users)
            if (user.getEMail().equals(eMail)) return true;

        return false;
    }

    private void addNewUserIntoFile(String newUserInfo) {
        try {
            Files.write(Paths.get(fileName), newUserInfo.getBytes(StandardCharsets.UTF_8), APPEND);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }

    private List<User> getAllUsersFromFile() {
        try {
            List<String> usersInfoList = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
            return getUsersEmailFromList(usersInfoList);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }

    private List<User> getUsersEmailFromList(List<String> usersInfoList) {
        List<User> userList = new ArrayList<>();

        for (String userInfo : usersInfoList) {
            String[] strings = userInfo.split("\\|");
            userList.add(new User(strings[1], strings[2].toCharArray()));
        }
        return userList;
    }

    private void cleanTheFile() {
        try {
            Files.write(Paths.get(fileName), "".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }
}
