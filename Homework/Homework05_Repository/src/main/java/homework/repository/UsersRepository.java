package homework.repository;

import homework.model.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository {
    Optional <User> findByEmail(String eMail);
    List <User> findAll();
    int count();
    void save(User user);
    void delete(User user);
    boolean existByEmail(String eMail);
}
