package homework.service;

import homework.errors.BadEmailException;
import homework.errors.BadPasswordException;
import homework.model.User;
import homework.repository.UsersRepository;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Optional;

import static homework.service.validator.UserInfoValidator.validateEmail;
import static homework.service.validator.UserInfoValidator.validatePassword;

@Slf4j
public class UsersServiceImp implements UsersService {

    private final UsersRepository usersRepository;

    public UsersServiceImp(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public void signUp(String email, String password) {
        if (validateUserData(email, password) && !usersRepository.existByEmail(email)) {
            usersRepository.save(new User(email, password.toCharArray()));
            log.info("Пользователь {} был успешно зарегестрирован!", email);
        } else throw new BadEmailException("Пользователь с данным Email-адресом уже зарегестрирован!");
    }

    @Override
    public void signIn(String email, String password) {
        if (validateUserData(email, password)) {
            Optional<User> userOptional = usersRepository.findByEmail(email);
            if (userOptional.isPresent()) {
                User user = userOptional.get();
                checkUserPassword(user, password);
                doSomething(user);
                return;
            }
        }
        throw new BadPasswordException();
    }

    @Override
    public void deleteUser(String email, String password) {
        if (validateUserData(email, password) && usersRepository.existByEmail(email)) {
            User user = new User(email, password.toCharArray());
            checkUserPassword(user, password);
            usersRepository.delete(user);
            log.info("Пользователь {} был успешно удален!", email);
        } else throw new BadPasswordException();
    }

    private boolean validateUserData(String email, String password) {
        return validateEmail(email) && validatePassword(password);
    }

    private void checkUserPassword(User user, String password) {
        String actualPassword = Arrays.toString(user.getPassword());
        if (actualPassword.equals(password)) return;
        else throw new BadPasswordException();
    }

    private void doSomething(User user) {
        System.out.println("Добро пожаловать!");
        log.info("Пользователь {} совершил вход в систему.", user.getEMail());
    }
}
