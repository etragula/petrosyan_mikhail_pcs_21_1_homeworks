package homework.service;

public interface UsersService {

    void signUp(String email, String password);

    void signIn(String email, String password);

    void deleteUser(String email, String password);
}

