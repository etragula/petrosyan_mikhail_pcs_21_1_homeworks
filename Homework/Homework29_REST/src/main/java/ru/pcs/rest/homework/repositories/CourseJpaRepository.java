package ru.pcs.rest.homework.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.rest.homework.models.Course;

public interface CourseJpaRepository extends JpaRepository<Course, Long> {
    Course findByTitle(String title);
}
