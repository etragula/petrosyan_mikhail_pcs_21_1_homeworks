package ru.pcs.rest.homework.services;

import ru.pcs.rest.homework.dto.LessonDto;

import java.util.List;

public interface LessonService {

    List<LessonDto> getAll();

    LessonDto createLesson(LessonDto lessonDto);

    LessonDto updateLesson(Long id, LessonDto lessonDto);

    void deleteLesson(Long id);

    void addLessonToCourse(Long lessonId, Long courseId);

    void deleteLessonFromCourse(Long lessonId);
}
