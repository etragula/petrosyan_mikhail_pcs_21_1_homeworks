package ru.pcs.rest.homework.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.rest.homework.models.Lesson;

public interface LessonJpaRepository extends JpaRepository<Lesson, Long> {
    Lesson findByName(String name);
}
