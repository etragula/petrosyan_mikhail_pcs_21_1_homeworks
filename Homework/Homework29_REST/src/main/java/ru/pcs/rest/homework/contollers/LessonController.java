package ru.pcs.rest.homework.contollers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.pcs.rest.homework.dto.LessonDto;
import ru.pcs.rest.homework.services.LessonServiceImpl;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/lessons")
@RequiredArgsConstructor
public class LessonController {

    private final LessonServiceImpl lessonServiceImpl;

    @GetMapping
    public List<LessonDto> getLessons() {
        return lessonServiceImpl.getAll();
    }

    @PostMapping
    public LessonDto createLesson(@RequestBody LessonDto lessonDto) {
        return lessonServiceImpl.createLesson(lessonDto);
    }

    @PutMapping("/{lesson-id}")
    public LessonDto updateLesson(@PathVariable("lesson-id") Long id, @RequestBody LessonDto lessonDto) {
        return lessonServiceImpl.updateLesson(id, lessonDto);
    }

    @DeleteMapping("/{lesson-id}")
    public void deleteLesson(@PathVariable("lesson-id") Long id) {
        lessonServiceImpl.deleteLesson(id);
    }

    @PostMapping("/addLessonToCourse")
    public void addLessonToCourse(Long lessonId, Long courseId) {
        lessonServiceImpl.addLessonToCourse(lessonId, courseId);
    }

    @PostMapping("/deleteLessonFromCourse")
    public void deleteLessonFromCourse(Long lessonId, Long courseId) {
        lessonServiceImpl.deleteLessonFromCourse(lessonId);
    }
}
