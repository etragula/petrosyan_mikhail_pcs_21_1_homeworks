package ru.pcs.rest.homework.contollers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.pcs.rest.homework.dto.CourseDto;
import ru.pcs.rest.homework.services.CourseServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor
public class CourseController {

    private final CourseServiceImpl courseServiceImpl;

    @GetMapping
    public List<CourseDto> getCourses() {
        return courseServiceImpl.getAll();
    }

    @PostMapping
    public CourseDto createCourse(@RequestBody CourseDto courseDto) {
        return courseServiceImpl.createCourse(courseDto);
    }

    @PutMapping("/{course-id}")
    public CourseDto updateCourse(@PathVariable("course-id") Long id, @RequestBody CourseDto courseDto) {
        return courseServiceImpl.updateCourse(id, courseDto);
    }

    @DeleteMapping("/{course-id}")
    public void deleteCourse(@PathVariable("course-id") Long id) {
        courseServiceImpl.deleteCourse(id);
    }

}
