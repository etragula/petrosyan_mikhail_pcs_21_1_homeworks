package ru.pcs.rest.homework.services;

import ru.pcs.rest.homework.dto.CourseDto;

import java.util.List;

public interface CourseService {

    List<CourseDto> getAll();

    CourseDto createCourse(CourseDto courseDto);

    CourseDto updateCourse(Long id, CourseDto courseDto);

    void deleteCourse(Long id);
}
