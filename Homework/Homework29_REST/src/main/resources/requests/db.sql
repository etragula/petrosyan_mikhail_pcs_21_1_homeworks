create table course
(
    id    bigserial
        constraint course_pkey
            primary key,
    title varchar(255)
);

create table lesson
(
    id        bigserial
        constraint lesson_pkey
            primary key,
    name      varchar(255),
    course_id bigint
        constraint course.id
            references course
);