package ru.pcs.boot.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<ValidPassword, String> {
    @Override
    public boolean isValid(String v, ConstraintValidatorContext constraintValidatorContext) {
        return v.contains("!") || v.contains("@") || v.contains("#");
    }
}
