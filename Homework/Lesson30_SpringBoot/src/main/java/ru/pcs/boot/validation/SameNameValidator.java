package ru.pcs.boot.validation;

import ru.pcs.boot.dto.SignUpForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SameNameValidator implements ConstraintValidator<NotSameNames, SignUpForm> {

    @Override
    public boolean isValid(SignUpForm signUpForm, ConstraintValidatorContext constraintValidatorContext) {
        return !signUpForm.getFirstName().equals(signUpForm.getLastName());
    }
}
