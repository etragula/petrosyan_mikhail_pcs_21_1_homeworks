package logging.homework.repositories;

import logging.homework.models.Product;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class ProductRepositoryJdbcImpl implements ProductRepository {

    public static final String SQL_INSERT = "insert into product(name, price, quantity) values (?, ?, ?);";
    public static final String SQL_DELETE_BY_ID = "delete from product where id = ?;";
    public static final String SQL_SELECT_BY_ID = "select * from product where id = ?;";
    public static final String SQL_UPDATE = "update product set name = ?, price = ?, quantity = ? where id = ?;";
    public static final String SQL_FIND_ALL = "select * from product order by id limit ? offset ?;";

    private final DataSource dataSource;

    private static final Function<ResultSet, Product> productMapper = resultSet -> {
        try {
            return new Product(
                    resultSet.getLong("id"),
                    resultSet.getString("name"),
                    resultSet.getDouble("price"),
                    resultSet.getInt("quantity")
            );
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    public ProductRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Product save(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, product.getName());
            statement.setDouble(2, product.getPrice());
            statement.setInt(3, product.getQuantity());
            final int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) throw new SQLException("Can't insert product.");
            final ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                product.setId(generatedKeys.getLong("id"));
                return product;
            } else throw new SQLException("Can't insert product.");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, product.getName());
            statement.setDouble(2, product.getPrice());
            statement.setInt(3, product.getQuantity());
            statement.setLong(4, product.getId());
            final int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) throw new SQLException("Can't update product.");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID)) {
            statement.setLong(1, id);
            final int i = statement.executeUpdate();
            if (i != 1) throw new IllegalArgumentException();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Product product) {
        deleteById(product.getId());
    }

    @Override
    public Optional<Product> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(productMapper.apply(resultSet));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Product> findAll(int page, int size) {
        List<Product> products = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL)) {
            statement.setInt(1, size);
            statement.setInt(2, page * size);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    products.add(productMapper.apply(resultSet));
                }
                return products;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
