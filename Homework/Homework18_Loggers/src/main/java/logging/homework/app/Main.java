package logging.homework.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import logging.homework.config.Config;
import logging.homework.repositories.ProductRepository;
import logging.homework.repositories.ProductRepositoryJdbcImpl;
import org.apache.log4j.Logger;

@Parameters(separators = "=")
class Main {

    @Parameter(names = {"--hikari-pool-size"})
    private int poolSize;

    private final Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        Main main = new Main();
        main.logger.info("Starting app...");
        Config config = new Config();
        HikariConfig hikariConfig = new HikariConfig();
        main.logger.info("Reading pool size from the argline...");
        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);

        main.logger.info("Reading database connection info from application properties...");
        main.logger.info("config.poolSize = " + main.poolSize);
        main.logger.info("config.Url = " + config.getUrl());
        main.logger.info("config.User = " + config.getUser());

        hikariConfig.setMaximumPoolSize(main.poolSize);
        hikariConfig.setJdbcUrl(config.getUrl());
        hikariConfig.setUsername(config.getUser());
        hikariConfig.setPassword(config.getPassword());

        main.logger.info("Creating hikari datasource...");
        HikariDataSource hikariDataSource = new HikariDataSource(hikariConfig);
        ProductRepository productRepository = new ProductRepositoryJdbcImpl(hikariDataSource);

        System.out.println(productRepository.findById(123426L).get());
    }
}