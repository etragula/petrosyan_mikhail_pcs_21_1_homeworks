package logging.lesson;

public class Main {

    public static void main(String[] args) {
        Clients clients = new Clients();
        clients.newClient(() -> System.out.println("Hello"));
        clients.newClient(() -> System.out.println("Hello"));
        clients.newClient(() -> System.out.println("Hello"));
        clients.newClient(() -> System.out.println("Hello"));
        clients.run();
        clients.stop();
    }
}
