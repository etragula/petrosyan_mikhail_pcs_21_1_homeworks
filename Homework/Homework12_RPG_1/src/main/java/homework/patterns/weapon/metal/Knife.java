package homework.patterns.weapon.metal;

import homework.patterns.weapon.WeaponBase;
import homework.patterns.weapon.specs.Damage;
import homework.patterns.weapon.specs.Rarity;

public class Knife extends WeaponBase {

    private String colour;

    public Knife(String colour) {
        this.colour = colour;
        super.rarity = Rarity.USUAL;
        super.damage = Damage.AVERAGE;
    }

    public String getColour() {
        return colour;
    }

    public Damage getDamage() {
        return super.damage;
    }

    public Rarity getRarity() {
        return super.rarity;
    }

    @Override
    public Knife clone() {
        return new Knife(this.colour);
    }

    @Override
    public String toString() {
        return "Knife{" +
                "damage=" + damage +
                ", rarity=" + rarity +
                ", colour='" + colour + '\'' +
                '}';
    }
}
