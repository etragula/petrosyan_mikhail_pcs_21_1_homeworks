package homework.patterns.weapon;

public interface WeaponFactory {

    Weapon createWeapon();
}
