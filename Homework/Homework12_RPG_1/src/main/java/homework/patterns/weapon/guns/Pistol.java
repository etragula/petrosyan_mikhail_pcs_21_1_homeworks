package homework.patterns.weapon.guns;

import homework.patterns.weapon.WeaponBase;
import homework.patterns.weapon.specs.Accuracy;
import homework.patterns.weapon.specs.Damage;
import homework.patterns.weapon.specs.Rarity;

public class Pistol extends WeaponBase {

    private String colour;
    private Double caliber;
    private Accuracy accuracy;
    private Integer ammoAmount;

    public static class Builder {
        private String colour;
        private Double caliber;
        private Integer ammoAmount;

        public Builder colour(String colour) {
            this.colour = colour;
            return this;
        }

        public Builder caliber(Double caliber) {
            this.caliber = caliber;
            return this;
        }

        public Builder ammoAmount(Integer ammoAmount) {
            this.ammoAmount = ammoAmount;
            return this;
        }

        public Pistol build() {
            return new Pistol(this);
        }

    }

    public Pistol() {
        super.damage = Damage.AVERAGE;
        super.rarity = Rarity.UNUSUAL;
        this.accuracy = Accuracy.HIGH;
    }

    private Pistol(Builder builder) {
        super.damage = Damage.LOW;
        super.rarity = Rarity.RARE;
        this.accuracy = Accuracy.LOW;
        this.colour = builder.colour;
        this.caliber = builder.caliber;
        this.ammoAmount = builder.ammoAmount;
    }

    public String getColour() {
        return colour;
    }

    public Double getCaliber() {
        return caliber;
    }

    public Damage getDamage() {
        return super.damage;
    }

    public Rarity getRarity() {
        return super.rarity;
    }

    public Accuracy getAccuracy() {
        return accuracy;
    }

    public Integer getAmmoAmount() {
        return ammoAmount;
    }

    @Override
    public Pistol clone() {
        Pistol clone = new Pistol();
        clone.accuracy = this.accuracy;
        clone.ammoAmount = this.ammoAmount;
        clone.caliber = this.caliber;
        clone.colour = this.colour;
        return clone;
    }

    @Override
    public String toString() {
        return "Pistol{" +
                "damage=" + damage +
                ", rarity=" + rarity +
                ", accuracy=" + accuracy +
                ", ammoAmount=" + ammoAmount +
                ", caliber=" + caliber +
                ", colour='" + colour + '\'' +
                '}';
    }
}
