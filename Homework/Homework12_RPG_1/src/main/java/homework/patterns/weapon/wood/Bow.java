package homework.patterns.weapon.wood;

import homework.patterns.weapon.WeaponBase;
import homework.patterns.weapon.specs.Accuracy;
import homework.patterns.weapon.specs.Damage;
import homework.patterns.weapon.specs.Rarity;

public class Bow extends WeaponBase {

    private final String colour;
    private final Accuracy accuracy;

    public Bow(String colour) {
        this.colour = colour;
        super.damage = Damage.LOW;
        super.rarity = Rarity.RARE;
        this.accuracy = Accuracy.LOW;
    }

    public String getColour() {
        return colour;
    }

    public Damage getDamage() {
        return super.damage;
    }

    public Rarity getRarity() {
        return super.rarity;
    }

    public Accuracy getAccuracy() {
        return accuracy;
    }

    @Override
    public Bow clone() {
        return new Bow(this.colour);
    }

    @Override
    public String toString() {
        return "Bow{" +
                "damage=" + damage +
                ", rarity=" + rarity +
                ", colour='" + colour + '\'' +
                ", accuracy=" + accuracy +
                '}';
    }
}
