package homework.patterns.weapon.wood;

import homework.patterns.weapon.Weapon;
import homework.patterns.weapon.WeaponFactory;

public class BowFactory implements WeaponFactory {
    @Override
    public Weapon createWeapon() {
        return new Bow("black");
    }
}
