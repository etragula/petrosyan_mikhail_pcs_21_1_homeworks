package homework.patterns.weapon;

import homework.patterns.weapon.specs.Damage;
import homework.patterns.weapon.specs.Rarity;

public abstract class WeaponBase implements Cloneable, Weapon {

    protected Damage damage;
    protected Rarity rarity;
}
