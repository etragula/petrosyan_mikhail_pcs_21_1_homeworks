package homework.patterns.weapon.specs;

public enum Rarity {
    USUAL, UNUSUAL, RARE, LEGEND;
}
