package homework.patterns.weapon.guns;

import homework.patterns.weapon.Weapon;
import homework.patterns.weapon.WeaponFactory;

public class PistolFactory implements WeaponFactory {
    @Override
    public Weapon createWeapon() {
        return new Pistol.Builder()
                .colour("black")
                .caliber(7.2)
                .ammoAmount(18)
                .build();
    }
}
