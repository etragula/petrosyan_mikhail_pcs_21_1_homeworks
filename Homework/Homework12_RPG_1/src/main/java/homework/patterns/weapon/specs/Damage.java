package homework.patterns.weapon.specs;

public enum Damage {
    LOW, AVERAGE, HIGH, ABSOLUTE;
}
