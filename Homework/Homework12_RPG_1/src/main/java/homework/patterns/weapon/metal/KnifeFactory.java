package homework.patterns.weapon.metal;

import homework.patterns.weapon.Weapon;
import homework.patterns.weapon.WeaponFactory;

public class KnifeFactory implements WeaponFactory {
    @Override
    public Weapon createWeapon() {
        return new Knife("brown");
    }
}
