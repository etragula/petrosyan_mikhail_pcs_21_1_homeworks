package homework.patterns;

import homework.patterns.weapon.WeaponFactory;
import homework.patterns.weapon.guns.Pistol;
import homework.patterns.weapon.guns.PistolFactory;
import homework.patterns.weapon.metal.Knife;
import homework.patterns.weapon.metal.KnifeFactory;
import homework.patterns.weapon.wood.Bow;
import homework.patterns.weapon.wood.BowFactory;

public class App {

    public static void main(String[] args) {
        WeaponFactory pistolFactory = new PistolFactory();
        Pistol pistol = (Pistol) pistolFactory.createWeapon();
        System.out.println(pistol.toString());

        WeaponFactory bowFactory = new BowFactory();
        Bow bow = (Bow) bowFactory.createWeapon();
        System.out.println(bow.toString());

        WeaponFactory knifeFactory = new KnifeFactory();
        Knife knife = (Knife) knifeFactory.createWeapon();
        System.out.println(knife.toString());
    }
}
