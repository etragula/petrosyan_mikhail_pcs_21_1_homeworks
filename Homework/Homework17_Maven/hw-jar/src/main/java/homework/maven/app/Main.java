package homework.maven.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import homework.maven.config.Config;
import homework.maven.repositories.ProductRepository;
import homework.maven.repositories.ProductRepositoryJdbcImpl;

@Parameters(separators = "=")
class Main {

    @Parameter(names = {"--hikari-pool-size"})
    private int poolSize;

    public static void main(String[] args) {
        Main main = new Main();
        Config config = new Config();
        HikariConfig hikariConfig = new HikariConfig();
        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);

        System.out.println("config.Url  = " + main.poolSize);
        System.out.println("config.Url  = " + config.getUrl());
        System.out.println("config.User = " + config.getUser());

        hikariConfig.setMaximumPoolSize(main.poolSize);
        hikariConfig.setJdbcUrl(config.getUrl());
        hikariConfig.setUsername(config.getUser());
        hikariConfig.setPassword(config.getPassword());

        HikariDataSource hikariDataSource = new HikariDataSource(hikariConfig);
        ProductRepository productRepository = new ProductRepositoryJdbcImpl(hikariDataSource);

        System.out.println(productRepository.findById(123426L));
    }
}
