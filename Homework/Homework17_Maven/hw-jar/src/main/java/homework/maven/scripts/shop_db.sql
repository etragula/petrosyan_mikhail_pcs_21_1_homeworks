-- Удаление таблиц --
DROP TABLE IF EXISTS invoice;
DROP TABLE IF EXISTS order_booking;
DROP TABLE IF EXISTS "order";
DROP TABLE IF EXISTS product_booking;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS customer;


-- Создание таблиц --
CREATE TABLE IF NOT EXISTS customer
(
    id         SERIAL
        constraint customer_pk
            primary key,
    first_name varchar not null,
    last_name  varchar not null,
    address    varchar not null,
    contact    varchar not null
);

CREATE TABLE IF NOT EXISTS product
(
    id       serial
        constraint product_pk
            primary key,
    name     varchar not null,
    price    double precision not null,
    quantity integer
);

-- drop table if exists "order" cascade;
CREATE TABLE IF NOT EXISTS product_booking
(
    id          serial
        constraint product_booking_pk
            primary key,
    customer_id integer
        constraint product_booking_customer_id_fk
            references customer,
    product_id  integer
        constraint product_booking_product_id_fk
            references product,
    amount      integer
);

-- drop table if exists product_booking cascade;
CREATE TABLE IF NOT EXISTS "order"
(
    id                 serial
        constraint order_pk
            primary key,
    product_booking_id integer          not null
        constraint order_product_booking_id_fk
            references product_booking,
    "totalPrice"       double precision not null
);

-- drop table if exists customer cascade;
CREATE TABLE IF NOT EXISTS order_booking
(
    id       serial
        constraint order_booking_pk
            primary key,
    order_id integer not null
        constraint order_booking_order_id_fk
            references "order"
);

-- drop table if exists product cascade;
CREATE TABLE IF NOT EXISTS invoice
(
    id               serial
        constraint invoice_pk
            primary key,
    kpp              varchar(12) default '223414354312'::bigint,
    inn              varchar(12) default '227812315421'::bigint,
    order_booking_id integer
        constraint invoice_order_booking_id_fk
            references order_booking
);


-- Заполнение таблиц --
INSERT INTO customer (id, first_name, last_name, address, contact)
VALUES (567987, 'Andrey', 'Khamenok', 'Moscow, Lubyanka 2', '89433765292');
INSERT INTO customer (id, first_name, last_name, address, contact)
VALUES (234234, 'Pavel', 'Potehin', 'Moscow, Bolotnikovskya 30', '89253205874');
INSERT INTO customer (id, first_name, last_name, address, contact)
VALUES (325243, 'Mikhail', 'Petrosyan', 'Moscow, Kutuzovskiy prospect 32', '88471936533');

INSERT INTO product (id, name, price, quantity)
VALUES (123425, 'Protein', 3650.00, 17);
INSERT INTO product (id, name, price, quantity)
VALUES (123426, 'Protein 2.0', 5400.00, 26);
INSERT INTO product (id, name, price, quantity)
VALUES (112753, 'Casein', 3799.00, 15);
INSERT INTO product (id, name, price, quantity)
VALUES (112754, 'Casein 2.0', 6000.00, 10);
INSERT INTO product (id, name, price, quantity)
VALUES (151512, 'Gainer', 3500, 24);
INSERT INTO product (id, name, price, quantity)
VALUES (151513, 'Gainer 2.0', 5500, 18);

INSERT INTO product_booking (id, product_id, customer_id, amount)
VALUES (1001,
        (SELECT id FROM product WHERE id = 112754),
        (SELECT id FROM customer WHERE id = 567987),
        2);
INSERT INTO product_booking (id, product_id, customer_id, amount)
VALUES (1002,
        (SELECT id FROM product WHERE id = 151513),
        (SELECT id FROM customer WHERE id = 234234),
        1);
INSERT INTO product_booking (id, product_id, customer_id, amount)
VALUES (1003,
        (SELECT id FROM product WHERE id = 123425),
        (SELECT id FROM customer WHERE id = 325243),
        3);

INSERT INTO "order" (id, product_booking_id, "totalPrice")
VALUES (4001,
        1001,
        (SELECT amount FROM product_booking WHERE id = 1001) *
        (SELECT price
         FROM product
                  JOIN product_booking
                       ON product.id = product_booking.product_id
         WHERE product_booking.id = 1001));
INSERT INTO "order" (id, product_booking_id, "totalPrice")
VALUES (4002,
        1002,
        (SELECT amount FROM product_booking WHERE id = 1002) *
        (SELECT price
         FROM product
                  JOIN product_booking
                       ON product.id = product_booking.product_id
         WHERE product_booking.id = 1002));
INSERT INTO "order" (id, product_booking_id, "totalPrice")
VALUES (4003,
        1003,
        (SELECT amount FROM product_booking WHERE id = 1003) *
        (SELECT price
         FROM product
                  JOIN product_booking
                       ON product.id = product_booking.product_id
         WHERE product_booking.id = 1003));

INSERT INTO order_booking (id, order_id)
VALUES (7890,
        4001);
INSERT INTO order_booking (id, order_id)
VALUES (7891,
        4002);
INSERT INTO order_booking (id, order_id)
VALUES (7892,
        4003);

INSERT INTO invoice (id, kpp, inn, order_booking_id)
VALUES (578,
        DEFAULT,
        DEFAULT,
        7890);
INSERT INTO invoice (id, kpp, inn, order_booking_id)
VALUES (579,
        DEFAULT,
        DEFAULT,
        7891);
INSERT INTO invoice (id, kpp, inn, order_booking_id)
VALUES (580,
        DEFAULT,
        DEFAULT,
        7892);


-- Сложные запросы --
-- 1. Получить данные клиента клиента по номеру чека.
SELECT first_name, last_name, address, contact
FROM customer
         JOIN product_booking pb
              ON customer.id = pb.customer_id
WHERE pb.id = (
    SELECT pb.id
    FROM product_booking pb
             JOIN "order" o
                  ON pb.id = o.product_booking_id
    WHERE o.id = (
        SELECT ob.order_id
        FROM order_booking ob
                 JOIN invoice i
                      ON ob.id = i.order_booking_id
        WHERE i.id = 580
    ));

-- 2. Получить номер бронимарования товара, артикул и количество по номеру бронирования заказа.
SELECT id, product_id, amount
FROM product_booking
WHERE id =
      (SELECT product_booking_id
       FROM "order"
       WHERE "order".id = (
           SELECT order_id
           FROM order_booking
           WHERE order_booking.id = 7892));
