package homework.maven.repositories;


import homework.maven.models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductRepository {

    Optional<Product> findById(Long id);

    List<Product> findAll(int page, int size);

    Product save(Product product);

    void update(Product product);

    void delete(Product product);

    void deleteById(Long id);
}
