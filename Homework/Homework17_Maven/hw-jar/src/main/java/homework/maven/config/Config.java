package homework.maven.config;

import java.io.FileInputStream;
import java.util.Properties;

public class Config {
    private final String user;
    private final String password;
    private final String url;

    public Config() {
        try {
            Properties properties = new Properties();
            final FileInputStream stream =
                    new FileInputStream("Homework/Homework17_Maven/hw-jar/src/main/resources/application.properties");
            properties.load(stream);
            this.user = properties.getProperty("db.user");
            this.password = properties.getProperty("db.password");
            this.url = properties.getProperty("db.url");
            stream.close();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getUrl() {
        return url;
    }
}
