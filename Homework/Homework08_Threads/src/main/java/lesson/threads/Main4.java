package lesson.threads;

import java.util.concurrent.*;


public class Main4 {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        CsvFileProcessor processor = new CsvFileProcessor("Homework/Homework08_Threads/src/main/resources/data.csv");

        Callable<Long> task = () -> processor.countByPosition(3, "2");

        Future<Long> submit = executorService.submit(task);


        try {
            System.out.println(submit.get());
        } catch (InterruptedException | ExecutionException exception) {
            throw new IllegalArgumentException();
        }
    }
}
