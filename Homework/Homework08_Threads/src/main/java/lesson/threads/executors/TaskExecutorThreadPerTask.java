package lesson.threads.executors;

public class TaskExecutorThreadPerTask implements TaskExecutor {
    @Override
    public void submit(Runnable task) {
        Thread thread = new Thread(task);
        thread.start();
    }
}
