package lesson.threads.executors;

import java.util.Deque;
import java.util.LinkedList;

public class TaskExecutorWorkingThread implements TaskExecutor {

    // очередь задач
    private final Deque<Runnable> tasks;

    private final WorkingThread workingThread;

    public TaskExecutorWorkingThread() {
        this.tasks = new LinkedList<>();
        this.workingThread = new WorkingThread();
        this.workingThread.start();
    }

    private class WorkingThread extends Thread {
        @Override
        public void run() {
            while (true) {
                synchronized (tasks) {
                    while (tasks.isEmpty()) {
                        try {
                            // пока задач для нас нет - уходим в ожидание
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalArgumentException();
                        }
                    }
                    // забираем задачу из очереди
                    Runnable task = tasks.poll();
                    task.run();
                }
            }
        }
    }

    @Override
    public void submit(Runnable task) {
        // блокируем все действия при добавлении новой задачи
        synchronized (tasks) {
            tasks.add(task);
            // как только добавили задачу в очередь - оповестили ожидающий поток
            tasks.notify();
        }
    }
}
