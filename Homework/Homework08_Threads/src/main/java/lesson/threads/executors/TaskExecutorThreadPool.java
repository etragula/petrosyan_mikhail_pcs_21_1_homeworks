package lesson.threads.executors;

import java.util.Deque;
import java.util.LinkedList;

public class TaskExecutorThreadPool implements TaskExecutor {

    // очередь задач
    private final Deque<Runnable> tasks;

    // массив параллельных потоков
    private final WorkingThread[] workingThreads;

    public TaskExecutorThreadPool(int count) {
        this.tasks = new LinkedList<>();
        this.workingThreads = new WorkingThread[count];
        for (int i = 0; i < count; i++) {
            this.workingThreads[i] = new WorkingThread();
        }
        for (WorkingThread thread : workingThreads) {
            thread.start();
        }
    }

    private class WorkingThread extends Thread {
        @Override
        public void run() {
            Runnable currenTask;
            while (true) {
                synchronized (tasks) {
                    while (tasks.isEmpty()) {
                        try {
                            // пока задач для нас нет - уходим в ожидание
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalArgumentException();
                        }
                    }
                    // забираем задачу из очереди
                    currenTask = tasks.poll();
                }
                currenTask.run();
            }
        }
    }

    @Override
    public void submit(Runnable task) {
        // блокируем все действия при добавлении новой задачи
        synchronized (tasks) {
            tasks.add(task);
            // как только добавили задачу в очередь - оповестили ожидающий поток
            tasks.notify();
        }
    }
}
