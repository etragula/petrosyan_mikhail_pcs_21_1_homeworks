package lesson.threads.executors;

public interface TaskExecutor {
    void submit(Runnable task);
}
