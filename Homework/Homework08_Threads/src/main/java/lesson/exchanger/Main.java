package lesson.exchanger;

import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        Exchanger<List<String>> exchanger = new Exchanger<>();
        CsvFileProcessor csvFileProcessor = new CsvFileProcessor("");
        StatisticUtil statisticUtil = new StatisticUtil();

        Runnable runnable = () -> {
            csvFileProcessor.readValuesByPosition(2, exchanger, 100_000);
        };

        Callable<Set<String>> getDomEmail = () -> statisticUtil.getEmailDomains(exchanger);

        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(runnable);

        try {
            System.out.println(executorService.submit(getDomEmail).get(10, TimeUnit.SECONDS));
        } catch (TimeoutException e) {
            System.out.println("не дождались результата ");
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        executorService.shutdown();;
    }
}
