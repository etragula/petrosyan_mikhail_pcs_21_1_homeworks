package lesson.exchanger;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Exchanger;

public class StatisticUtil {

    public Set<String> getEmailDomains(Exchanger<List<String>> exchanger) {
        Set<String> result = new HashSet<>();
        List<String> buffer;

        do {

            System.out.println("Stat ожидание данных.");
            try {
                buffer = exchanger.exchange(null);

                if (buffer != null) {
                    System.out.println("Stat данные получены.");
                    result.addAll(buffer);
                    System.out.println("Stat данные обработаны.");
                }
            } catch (InterruptedException e) {
                throw new IllegalArgumentException();
            }
        } while (buffer != null);
        System.out.println("Stat работа завершена.");
        return result;
    }
}
