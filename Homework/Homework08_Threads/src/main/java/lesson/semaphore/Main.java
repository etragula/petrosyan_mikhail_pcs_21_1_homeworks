package lesson.semaphore;

import lesson.exchanger.CsvFileProcessor;

import java.util.Scanner;
import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) {
        CsvFileProcessor processor = new CsvFileProcessor("");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        Semaphore semaphore = new Semaphore(3);
        int citiesCount = scanner.nextInt();

        String city = scanner.nextLine();
        scanner.nextLine();
        int i = 0;

        while (i < citiesCount) {
            final String cityForTask = city;

            Runnable task = () -> {
                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    throw new IllegalArgumentException();
                }
                System.out.println(processor.countByPosition(30, cityForTask));
                semaphore.release();
            };


            new Thread(task).start();
            i++;
            city = scanner.nextLine();
        }
    }
}
