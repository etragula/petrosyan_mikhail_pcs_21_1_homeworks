package homework.threads;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class App {
    private static int counter;

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        List<String> links = getLinksFromFile();

        ExecutorService executor = Executors.newFixedThreadPool(15);
        List<Long> allSize = new ArrayList<>();
        List<Callable<Long>> todo = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            for (String link : links) {
                Callable<Long> callable = () -> downloadFileFromLink(link);
                todo.add(callable);
            }
        }
        List<Future<Long>> futures = executor.invokeAll(todo);
        for (Future<Long> future : futures) {
            allSize.add(future.get());
        }

        System.out.printf("%.2f megabytes", (getSum(allSize) / 1024.0) / 1024.0);
        executor.shutdown();
    }

    private static List<String> getLinksFromFile() {
        try {
            return Files.readAllLines(Paths.get("Homework/Homework08_Threads/src/main/resources/links.txt"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static long downloadFileFromLink(String link) {
        String file = "Homework/Homework08_Threads/src/main/resources/downloaded" + counter++ + ".csv";
        try {
            URL url = new URL(link);
            ReadableByteChannel rbc = Channels.newChannel(url.openStream());
            FileOutputStream fos = new FileOutputStream(file);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
            rbc.close();
            return Files.size(Paths.get(file));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static Long getSum(List<Long> allSize) {
        Long sum = 0L;
        for (Long aLong : allSize) {
            sum += aLong;
        }
        return sum;
    }
}
