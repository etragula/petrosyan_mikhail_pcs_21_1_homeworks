package homework.jdbc.repository.repositories;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import homework.jdbc.repository.models.Product;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ProductRepositoryJdbcImplTest {
    public static final String USER = "";
    public static final String PASSWORD = "";
    public static final String URL = "jdbc:postgresql://localhost:5433/template1";

    private static ProductRepository productRepository;

    @BeforeAll
    public static void doBeforeAll() {
        HikariConfig config = new HikariConfig();
        config.setMaximumPoolSize(10);
        config.setJdbcUrl(URL);
        config.setUsername(USER);
        config.setPassword(PASSWORD);

        HikariDataSource hikariDataSource = new HikariDataSource(config);
        productRepository = new ProductRepositoryJdbcImpl(hikariDataSource);
    }

    @Test
    void save() {
        final Product expectedProduct =
                productRepository.save(getProduct(0L, "ProductTest", 3140.23, 55));
        final Optional<Product> optionalProduct = productRepository.findById(expectedProduct.getId());
        final Product actualProduct = optionalProduct.get();
        assertProduct(expectedProduct, actualProduct);
    }

    @Test
    void update() {
        final Product newProduct =
                productRepository.save(getProduct(0L, "ProductTest", 3140.23, 55));
        final Product expectedProduct = getProduct(newProduct.getId(), "newName", 396.32, 642);
        productRepository.update(expectedProduct);
        final Optional<Product> optionalProduct = productRepository.findById(expectedProduct.getId());
        final Product actualProduct = optionalProduct.get();
        assertProduct(expectedProduct, actualProduct);
    }

    @Test
    void deleteById() {
        final Product productToDelete =
                productRepository.save(getProduct(0L, "ProductToDelete", 7580.32, 45));
        final Optional<Product> optionalProduct = productRepository.findById(productToDelete.getId());
        final Product actualProduct = optionalProduct.get();
        assertProduct(productToDelete, actualProduct);
        productRepository.delete(productToDelete);
        final Optional<Product> emptyProduct = productRepository.findById(productToDelete.getId());
        assertFalse(emptyProduct.isPresent());
    }

    @Test
    void findById() {
        final Product expectedProduct = getProduct(123426L, "Protein 2.0", 5400.0, 26);
        final Optional<Product> optionalProduct = productRepository.findById(123426L);
        final Product actualProduct = optionalProduct.get();
        assertProduct(expectedProduct, actualProduct);
    }

    private void assertProduct(Product expectedProduct, Product actualProduct) {
        assertNotNull(actualProduct);
        assertEquals(expectedProduct, actualProduct);
    }

    private Product getProduct(Long id, String name, Double price, Integer quantity) {
        return Product.builder()
                .id(id)
                .name(name)
                .price(price)
                .quantity(quantity)
                .build();
    }
}