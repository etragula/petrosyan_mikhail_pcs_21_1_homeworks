package homework.jdbc.repository;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import homework.jdbc.repository.models.Product;
import homework.jdbc.repository.repositories.ProductRepository;
import homework.jdbc.repository.repositories.ProductRepositoryJdbcImpl;

import java.util.Optional;

public class Main {

    public static final String USER = "";
    public static final String PASSWORD = "";
    public static final String URL = "jdbc:postgresql://localhost:5432/postgres";

    public static void main(String[] args) {
        HikariConfig config = new HikariConfig();
        config.setMaximumPoolSize(10);
        config.setJdbcUrl(URL);
        config.setUsername(USER);
        config.setPassword(PASSWORD);

        HikariDataSource hikariDataSource = new HikariDataSource(config);
        ProductRepository productRepository = new ProductRepositoryJdbcImpl(hikariDataSource);

        productRepository.save(Product.builder()
                .name("Some")
                .price(1431.31)
                .quantity(4)
                .build());


        final Optional<Product> byId = productRepository.findById(1L);
        Product product = null;
        if (byId.isPresent()) {
            product = byId.get();
            product.setName("new_Name");
            product.setQuantity(10);
            product.setPrice(134.13);

            productRepository.update(product);
        }

        productRepository.delete(Product.builder().id(7L).build());

        System.out.println(productRepository.findAll(0, 10));

    }
}
