package homework.jdbc.repository.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Product {

    private Long id;
    private String name;
    private Double price;
    private Integer quantity;
}
