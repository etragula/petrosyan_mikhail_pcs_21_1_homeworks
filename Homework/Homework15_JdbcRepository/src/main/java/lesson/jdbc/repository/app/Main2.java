package lesson.jdbc.repository.app;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lesson.jdbc.repository.repositories.StudentRepository;
import lesson.jdbc.repository.repositories.StudentRepositoryJdbcImpl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main2 {
    public static final String USER = "";
    public static final String PASSWORD = "";
    public static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    public static final String DRIVER = "org.postgresql.Driver";

    public static void main(String[] args) {
//        DataSource dataSource = new SingleConnectionDatasource(PASSWORD, URL, USER);
//        DataSource dataSource = new OnePerQueryDataSource(PASSWORD, URL, USER);

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(URL);
        config.setUsername(USER);
        config.setPassword(PASSWORD);
        config.setDriverClassName(DRIVER);
        config.setMaximumPoolSize(20);
        HikariDataSource dataSource = new HikariDataSource(config);

        StudentRepository studentRepository = new StudentRepositoryJdbcImpl(dataSource);

        ExecutorService executorService = Executors.newFixedThreadPool(10);


        for (int clientNumber = 0; clientNumber < 10; clientNumber++) {
            executorService.submit(() -> {
                for (int i = 0; i < 180; i++) {
                    try {
                        System.out.println(studentRepository.findAll(i, 100));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        executorService.shutdown();
    }
}
