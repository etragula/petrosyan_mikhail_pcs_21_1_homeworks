package lesson.jdbc.repository.app;


import lesson.jdbc.repository.repositories.StudentRepository;
import lesson.jdbc.repository.repositories.StudentRepositoryJdbcImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {
    public static final String USER = "";
    public static final String PASSWORD = "";
    public static final String URL = "jdbc:postgresql://localhost:5432/postgres";

    public static void main(String[] args) throws SQLException {
        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
            StudentRepository studentRepository = new StudentRepositoryJdbcImpl(null);

            System.out.println(studentRepository.findAll(1, 10));
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
