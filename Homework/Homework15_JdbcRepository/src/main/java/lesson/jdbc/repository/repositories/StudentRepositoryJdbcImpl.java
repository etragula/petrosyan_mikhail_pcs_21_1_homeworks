package lesson.jdbc.repository.repositories;

import lesson.jdbc.repository.models.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class StudentRepositoryJdbcImpl implements StudentRepository {

    public static final String SQL_INSERT = "insert into student(birth_date, age) values (?, ?);";
    public static final String SQL_DELETE_BY_ID = "delete from student where id = ?;";
    public static final String SQL_SELECT_BY_ID = "select id, birth_date, age from student where id = ?;";
    public static final String SQL_UPDATE = "update student set birth_date = ?, age = ? where id = ?;";
    public static final String SQL_FIND_ALL = "select * from student order by id limit ? offset ?;";

    private final DataSource dataSource;

    private static final Function<ResultSet, Student> studentMapper = resultSet -> {
        try {
            return new Student(
                    resultSet.getLong("id"),
                    LocalDate.parse(resultSet.getString("birth_date").split(" ")[0]),
                    resultSet.getObject("age", Integer.class)
            );
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    public StudentRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Student student) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setTimestamp(1, Timestamp.valueOf(LocalDateTime.of(student.getBirthDate(), LocalTime.now())));
            statement.setInt(2, student.getAge());
            final int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) throw new SQLException("Can't insert student.");
            final ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                student.setId(generatedKeys.getLong("id"));
            } else throw new SQLException("Can't insert student.");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Student student) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setTimestamp(1, Timestamp.valueOf(LocalDateTime.of(student.getBirthDate(), LocalTime.now())));
            statement.setInt(2, student.getAge());
            statement.setLong(3, student.getId());
            final int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) throw new SQLException("Can't update student.");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID)) {
            statement.setLong(1, id);
            final int i = statement.executeUpdate();
            if (i != 1) throw new IllegalArgumentException();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Student student) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID)) {
            statement.setLong(1, student.getId());
            final int i = statement.executeUpdate();
            if (i != 1) throw new IllegalArgumentException();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Student> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(
                            studentMapper.apply(resultSet)
                    );
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Student> findAll(int page, int size) {
        List<Student> students = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL)) {
            statement.setInt(1, size);
            statement.setInt(2, page * size);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    students.add(studentMapper.apply(resultSet));
                }
                return students;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
