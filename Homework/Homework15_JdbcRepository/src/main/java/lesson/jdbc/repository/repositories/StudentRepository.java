package lesson.jdbc.repository.repositories;

import lesson.jdbc.repository.models.Student;

import java.util.List;
import java.util.Optional;

public interface StudentRepository {

    void save(Student student);

    void update(Student student);

    void deleteById(Long id);

    void delete(Student student);

    Optional<Student> findById(Long id);

    List<Student> findAll(int page, int size);
}
