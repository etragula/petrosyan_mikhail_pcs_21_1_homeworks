package lesson.jdbc.repository.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class Student {
    private Long id;
    private LocalDate birthDate;
    private Integer age;

}
