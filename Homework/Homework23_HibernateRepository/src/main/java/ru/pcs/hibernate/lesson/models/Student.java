package ru.pcs.hibernate.lesson.models;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "courses")
public class Student {
    private Long id;
    private String firstName;
    private String lastName;

    private List<Course> courses;
}
