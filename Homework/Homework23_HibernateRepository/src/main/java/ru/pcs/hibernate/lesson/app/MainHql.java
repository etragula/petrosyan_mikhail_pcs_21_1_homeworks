package ru.pcs.hibernate.lesson.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import ru.pcs.hibernate.lesson.models.Student;

public class MainHql {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        final Query<Student> query = session.createQuery("select student from Student student " +
                "left join student.courses as course where course.lesson.size > 1", Student.class);

        System.out.println(query.getResultList());

        session.close();
        sessionFactory.close();
    }
}
