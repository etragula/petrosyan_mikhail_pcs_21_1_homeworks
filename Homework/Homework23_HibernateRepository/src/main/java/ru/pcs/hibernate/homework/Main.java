package ru.pcs.hibernate.homework;

import ru.pcs.hibernate.homework.models.File;
import ru.pcs.hibernate.homework.repository.FileRepository;
import ru.pcs.hibernate.homework.repository.FileRepositoryImpl;

public class Main {

    public static void main(String[] args) {
        FileRepository fileRepository = new FileRepositoryImpl("hibernate/hibernate.cfg.xml");

        final File file = File.builder()
                .originalFileName("test")
                .description("some test for file")
                .storageFileName("name")
                .size(1354134L)
                .mimeType("text")
                .build();
        final File file1 = File.builder()
                .originalFileName("original_name")
                .description("some file for test")
                .storageFileName("storage_name")
                .size(987797L)
                .mimeType("image")
                .build();

//        fileRepository.save(file1);
//        fileRepository.save(file);

        final File expectedFile = fileRepository.findByStorageName(file.getStorageFileName()).get();
        final File expectedFile1 = fileRepository.findByStorageName(file1.getStorageFileName()).get();

        System.out.println(expectedFile);
        System.out.println(expectedFile1);
    }
}
