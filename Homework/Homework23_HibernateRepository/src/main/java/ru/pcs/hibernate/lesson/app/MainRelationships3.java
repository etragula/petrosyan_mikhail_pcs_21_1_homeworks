package ru.pcs.hibernate.lesson.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.hibernate.lesson.models.Course;
import ru.pcs.hibernate.lesson.models.Student;

public class MainRelationships3 {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        Course java = session.load(Course.class, 1L);
        Course sql = session.load(Course.class, 2L);

        Student marsel = session.load(Student.class, 1L);
        Student victor = session.load(Student.class, 2L);

        marsel.getCourses().add(java);
        marsel.getCourses().add(sql);
        victor.getCourses().add(sql);
        victor.getCourses().add(sql);

        // todo: НЕ РАБОТАЕТ!
//        java.getStudents().add(marsel);
//        java.getStudents().add(victor);
//        sql.getStudents().add(marsel);
//        sql.getStudents().add(victor);

        session.getTransaction().commit();

        session.close();
        sessionFactory.close();
    }
}
