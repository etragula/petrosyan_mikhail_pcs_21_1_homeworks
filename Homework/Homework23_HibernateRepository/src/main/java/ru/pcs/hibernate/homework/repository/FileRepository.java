package ru.pcs.hibernate.homework.repository;

import ru.pcs.hibernate.homework.models.File;

import java.util.Optional;

public interface FileRepository {
    void save(File file);

    Optional<File> findByStorageName(String storageFileName);
}
