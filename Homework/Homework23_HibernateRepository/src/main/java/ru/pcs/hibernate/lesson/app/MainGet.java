package ru.pcs.hibernate.lesson.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.hibernate.lesson.models.Student;

public class MainGet {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

//        session.beginTransaction();     // транзакция открыта

        Student student = session.load(Student.class, 1L);
        System.out.println(student   );


//        session.getTransaction().commit();     // транзакция выполнена

        // в таблицу был добавлен объект с двумя заполненными полями
        session.close();
        sessionFactory.close();
    }
}
