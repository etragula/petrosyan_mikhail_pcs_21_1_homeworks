package ru.pcs.hibernate.lesson.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.hibernate.lesson.models.Student;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();     // транзакция открыта

        String firstName = "FirstName";
        Student student = Student.builder()
                .firstName(firstName)
                .build();
        session.persist(student);       // вызов метода
        String lastName = "LastName";
        student.setLastName(lastName);

        session.getTransaction().commit();     // транзакция выполнена
        // в таблицу был добавлен объект с двумя заполненными полями
        session.close();
        sessionFactory.close();
    }
}
