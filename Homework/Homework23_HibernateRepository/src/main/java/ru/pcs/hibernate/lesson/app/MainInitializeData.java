package ru.pcs.hibernate.lesson.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.hibernate.lesson.models.Course;
import ru.pcs.hibernate.lesson.models.Lesson;
import ru.pcs.hibernate.lesson.models.Student;

import java.util.Arrays;

public class MainInitializeData {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        Lesson spring = Lesson.builder()
                .name("Spring")
                .build();

        Lesson jvm = Lesson.builder()
                .name("JVM")
                .build();

        Lesson jdbc = Lesson.builder()
                .name("JDBC")
                .build();

        Lesson sql = Lesson.builder()
                .name("SQL")
                .build();

        Lesson db = Lesson.builder()
                .name("db")
                .build();

        Lesson exceptions = Lesson.builder()
                .name("exceptions")
                .build();

        session.persist(spring);
        session.persist(jvm);
        session.persist(jdbc);
        session.persist(sql);
        session.persist(db);
        session.persist(exceptions);

        Course java = Course.builder()
                .title("Java")
                .lessons(Arrays.asList(spring, jvm, jdbc))
                .build();

        Course sql_c = Course.builder()
                .title("sql_c")
                .lessons(Arrays.asList(sql, db, exceptions))
                .build();

        session.persist(java);
        session.persist(sql_c);

        Student marsel = Student.builder()
                .firstName("Mars")
                .lastName("Sit")
                .build();

        Student victor = Student.builder()
                .firstName("Victor")
                .lastName("Bit")
                .build();

        session.persist(marsel);
        session.persist(victor);

        session.getTransaction().commit();

        session.close();
        sessionFactory.close();
    }
}
