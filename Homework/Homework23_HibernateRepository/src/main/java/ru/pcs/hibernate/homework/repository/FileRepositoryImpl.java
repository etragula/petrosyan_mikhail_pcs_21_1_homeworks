package ru.pcs.hibernate.homework.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import ru.pcs.hibernate.homework.models.File;

import java.util.Optional;

public class FileRepositoryImpl implements FileRepository {

    private final SessionFactory sessionFactory;
    private Session session;

    public FileRepositoryImpl(String resource) {
        Configuration configuration = new Configuration();
        configuration.configure(resource);
        sessionFactory = configuration.buildSessionFactory();
    }

    private void initConfigurations() {
        session = sessionFactory.openSession();
        session.beginTransaction();
    }

    private void closeConfigurations() {
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void save(File file) {
        initConfigurations();
        session.persist(file);
        closeConfigurations();
    }

    @Override
    public Optional<File> findByStorageName(String storageFileName) {
        initConfigurations();
        final String query = "select file from File file where storage_name = '" + storageFileName + "'";
        final Query<File> fileQuery = session.createQuery(query, File.class);
        if (fileQuery.getResultList() != null) {
            return Optional.of(fileQuery.getSingleResult());
        }
        return Optional.empty();
    }
}
