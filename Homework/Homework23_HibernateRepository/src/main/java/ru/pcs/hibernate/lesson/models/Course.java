package ru.pcs.hibernate.lesson.models;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "students")
public class Course {
    private Long id;
    private String title;

    private List<Lesson> lessons;

    private List<Student> students;
}
