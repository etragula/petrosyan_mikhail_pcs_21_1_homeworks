package ru.pcs.hibernate.lesson.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.hibernate.lesson.models.Course;
import ru.pcs.hibernate.lesson.models.Lesson;

import java.util.Arrays;

public class MainRelationships2 {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        Lesson spring = Lesson.builder()
                .name("Spring")
                .build();

        Lesson jvm = Lesson.builder()
                .name("JVM")
                .build();

        Lesson jdbc = Lesson.builder()
                .name("JDBC")
                .build();

        session.persist(spring);
        session.persist(jvm);
        session.persist(jdbc);

        Course java = Course.builder()
                .title("Java")
                .lessons(Arrays.asList(spring, jvm, jdbc))
                .build();

        session.persist(java);

        session.getTransaction().commit();

        session.close();
        sessionFactory.close();
    }
}
