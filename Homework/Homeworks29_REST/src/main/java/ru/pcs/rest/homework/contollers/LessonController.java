package ru.pcs.rest.homework.contollers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.pcs.rest.homework.dto.LessonDto;
import ru.pcs.rest.homework.services.LessonService;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/lessons")
@RequiredArgsConstructor
public class LessonController {

    private final LessonService lessonService;

    @GetMapping
    public List<LessonDto> getLessons() {
        return lessonService.getAll();
    }

    @PostMapping
    public LessonDto createLesson(@RequestBody LessonDto lessonDto) {
        return lessonService.createLesson(lessonDto);
    }

    @PostMapping("/addLessonToCourse")
    public void addLessonToCourse(Long lessonId, Long courseId) {
        lessonService.addLessonToCourse(lessonId, courseId);
    }

    @PutMapping("/{lesson-id}")
    public LessonDto updateLesson(@PathVariable("lesson-id") Long id, @RequestBody LessonDto lessonDto) {
        return lessonService.updateLesson(id, lessonDto);
    }

    @DeleteMapping("/{lesson-id}")
    public void deleteLesson(@PathVariable("lesson-id") Long id) {
        lessonService.deleteLesson(id);
    }
}
