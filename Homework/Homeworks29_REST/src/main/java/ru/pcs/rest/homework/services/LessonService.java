package ru.pcs.rest.homework.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.rest.homework.dto.LessonDto;
import ru.pcs.rest.homework.models.Course;
import ru.pcs.rest.homework.models.Lesson;
import ru.pcs.rest.homework.repositories.CourseJpaRepository;
import ru.pcs.rest.homework.repositories.LessonJpaRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LessonService {

    private final LessonJpaRepository lessonJpaRepository;
    private final CourseJpaRepository courseJpaRepository;

    public List<LessonDto> getAll() {
        return LessonDto.from(lessonJpaRepository.findAll());
    }

    public LessonDto createLesson(LessonDto lessonDto) {
        Lesson lesson = Lesson.builder()
                .name(lessonDto.getName())
                .build();
        lessonJpaRepository.save(lesson);
        return LessonDto.from(lessonJpaRepository.findByName(lesson.getName()));

    }
    public LessonDto updateLesson(Long id, LessonDto lessonDto) {
        Lesson lesson = lessonJpaRepository.getById(id);
        lesson.setName(lessonDto.getName());
        lessonJpaRepository.save(lesson);
        return LessonDto.from(lesson);    }

    public void deleteLesson(Long id) {
        lessonJpaRepository.deleteById(id);
    }

    public void addLessonToCourse(Long lessonId, Long courseId) {
        Lesson lesson = lessonJpaRepository.getById(lessonId);
        Course course = courseJpaRepository.getById(courseId);
        lesson.setCourse(course);
        lessonJpaRepository.save(lesson);
        course.getLessons().add(lesson);
        courseJpaRepository.save(course);
    }

    public void deleteLessonFromCourse(Long lessonId) {
        Lesson lesson = lessonJpaRepository.getById(lessonId);
        lesson.setCourse(null);
        lessonJpaRepository.save(lesson);
    }
}
