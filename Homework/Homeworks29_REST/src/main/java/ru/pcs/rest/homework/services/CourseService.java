package ru.pcs.rest.homework.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.rest.homework.dto.CourseDto;
import ru.pcs.rest.homework.models.Course;
import ru.pcs.rest.homework.repositories.CourseJpaRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CourseService {

    private final CourseJpaRepository courseJpaRepository;

    public List<CourseDto> getAll() {
        return CourseDto.from(courseJpaRepository.findAll());
    }

    public CourseDto createCourse(CourseDto courseDto) {
        Course course = Course.builder()
                .title(courseDto.getTitle())
                .build();
        courseJpaRepository.save(course);
        return CourseDto.from(courseJpaRepository.findByTitle(course.getTitle()));
    }

    public CourseDto updateCourse(Long id, CourseDto courseDto) {
        Course course = courseJpaRepository.getById(id);
        course.setTitle(courseDto.getTitle());
        courseJpaRepository.save(course);
        return CourseDto.from(course);
    }

    public void deleteCourse(Long id) {
        courseJpaRepository.deleteById(id);
    }


}
