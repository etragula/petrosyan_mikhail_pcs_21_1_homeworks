package ru.pcs.rest.homework.contollers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.pcs.rest.homework.dto.CourseDto;
import ru.pcs.rest.homework.services.CourseService;

import java.util.List;

@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @GetMapping
    public List<CourseDto> getCourses() {
        return courseService.getAll();
    }

    @PostMapping
    public CourseDto createCourse(@RequestBody CourseDto courseDto) {
        return courseService.createCourse(courseDto);
    }

    @PutMapping("/{course-id}")
    public CourseDto updateCourse(@PathVariable("course-id") Long id, @RequestBody CourseDto courseDto) {
        return courseService.updateCourse(id, courseDto);
    }

    @DeleteMapping("/{course-id}")
    public void deleteCourse(@PathVariable("course-id") Long id) {
        courseService.deleteCourse(id);
    }

    @PostMapping("/deleteLessonToCourse")
    public void deleteLessonToCourse(Long lessonId, Long courseId) {
        courseService.deleteLessonToCourse(lessonId, courseId);
    }
}
