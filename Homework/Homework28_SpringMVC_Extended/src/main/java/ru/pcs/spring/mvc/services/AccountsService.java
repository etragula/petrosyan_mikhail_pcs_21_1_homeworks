package ru.pcs.spring.mvc.services;

import ru.pcs.spring.mvc.dto.AccountDto;

import java.util.List;

/**
 * 18.10.2021
 * 30. Java Web Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface AccountsService {
    List<AccountDto> getAll();

    void addUser(AccountDto account);
}
