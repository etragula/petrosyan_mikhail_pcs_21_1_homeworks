package ru.pcs.spring.mvc.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.pcs.spring.mvc.dto.AccountDto;
import ru.pcs.spring.mvc.services.AccountsService;

import java.util.List;

/**
 * 03.11.2021
 * 38. Spring MVC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

@Controller
@RequestMapping
@RequiredArgsConstructor
public class UsersController {

    private final AccountsService accountsService;

    @GetMapping("/users")
    public String getUsersPage(Model model) {
        model.addAttribute("accounts", accountsService.getAll());
        return "accounts";
    }

    @PostMapping(value = "/users/json", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AccountDto> addUser(@RequestBody AccountDto account) {
        accountsService.addUser(account);
        return accountsService.getAll();
    }
}
