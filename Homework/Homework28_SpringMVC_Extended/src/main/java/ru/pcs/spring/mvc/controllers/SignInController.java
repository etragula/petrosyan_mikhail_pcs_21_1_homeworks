package ru.pcs.spring.mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 03.11.2021
 * 38. Spring MVC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

@Controller
@RequestMapping
public class SignInController {

    @GetMapping( "/signIn")
    public String signIn() {
        return "sign-in";
    }
}
