<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="ru.pcs.spring.jdbc.homework.dto.AccountDto" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  Date: 04.11.2021
  Time: 16:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Accounts</title>
</head>
<script>
    function addAccount(firstName, lastName) {
        let body = {
            "firstName": firstName,
            "lastName": lastName
        }

        let request = new XMLHttpRequest();

        request.open('POST', '/accounts/json', false);
        request.setRequestHeader('Content-Type', 'application/json');
        request.send(JSON.stringify(body));

        if (request.status !== 200) {
            alert("Error!")
        } else {
            let html =
                '<tr>' +
                '<th>ID</th>' +
                '<th>First Name</th>' +
                '<th>Last Name</th>' +
                '</tr>';

            let json = JSON.parse(request.response);
            for (let i = 0; i < json.length; i++) {
                html += '<tr>';
                html += '<td>' + json[i]['id'] + '</td>';
                html += '<td>' + json[i]['firstName'] + '</td>';
                html += '<td>' + json[i]['lastName'] + '</td>';
                html += '</tr>';
            }
            document.getElementById('accounts_table').innerHTML = html;
        }
    }
</script>
<body>
<h1 style="color: ${color}">Accounts size - ${accounts.size()}</h1>
</h1>
<table id="accounts_table">
    <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
    </tr>
<c:forEach items="${accounts}" var="account">
    <tr>
        <th>${account.id}</th>
        <th>${account.firstName}</th>
        <th>${account.lastName}</th>
    </tr>
    </c:forEach>

    <%--
<%
    List<AccountDto> accounts = (List<AccountDto>) request.getAttribute("accounts");
    for (AccountDto account : accounts) {
    %>
    <tr>
    <td><%=account.getId()%>
    <td><%=account.getFirstName()%>
    <td><%=account.getLastName()%>
    <%}%>
    </tr>
</table>
--%>
</table>
<div>
    <input id="firstName" placeholder="Enter First Name">
    <input id="lastName" placeholder="Enter Last Name">
    <button onclick="addAccount(
        document.getElementById('firstName').value,
        document.getElementById('lastName').value)">
        Add
    </button>
</div>
</body>
</html>
