package ru.pcs.spring.jdbc.homework.servlets;


import org.springframework.context.ApplicationContext;
import ru.pcs.spring.jdbc.homework.dto.FileDto;
import ru.pcs.spring.jdbc.homework.services.FilesService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@WebServlet("/filesUpload")
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {

    private FilesService filesService;

    @Override
    public void init(ServletConfig config) {
        final ServletContext servletContext = config.getServletContext();
        ApplicationContext context = (ApplicationContext) servletContext.getAttribute("context");
        this.filesService = context.getBean(FilesService.class);
        this.filesService.setStoragePath((String) servletContext.getAttribute("storagePath"));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/jsp/files-upload.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        req.getReader().lines().forEach(System.out::println);

        BufferedReader descReader =
                new BufferedReader(new InputStreamReader(req.getPart("description").getInputStream()));

        Part file = req.getPart("file");

        FileDto form = FileDto.builder()
                .description(descReader.readLine())
                .orFileName(file.getSubmittedFileName().replace(" ", "_"))
                .size(file.getSize())
                .mimeType(file.getContentType())
                .fileStream(file.getInputStream())
                .eMail(req.getSession().getAttribute("logIn").toString())
                .build();

        filesService.upload(form);
    }

}
