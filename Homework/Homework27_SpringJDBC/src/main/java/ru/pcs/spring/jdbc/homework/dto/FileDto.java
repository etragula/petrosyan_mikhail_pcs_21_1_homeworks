package ru.pcs.spring.jdbc.homework.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.spring.jdbc.homework.models.FileInfo;

import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileDto {
    private Long size;
    private String orFileName;
    private String storageFileName;
    private String mimeType;
    private InputStream fileStream;
    private String description;
    private String eMail;


    public static FileDto from(FileInfo fileInfo) {
        return FileDto.builder()
                .description(fileInfo.getDescription())
                .orFileName(fileInfo.getOriginalFileName())
                .storageFileName(fileInfo.getStorageFileName())
                .size(fileInfo.getSize())
                .mimeType(fileInfo.getMimeType())
                .eMail(fileInfo.getEmail())
                .build();
    }


    public static List<FileDto> from(List<FileInfo> files) {
        return files.stream().map(FileDto::from).collect(Collectors.toList());
    }
}
