package ru.pcs.spring.jdbc.homework.services;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.spring.jdbc.homework.dto.FileDto;
import ru.pcs.spring.jdbc.homework.models.FileInfo;
import ru.pcs.spring.jdbc.homework.repositories.FileInfoRepository;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static ru.pcs.spring.jdbc.homework.dto.FileDto.from;

@Service
@RequiredArgsConstructor
public class FilesServiceImpl implements FilesService {

    private final FileInfoRepository fileInfoRepository;
    private String storagePath;

    @Override
    public void upload(FileDto fileDto) {
        String fileName = fileDto.getOrFileName();
        String extension = fileName.substring(fileName.lastIndexOf("."));
        String storageFileName = UUID.randomUUID() + extension;

        fileInfoRepository.save(
                FileInfo.builder()
                        .originalFileName(fileName)
                        .storageFileName(storageFileName)
                        .mimeType(fileDto.getMimeType())
                        .description(fileDto.getDescription())
                        .size(fileDto.getSize())
                        .email(fileDto.getEMail())
                        .build()
        );

        if (storagePath != null) {
            try {
                Files.copy(fileDto.getFileStream(), Paths.get(storagePath + storageFileName));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    @Override
    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }

    @Override
    public FileDto getFileByStorageName(String storageFileName) {
        Optional<FileInfo> fileInfoOptional = fileInfoRepository.findFileInfoByStorageFileName(storageFileName);
        if (fileInfoOptional.isPresent()) {
            FileDto file = from(fileInfoOptional.get());
            file.setOrFileName(storagePath + storageFileName);
            return file;
        }
        return null;
    }

    @Override
    public List<FileDto> getFileByOriginalName(String originalFileName, String eMail) {
        return from(fileInfoRepository.findFileInfosByEmailAndOriginalFileNameContains(eMail, originalFileName));
    }

    @Override
    public void writeFile(FileDto file, OutputStream outputStream) {
        try {
            final String path = file.getOrFileName();
            System.out.println("path - \n" + path);
            Files.copy(Paths.get(path), outputStream);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
