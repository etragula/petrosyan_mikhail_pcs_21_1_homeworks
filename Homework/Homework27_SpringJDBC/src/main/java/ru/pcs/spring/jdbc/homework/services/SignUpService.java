package ru.pcs.spring.jdbc.homework.services;

import ru.pcs.spring.jdbc.homework.dto.SignUpForm;

public interface SignUpService {
    void signUp(SignUpForm form);
}
