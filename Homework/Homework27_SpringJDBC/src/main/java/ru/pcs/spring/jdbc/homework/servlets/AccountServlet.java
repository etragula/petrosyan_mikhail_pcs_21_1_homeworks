package ru.pcs.spring.jdbc.homework.servlets;

import org.springframework.context.ApplicationContext;
import ru.pcs.spring.jdbc.homework.dto.AccountDto;
import ru.pcs.spring.jdbc.homework.services.AccountService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/accounts")
public class AccountServlet extends HttpServlet {

    private AccountService accountService;

    @Override
    public void init(ServletConfig config) {
        final ServletContext servletContext = config.getServletContext();
        ApplicationContext context = (ApplicationContext) servletContext.getAttribute("context");
        this.accountService = context.getBean(AccountService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<AccountDto> accounts = accountService.getAll();
        req.setAttribute("accounts", accounts);
        req.getRequestDispatcher("jsp/accounts.jsp").forward(req, resp);
    }
}
