package ru.pcs.spring.jdbc.homework.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.spring.jdbc.homework.models.FileInfo;

import java.util.List;
import java.util.Optional;

public interface FileInfoRepository extends JpaRepository<FileInfo, Long> {

    Optional<FileInfo> findFileInfoByStorageFileName(String storageFileName);

    List<FileInfo> findFileInfosByEmailAndOriginalFileNameContains(String email, String originalFileName);
}
