package ru.pcs.spring.jdbc.homework.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.spring.jdbc.homework.dto.SignUpForm;
import ru.pcs.spring.jdbc.homework.models.Account;
import ru.pcs.spring.jdbc.homework.repositories.AccountRepository;

import java.util.Locale;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final AccountRepository accountRepository;

    @Override
    public void signUp(SignUpForm form) {
        accountRepository.save(
                Account.builder()
                        .firstName(form.getFirstName())
                        .lastName(form.getLastName())
                        .email(form.getEMail().toLowerCase(Locale.ROOT))
                        .password(form.getPassword())
                        .build());
    }
}
