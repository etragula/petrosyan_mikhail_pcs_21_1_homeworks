package ru.pcs.spring.jdbc.homework.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.spring.jdbc.homework.dto.SignInForm;
import ru.pcs.spring.jdbc.homework.models.Account;
import ru.pcs.spring.jdbc.homework.repositories.AccountRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SignInServiceImpl implements SignInService {

    private final AccountRepository accountRepository;

    @Override
    public boolean isAuthenticated(SignInForm signInForm) {
        List<Account> accountsByEMail = accountRepository.findAllAccountsByEmail(signInForm.getEMail());
        for (Account account : accountsByEMail) {
            if (account.getPassword().equals(signInForm.getPassword()))
                return true;
        }
        return false;
    }
}
