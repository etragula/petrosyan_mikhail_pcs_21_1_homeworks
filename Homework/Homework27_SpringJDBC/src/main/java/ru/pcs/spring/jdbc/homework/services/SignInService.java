package ru.pcs.spring.jdbc.homework.services;

import ru.pcs.spring.jdbc.homework.dto.SignInForm;

public interface SignInService {
    boolean isAuthenticated(SignInForm signInForm);
}
