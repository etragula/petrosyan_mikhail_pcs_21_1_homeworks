package ru.pcs.spring.jdbc.homework.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.pcs.spring.jdbc.homework.models.Account;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query(value = "select account from Account account where account.email = :email")
    List<Account> findAllAccountsByEmail(@Param("email") String email);
}
