package ru.pcs.spring.jdbc.homework.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.pcs.spring.jdbc.homework.dto.AccountDto;
import ru.pcs.spring.jdbc.homework.dto.SignUpForm;
import ru.pcs.spring.jdbc.homework.services.AccountService;
import ru.pcs.spring.jdbc.homework.services.SignUpService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/accounts/json")
public class AccountsJsonServlet extends HttpServlet {

    private AccountService accountService;
    private SignUpService signUpService;
    private ObjectMapper mapper;

    @Override
    public void init(ServletConfig config) {
        final ServletContext servletContext = config.getServletContext();
        ApplicationContext context = (ApplicationContext) servletContext.getAttribute("context");
        this.accountService = context.getBean(AccountService.class);
        this.signUpService = context.getBean(SignUpService.class);
        this.mapper = new ObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<AccountDto> accountDtos = accountService.getAll();
        resp.setContentType("application/json");
        String accountsJson = mapper.writeValueAsString(accountDtos);
        resp.getWriter().println(accountsJson);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String body = req.getReader().readLine();
        SignUpForm account = mapper.readValue(body, SignUpForm.class);
        signUpService.signUp(SignUpForm.builder()
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .eMail("")
                .password("")
                .build());
        resp.getWriter().println(mapper.writeValueAsString(accountService.getAll()));
    }
}
