package ru.pcs.spring.jdbc.homework.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.spring.jdbc.homework.dto.AccountDto;
import ru.pcs.spring.jdbc.homework.repositories.AccountRepository;

import java.util.List;

import static ru.pcs.spring.jdbc.homework.dto.AccountDto.from;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Override
    public List<AccountDto> getAll() {
        return from(accountRepository.findAll());
    }

    @Override
    public List<AccountDto> searchByEmail(String email) {
        return from(accountRepository.findAllAccountsByEmail(email));
    }
}
