package ru.pcs.spring.jdbc.homework.services;


import ru.pcs.spring.jdbc.homework.dto.AccountDto;

import java.util.List;

public interface AccountService {

    List<AccountDto> getAll();

    List<AccountDto> searchByEmail(String email);

}
