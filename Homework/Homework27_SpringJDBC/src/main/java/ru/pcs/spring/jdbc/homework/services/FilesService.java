package ru.pcs.spring.jdbc.homework.services;

import ru.pcs.spring.jdbc.homework.dto.FileDto;

import java.io.OutputStream;
import java.util.List;

public interface FilesService {
    void upload(FileDto fileDto);

    void setStoragePath(String storagePath);

    FileDto getFileByStorageName(String storageFileName);

    List<FileDto> getFileByOriginalName(String storageFileName, String eMail);

    void writeFile(FileDto file, OutputStream outputStream);
}
