package ru.pcs.security.homework.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pcs.security.homework.dto.AccountDto;

/**
 * 03.11.2021
 * 38. Spring MVC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

@Controller
@RequestMapping("/profile")
public class ProfileController {

    @GetMapping
    public String getProfilePage(Model model) {
        model.addAttribute("account", new AccountDto());
        return "profile";
    }

}
