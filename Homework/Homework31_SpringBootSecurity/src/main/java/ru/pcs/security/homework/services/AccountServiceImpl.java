package ru.pcs.security.homework.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.pcs.security.homework.dto.AccountDto;
import ru.pcs.security.homework.models.Account;
import ru.pcs.security.homework.repositories.AccountsRepository;

import java.util.Locale;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountsRepository accountsRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void save(AccountDto accountDto) {
        final Account account = Account.builder()
                .firstName(accountDto.getFirstName())
                .lastName(accountDto.getLastName())
                .email(accountDto.getEmail().toLowerCase(Locale.ROOT))
                .password(passwordEncoder.encode(accountDto.getPassword()))
                .role(Account.Role.USER)
                .state(Account.State.NOT_CONFIRMED)
                .build();

        accountsRepository.save(account);
    }
}
