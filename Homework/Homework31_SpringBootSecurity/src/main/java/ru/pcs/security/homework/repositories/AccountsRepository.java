package ru.pcs.security.homework.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.security.homework.models.Account;

import java.util.Optional;

public interface AccountsRepository extends JpaRepository<Account, Long> {
    Optional<Account> findAccountByEmail(String email);
}
