package ru.pcs.security.homework.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pcs.security.homework.dto.ProductDto;
import ru.pcs.security.homework.services.ProductService;

import javax.validation.Valid;

@Controller
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping
    public String getProductPage(Model model) {
        model.addAttribute("productDto", new ProductDto());
        return "add-product";
    }

    @PostMapping
    public String addProduct(@Valid ProductDto productDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("productDto", productDto);
            return "add-product";
        }
        productService.save(productDto);
        return "add-product";
    }

    @GetMapping("/all")
    public String addProduct(Model model) {
        model.addAttribute("products", productService.getAllProducts());
        return "show-products";
    }
}
