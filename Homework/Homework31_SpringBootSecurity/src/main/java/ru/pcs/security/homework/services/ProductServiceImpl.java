package ru.pcs.security.homework.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.security.homework.dto.ProductDto;
import ru.pcs.security.homework.models.Product;
import ru.pcs.security.homework.repositories.ProductRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public void save(ProductDto productDto) {
        productRepository.save(Product.builder()
                .name(productDto.getName())
                .price(productDto.getPrice())
                .amount(productDto.getAmount())
                .build());
    }

    @Override
    public List<ProductDto> getAllProducts() {
        return ProductDto.from(productRepository.findAll());
    }
}
