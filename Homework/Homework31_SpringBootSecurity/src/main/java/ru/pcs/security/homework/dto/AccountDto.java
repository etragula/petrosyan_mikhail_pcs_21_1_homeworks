package ru.pcs.security.homework.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.security.homework.models.Account;
import ru.pcs.security.homework.validation.NoDigits;
import ru.pcs.security.homework.validation.NotSameNames;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NotSameNames
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountDto {
    @NotBlank
    @NoDigits
    private String firstName;
    @NotBlank
    @NoDigits
    private String lastName;
    @NotBlank
    @Email
    private String email;
    @NotBlank
    private String password;

    public static AccountDto from(Account account) {
        return AccountDto.builder()
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .email(account.getEmail())
                .password(account.getPassword())
                .build();
    }

    public static List<AccountDto> from(List<Account> accounts) {
        return accounts.stream().map(AccountDto::from).collect(Collectors.toList());
    }
}
