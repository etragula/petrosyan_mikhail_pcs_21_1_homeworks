package ru.pcs.security.homework.services;

import ru.pcs.security.homework.dto.ProductDto;

import java.util.List;

public interface ProductService {
    void save(ProductDto productDto);

    List<ProductDto> getAllProducts();
}
