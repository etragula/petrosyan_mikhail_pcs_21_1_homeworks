package ru.pcs.security.homework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homework31SpringBootSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(Homework31SpringBootSecurityApplication.class, args);
    }

}
