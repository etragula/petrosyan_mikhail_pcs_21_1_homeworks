package ru.pcs.security.homework.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = IsIntegerValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IsInteger {
    String message() default "DigitsFound";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
