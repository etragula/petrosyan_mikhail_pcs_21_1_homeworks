package ru.pcs.security.homework.services;

import ru.pcs.security.homework.dto.AccountDto;

public interface AccountService {

    void save(AccountDto accountDto);
}
