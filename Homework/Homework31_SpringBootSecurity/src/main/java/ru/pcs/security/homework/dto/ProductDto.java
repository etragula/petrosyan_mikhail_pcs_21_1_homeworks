package ru.pcs.security.homework.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.security.homework.models.Product;
import ru.pcs.security.homework.validation.IsInteger;
import ru.pcs.security.homework.validation.NoDigits;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDto {
    @NotBlank
    @NoDigits
    private String name;
    @NotNull
    @Positive
    private Float price;
    @NotNull
    @IsInteger
    @PositiveOrZero
    private Integer amount;

    public static ProductDto from(Product product) {
        return ProductDto.builder()
                .name(product.getName())
                .amount(product.getAmount())
                .price(product.getPrice())
                .build();
    }

    public static List<ProductDto> from(List<Product> products) {
        return products.stream().map(ProductDto::from).collect(Collectors.toList());
    }
}
