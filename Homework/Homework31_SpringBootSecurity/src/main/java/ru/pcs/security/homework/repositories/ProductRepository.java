package ru.pcs.security.homework.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.security.homework.models.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
