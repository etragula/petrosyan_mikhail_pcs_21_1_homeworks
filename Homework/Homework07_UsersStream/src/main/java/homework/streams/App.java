package homework.streams;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class App {

    public static void main(String[] args) {
        List<Car> allCars = findAllCars();

//        1) Номера всех автомобилей, имеющих черный цвет или нулевой пробег.
        allCars.stream()
                .filter(car -> car.getColour().equals("Черный") || car.getMileage().equals(0))
                .forEach(System.out::println);

//        2) Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.
        Set<String> set = new HashSet<>();
        long count = allCars.stream().filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                .filter(car -> set.add(car.getModel())).count();
        System.out.println(count);

//        3) Вывести цвет автомобиля с минимальной стоимостью.
        Optional<Car> cheapestCar = allCars.stream().min(Comparator.comparingInt(Car::getPrice));
        cheapestCar.ifPresent(car -> System.out.println(car.getColour()));

//        4) Среднюю стоимость Camry
        allCars.stream()
                .filter(car -> car.getModel().equals("Toyota"))
                .mapToInt(Car::getPrice)
                .average()
                .ifPresent(System.out::println);

    }

    private static List<Car> findAllCars() {
        List<Car> cars = new ArrayList<>();

        for (String carInfo : getCarsFromFile()) {
            String[] s = carInfo.split(" ");
            cars.add(new Car(s[0], s[1], s[2], Integer.parseInt(s[3]), Integer.parseInt(s[4])));
        }
        return cars;
    }

    private static List<String> getCarsFromFile() {
        try {
            final String PATH = "Homework/Homework07_UsersStream/src/main/resources/info.txt";
            List<String> strings = Files.readAllLines(Paths.get(PATH), StandardCharsets.UTF_8);
            strings.replaceAll(s -> s
                    .replaceAll("]", " ")
                    .replaceAll("\\[", "")
                    .trim()
            );
            return strings;
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }

    @Data
    @AllArgsConstructor
    private static class Car {

        private String number;
        private String model;
        private String colour;
        private Integer mileage;
        private Integer price;
    }
}
