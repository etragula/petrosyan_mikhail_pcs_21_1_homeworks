package lesson.streams.func.interfaces;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class ExmpPredicate<T> {

    private List<T> elements;

    public ExmpPredicate(List<T> elements) {
        this.elements = elements;
    }

    public List<T> map(Predicate<T> predicate) {
        List<T> result = new ArrayList<>();

        for (T element : elements) {
            if (predicate.test(element))
                result.add(element);
        }
        return result;
    }

    public static void main(String[] args) {
        List<String> text = Arrays.asList("Привет", "Как", "что нового");
        ExmpPredicate<String> exmpPredicate = new ExmpPredicate<>(text);
        List<String> result = exmpPredicate.map(s -> s.length() >= 5);
        System.out.println(result);
    }
}
