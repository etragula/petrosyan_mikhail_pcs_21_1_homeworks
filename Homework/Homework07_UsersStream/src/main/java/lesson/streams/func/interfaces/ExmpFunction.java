package lesson.streams.func.interfaces;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class ExmpFunction<T> {

    private List<T> elements;

    public ExmpFunction(List<T> elements) {
        this.elements = elements;
    }

    public <R> List<R> map(Function<T, R> function) {
        List<R> result = new ArrayList<>();

        for (T element : elements) {
            result.add(function.apply(element));
        }
        return result;
    }

    public static void main(String[] args) {
        List<String> text = Arrays.asList("Привет", "Как ты", "что нового");
        ExmpFunction<String> exmpFunction = new ExmpFunction<>(text);
        List<Integer> result = exmpFunction.map(s -> s.length());
        System.out.println(result);
    }
}
