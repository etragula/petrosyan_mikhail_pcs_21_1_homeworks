package lesson.db;

import java.sql.*;

public class Main {

    private static final String DB_USER = "";
    private static final String DB_PASS = "";
    private static final String DB_URL = "";

    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
             Statement statement = connection.createStatement()) {

            String sql = "select * from product where product.id = 112753";

            try (ResultSet resultSet = statement.executeQuery(sql)) {
                while (resultSet.next()) {
                    System.out.println(resultSet.getInt("price") + "\n"
                            + resultSet.getString("name"));
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
