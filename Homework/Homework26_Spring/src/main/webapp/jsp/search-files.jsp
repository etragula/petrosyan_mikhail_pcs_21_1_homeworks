<%--
  Created by IntelliJ IDEA.
  Date: 10.11.2021
  Time: 20:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Search Files
    </title>
</head>
<script>
    function searchFiles(name) {
        let request = new XMLHttpRequest();

        request.open('POST', '/files?name=' + name, false);

        request.send()

        if (request.status !== 200) {
            alert("Error!")
        } else {
            let html =
                '<tr>' +
                '<th>File Name</th>' +
                '<th>Size</th>' +
                '</tr>';

            let json = JSON.parse(request.response);
            for (let i = 0; i < json.length; i++) {
                html += '<tr>';
                let element = json[i]['orFileName'];
                html += '<td>' + '<a href="/download?name=' + element + '">' + element + '</a>' + '</td>';
                html += '<td>' + json[i]['size'] + '</td>';
                html += '</tr>';
            }
            document.getElementById('files_table').innerHTML = html;
        }
    }

</script>

<body>
<label for="name">Search file by name:</label>
<input id="name" type="text" placeholder="Enter file name..."
       onkeyup="searchFiles(document.getElementById('name').value)">
<table id="files_table">
</table>
<form align="right" action="/logOut" method="get">
    <label class="logoutLblPos">
        <input name="submit2" type="submit" id="submit2" value="Log out from ${logIn}">
    </label>
</form>
</body>
</html>
