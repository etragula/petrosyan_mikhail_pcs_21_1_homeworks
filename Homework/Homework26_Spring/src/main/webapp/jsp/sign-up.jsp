<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title style="color: ${color}">SignUp</title>
</head>
<body>
<h1 style="color: ${color}">Sign Up Page</h1>
<h2>Please Enter Your Data:</h2>
<form action="/signUp" method="post">
    <label for="firstName">Enter first name:</label>
    <input id="firstName" name="firstName" placeholder="Your first name">
    <br>
    <label for="lastName">Enter last name:</label>
    <input id="lastName" name="lastName" placeholder="Your last  name">
    <br>
    <label for="email">Enter eMail:</label>
    <input type="email" id="eMail" name="eMail" placeholder="Your eMail">
    <br>
    <label for="password">Enter password:</label>
    <input type="password" id="password" name="password" placeholder="Your password">
    <br>
    <input type="submit" name="Sign Up!">
</form>
</body>
</html>