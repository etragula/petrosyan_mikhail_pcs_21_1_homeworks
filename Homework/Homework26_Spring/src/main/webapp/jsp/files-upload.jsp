<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>
        File Upload
    </title>
</head>
<body>
<form action="/filesUpload" method="post" enctype="multipart/form-data">
    <label for="description">Description</label>
    <input type="text" id="description" name="description" placeholder="Enter description...">
    <br>
    <input type="file" name="file">
    <input type="submit" value="File Upload">
</form>
<form align="right" action="/logOut" method="get">
    <label class="logoutLblPos">
        <input name="submit2" type="submit" id="submit2" value="Log out from ${logIn}">
    </label>
</form>
</body>
</html>
