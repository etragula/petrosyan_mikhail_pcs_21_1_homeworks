package ru.pcs.spring.homework.services;

import ru.pcs.spring.homework.dto.SignUpForm;

public interface SignUpService {
    void signUp(SignUpForm form);
}
