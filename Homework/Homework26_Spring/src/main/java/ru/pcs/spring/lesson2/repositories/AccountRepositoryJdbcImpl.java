package ru.pcs.spring.lesson2.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.pcs.spring.lesson2.models.Account;

import javax.sql.DataSource;
import java.sql.*;
import java.util.function.Function;

@Repository
public class AccountRepositoryJdbcImpl implements AccountRepository {

    public static final String SQL_INSERT = "insert into account(eMail, password) values (?, ?);";

    @Autowired
    private DataSource dataSource;

    private static final Function<ResultSet, Account> AccountMapper = resultSet -> {
        try {
            return new Account(
                    resultSet.getLong("id"),
                    resultSet.getString("eMail"),
                    resultSet.getString("password")
            );
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    @Override
    public Account save(Account account) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, account.getEMail());
            statement.setString(2, account.getPassword());
            final int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) throw new SQLException("Can't insert account.");
            final ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                account.setId(generatedKeys.getLong("id"));
                return account;
            } else throw new SQLException("Can't insert account.");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


}
