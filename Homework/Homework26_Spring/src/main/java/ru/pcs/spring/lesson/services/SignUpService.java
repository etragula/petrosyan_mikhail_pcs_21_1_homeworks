package ru.pcs.spring.lesson.services;

public interface SignUpService {

    void signUp(String email, String password);
}
