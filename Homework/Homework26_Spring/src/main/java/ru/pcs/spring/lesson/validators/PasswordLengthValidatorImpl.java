package ru.pcs.spring.lesson.validators;

public class PasswordLengthValidatorImpl implements PasswordValidator {

    private int minimalLength;

    public PasswordLengthValidatorImpl(int minimalLength) {
        this.minimalLength = minimalLength;
    }
    @Override
    public void validate(String password) {
        if (password.length() < minimalLength)
            throw new IllegalArgumentException("Пароль не соотвутствует требованиям!");

    }
}
