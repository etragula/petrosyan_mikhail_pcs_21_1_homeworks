package ru.pcs.spring.homework.services;


import ru.pcs.spring.homework.dto.AccountDto;

import java.util.List;

public interface AccountService {

    List<AccountDto> getAll();

    List<AccountDto> searchByEmail(String email);

}
