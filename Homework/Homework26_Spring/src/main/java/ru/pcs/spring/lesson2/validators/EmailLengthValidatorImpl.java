package ru.pcs.spring.lesson2.validators;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component(value = "lengthValidator")
public class EmailLengthValidatorImpl implements EmailValidator {

    private int minimalLength;

    public EmailLengthValidatorImpl(@Value("${length}") int minimalLength) {
        this.minimalLength = minimalLength;
    }

    @Override
    public void validate(String email) {
        if (email.length() < minimalLength)
            throw new IllegalArgumentException("Email не соотвутствует требованиям!");
    }
}
