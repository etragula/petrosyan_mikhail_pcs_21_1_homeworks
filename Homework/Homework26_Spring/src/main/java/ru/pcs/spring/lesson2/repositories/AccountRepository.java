package ru.pcs.spring.lesson2.repositories;

import ru.pcs.spring.lesson2.models.Account;

public interface AccountRepository {
    Account save(Account account);
}
