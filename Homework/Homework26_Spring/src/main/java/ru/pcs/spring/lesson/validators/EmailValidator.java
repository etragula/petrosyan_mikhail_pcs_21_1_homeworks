package ru.pcs.spring.lesson.validators;

public interface EmailValidator {
    void validate(String email);
}
