package ru.pcs.spring.lesson.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Account {
    private Long id;
      private String eMail;
    private String password;
}
