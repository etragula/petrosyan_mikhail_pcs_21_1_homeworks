package ru.pcs.spring.lesson2.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.pcs.spring.lesson2.blacklist.PasswordBlackList;
import ru.pcs.spring.lesson2.models.Account;
import ru.pcs.spring.lesson2.repositories.AccountRepository;
import ru.pcs.spring.lesson2.validators.EmailValidator;
import ru.pcs.spring.lesson2.validators.PasswordValidator;

@Service
public class SignUpServiceImpl implements SignUpService {

    private final PasswordBlackList passwordBlackList;
    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;
    private final AccountRepository accountRepository;

    @Autowired
    public SignUpServiceImpl(PasswordBlackList passwordBlackList,
                             @Qualifier("emailValidatorImpl") EmailValidator emailValidator,
                             @Qualifier("passwordCharacterValidatorImpl") PasswordValidator passwordValidator,
                             AccountRepository accountRepository) {
        this.passwordBlackList = passwordBlackList;
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
        this.accountRepository = accountRepository;
    }

    @Override
    public void signUp(String email, String password) {
        if (passwordBlackList.contains(password)) throw new IllegalArgumentException("Пароль уже был взломан!");
        emailValidator.validate(email);
        passwordValidator.validate(password);
        accountRepository.save(Account.builder()
                .eMail(email)
                .password(password)
                .build());
    }
}
