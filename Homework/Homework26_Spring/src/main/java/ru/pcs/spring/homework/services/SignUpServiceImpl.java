package ru.pcs.spring.homework.services;

import ru.pcs.spring.homework.dto.SignUpForm;
import ru.pcs.spring.homework.models.Account;
import ru.pcs.spring.homework.repositories.AccountRepository;

import java.util.Locale;

public class SignUpServiceImpl implements SignUpService {

    private final AccountRepository accountRepository;

    public SignUpServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void signUp(SignUpForm form) {
        accountRepository.save(
                Account.builder()
                        .firstName(form.getFirstName())
                        .lastName(form.getLastName())
                        .eMail(form.getEMail().toLowerCase(Locale.ROOT))
                        .password(form.getPassword())
                        .build());
    }
}
