package ru.pcs.spring.lesson2.validators;

public interface EmailValidator {
    void validate(String email);
}
