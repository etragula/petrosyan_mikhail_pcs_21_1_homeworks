package ru.pcs.spring.lesson.blacklist;

public interface PasswordBlackList {
    boolean contains(String password);
}
