package ru.pcs.spring.lesson.validators;

public class PasswordCharacterValidatorImpl implements PasswordValidator {


    @Override
    public void validate(String password) {
        if (password.indexOf('&') == -1 ||
                password.indexOf('#') == -1 ||
                password.indexOf('!') == -1) {
            throw new IllegalArgumentException("Пароль не содержит спец. символа");
        }
    }
}
