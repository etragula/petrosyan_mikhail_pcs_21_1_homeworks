package ru.pcs.spring.homework.services;


import ru.pcs.spring.homework.dto.AccountDto;
import ru.pcs.spring.homework.repositories.AccountRepository;

import java.util.List;

import static ru.pcs.spring.homework.dto.AccountDto.from;


public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;


    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public List<AccountDto> getAll() {
        return from(accountRepository.findAll());
    }

    @Override
    public List<AccountDto> searchByEmail(String email) {
        return from(accountRepository.searchByEmail(email));
    }
}
