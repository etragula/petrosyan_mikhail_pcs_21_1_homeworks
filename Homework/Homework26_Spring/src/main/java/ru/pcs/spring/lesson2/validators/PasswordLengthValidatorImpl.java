package ru.pcs.spring.lesson2.validators;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PasswordLengthValidatorImpl implements PasswordValidator {

    private int minimalLength;

    public PasswordLengthValidatorImpl(@Value("${length}") int minimalLength) {
        this.minimalLength = minimalLength;
    }

    @Override
    public void validate(String password) {
        if (password.length() < minimalLength)
            throw new IllegalArgumentException("Пароль не соотвутствует требованиям!");

    }
}
