package ru.pcs.spring.lesson2.services;

public interface SignUpService {

    void signUp(String email, String password);
}
