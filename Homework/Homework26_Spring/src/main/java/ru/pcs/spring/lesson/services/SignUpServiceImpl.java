package ru.pcs.spring.lesson.services;

import ru.pcs.spring.lesson.blacklist.PasswordBlackList;
import ru.pcs.spring.lesson.models.Account;
import ru.pcs.spring.lesson.repositories.AccountRepository;
import ru.pcs.spring.lesson.validators.EmailValidator;
import ru.pcs.spring.lesson.validators.PasswordValidator;

public class SignUpServiceImpl implements SignUpService {

    private final PasswordBlackList passwordBlackList;
    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;
    private final AccountRepository accountRepository;

    public SignUpServiceImpl(PasswordBlackList passwordBlackList,
                             EmailValidator emailValidator,
                             PasswordValidator passwordValidator,
                             AccountRepository accountRepository) {
        this.passwordBlackList = passwordBlackList;
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
        this.accountRepository = accountRepository;
    }

    @Override
    public void signUp(String email, String password) {
        if (passwordBlackList.contains(password)) throw new IllegalArgumentException("Пароль уже был взломан!");
        emailValidator.validate(email);
        passwordValidator.validate(password);
        accountRepository.save(Account.builder()
                .eMail(email)
                .password(password)
                .build());
    }
}
