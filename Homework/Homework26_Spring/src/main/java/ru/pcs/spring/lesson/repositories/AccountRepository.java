package ru.pcs.spring.lesson.repositories;

import ru.pcs.spring.lesson.models.Account;

public interface AccountRepository {
    Account save(Account account);
}
