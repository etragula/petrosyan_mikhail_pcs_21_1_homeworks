package ru.pcs.spring.lesson2.blacklist;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class PasswordBlackListImpl implements PasswordBlackList {

    public static final List<String> BAD_PASSWORDS = Arrays.asList("qwerty007", "qwerty008", "123");

    @Override
    public boolean contains(String password) {
        return BAD_PASSWORDS.contains(password);
    }
}
