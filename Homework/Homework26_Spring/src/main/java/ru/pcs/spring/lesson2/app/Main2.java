package ru.pcs.spring.lesson2.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.pcs.spring.lesson2.ApplicationConfig;
import ru.pcs.spring.lesson2.services.SignUpService;

public class Main2 {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        SignUpService signUpService = context.getBean(SignUpService.class);
        signUpService.signUp("java@java.com", "java!&#java");
    }
}
