package ru.pcs.spring.lesson2.validators;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component(value = "emailValidatorImpl")
public class EmailValidatorImpl implements EmailValidator {

    private Pattern pattern;

    public EmailValidatorImpl(@Value("${pattern}") String regex) {
        this.pattern = Pattern.compile(regex);
    }

    @Override
    public void validate(String email) {
        if (!pattern.matcher(email).find())
            throw new IllegalArgumentException("Email не соотвутствует формату!");
    }
}
