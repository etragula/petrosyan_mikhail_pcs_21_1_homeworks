package ru.pcs.spring.homework.servlets;

import org.springframework.context.ApplicationContext;
import ru.pcs.spring.homework.dto.SignInForm;
import ru.pcs.spring.homework.services.SignInService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {

    private SignInService signInService;

    @Override
    public void init(ServletConfig config) {
        final ServletContext servletContext = config.getServletContext();
        ApplicationContext context = (ApplicationContext) servletContext.getAttribute("context");
        this.signInService = context.getBean(SignInService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/jsp/sign-in.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SignInForm signInForm = SignInForm.builder()
                .eMail(req.getParameter("eMail"))
                .password(req.getParameter("password"))
                .build();

        if (signInService.isAuthenticated(signInForm)) {
            HttpSession session = req.getSession(true);
            session.setAttribute("isAuthenticated", true);
            session.setAttribute("logIn", req.getParameter("eMail"));

            resp.sendRedirect("/profile");
        } else {
            resp.sendRedirect("/signIn?error");
        }
    }
}
