package ru.pcs.spring.homework.repositories;

import ru.pcs.spring.homework.models.FileInfo;

import java.util.List;
import java.util.Optional;

public interface FileInfoRepository {

    void save(FileInfo fileInfo);

    Optional<FileInfo> findByStorageName(String storageFileName);

    List<FileInfo> findByOriginalName(String originalFileName, String eMail);
}
