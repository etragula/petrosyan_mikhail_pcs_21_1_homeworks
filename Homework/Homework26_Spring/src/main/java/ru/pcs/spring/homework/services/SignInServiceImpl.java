package ru.pcs.spring.homework.services;

import ru.pcs.spring.homework.dto.SignInForm;
import ru.pcs.spring.homework.repositories.AccountRepository;

public class SignInServiceImpl implements SignInService {

    private final AccountRepository accountRepository;

    public SignInServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public boolean isAuthenticated(SignInForm signInForm) {
        return accountRepository
                .findByEmail(signInForm.getEMail())
                .map(account -> account.getPassword().equals(signInForm.getPassword()))
                .orElse(false);
    }
}
