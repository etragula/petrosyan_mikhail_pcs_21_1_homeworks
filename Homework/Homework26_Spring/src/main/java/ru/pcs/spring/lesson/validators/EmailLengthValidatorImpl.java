package ru.pcs.spring.lesson.validators;

public class EmailLengthValidatorImpl implements EmailValidator {

    private int minimalLength;

    public EmailLengthValidatorImpl(int minimalLength) {
        this.minimalLength = minimalLength;
    }

    @Override
    public void validate(String email) {
        if (email.length() < minimalLength)
            throw new IllegalArgumentException("Email не соотвутствует требованиям!");
    }
}
