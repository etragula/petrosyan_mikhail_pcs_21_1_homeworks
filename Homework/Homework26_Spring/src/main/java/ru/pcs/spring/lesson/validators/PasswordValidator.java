package ru.pcs.spring.lesson.validators;

public interface PasswordValidator {
    void validate(String password);
}
