package ru.pcs.spring.homework.repositories;


import ru.pcs.spring.homework.models.FileInfo;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class FileInfoRepositoryImpl implements FileInfoRepository {

    private final DataSource dataSource;

    public FileInfoRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static final String SQL_INSERT =
            "insert into file_info(original_file_name, storage_file_name, size, mime_type, description, eMail) " +
                    "values (?, ?, ?, ?, ?, ?);";

    public static final String SQL_FIND_BY_STORE_NAME =
            "select * from file_info where storage_file_name = ?;";

    public static final String SQL_FIND_BY_ORIGINAL_NAME =
            "select * from file_info where eMail = ? and original_file_name like ?;";

    private static final Function<ResultSet, FileInfo> FileInfoMapper = resultSet -> {
        try {
            return FileInfo.builder()
                    .id(resultSet.getLong("id"))
                    .storageFileName(resultSet.getString("storage_file_name"))
                    .originalFileName(resultSet.getString("original_file_name"))
                    .mimeType(resultSet.getString("mime_type"))
                    .description(resultSet.getString("description"))
                    .size(resultSet.getLong("size"))
                    .eMail(resultSet.getString("eMail"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    @Override
    public void save(FileInfo fileInfo) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, fileInfo.getOriginalFileName());
            statement.setString(2, fileInfo.getStorageFileName());
            statement.setLong(3, fileInfo.getSize());
            statement.setString(4, fileInfo.getMimeType());
            statement.setString(5, fileInfo.getDescription());
            statement.setString(6, fileInfo.getEMail());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<FileInfo> findByStorageName(String storageFileName) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_STORE_NAME)) {
            statement.setString(1, storageFileName);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(FileInfoMapper.apply(resultSet));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<FileInfo> findByOriginalName(String originalFileName, String eMail) {
        List<FileInfo> files = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ORIGINAL_NAME)) {
            statement.setString(1, eMail);
            statement.setString(2, "%" + originalFileName + "%");
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    files.add(FileInfoMapper.apply(resultSet));
                }
                return files;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
