package ru.pcs.spring.lesson2.blacklist;

public interface PasswordBlackList {
    boolean contains(String password);
}
