package ru.pcs.spring.lesson.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.pcs.spring.lesson.blacklist.PasswordBlackList;
import ru.pcs.spring.lesson.blacklist.PasswordBlackListImpl;
import ru.pcs.spring.lesson.repositories.AccountRepository;
import ru.pcs.spring.lesson.repositories.AccountRepositoryJdbcImpl;
import ru.pcs.spring.lesson.services.SignUpService;
import ru.pcs.spring.lesson.services.SignUpServiceImpl;
import ru.pcs.spring.lesson.validators.EmailValidatorImpl;
import ru.pcs.spring.lesson.validators.PasswordLengthValidatorImpl;
import ru.pcs.spring.lesson.validators.PasswordValidator;

public class Main {
    public static void main(String[] args) {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        config.setUsername("a19188759");//todo
        config.setPassword("");//todo
        config.setDriverClassName("org.postgresql.Driver");
        config.setMaximumPoolSize(20);
        HikariDataSource hikariDataSource = new HikariDataSource(config);

        PasswordValidator passwordValidator = new PasswordLengthValidatorImpl(5);
        EmailValidatorImpl emailValidator = new EmailValidatorImpl();
        emailValidator.setPattern(".+@+.");
        PasswordBlackList passwordBlackList = new PasswordBlackListImpl();
        AccountRepository accountRepository = new AccountRepositoryJdbcImpl(hikariDataSource);

        SignUpService signUpService = new SignUpServiceImpl(passwordBlackList, emailValidator, passwordValidator, accountRepository);

        signUpService.signUp("MISHA-PETROSYAN@MAIL.RU", "daspognpos31");

    }
}
