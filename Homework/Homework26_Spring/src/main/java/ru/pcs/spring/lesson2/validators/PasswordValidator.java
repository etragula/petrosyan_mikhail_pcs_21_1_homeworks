package ru.pcs.spring.lesson2.validators;

public interface PasswordValidator {
    void validate(String password);
}
