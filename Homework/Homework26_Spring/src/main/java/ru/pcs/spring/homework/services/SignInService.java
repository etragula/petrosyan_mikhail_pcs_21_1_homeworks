package ru.pcs.spring.homework.services;

import ru.pcs.spring.homework.dto.SignInForm;

public interface SignInService {
    boolean isAuthenticated(SignInForm signInForm);
}
