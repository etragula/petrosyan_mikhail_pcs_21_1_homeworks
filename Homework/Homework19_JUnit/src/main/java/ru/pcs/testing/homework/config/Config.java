package ru.pcs.testing.homework.config;

import lombok.Data;

import java.io.FileInputStream;
import java.util.Properties;

@Data
public class Config {
    private final String user;
    private final String password;
    private final String url;

    public Config() {
        try {
            Properties properties = new Properties();
            final FileInputStream stream = new FileInputStream("Homework/Homework18_Loggers/src/main/resources/application.properties");
            properties.load(stream);
            this.user = properties.getProperty("db.user");
            this.password = properties.getProperty("db.password");
            this.url = properties.getProperty("db.url");
            stream.close();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}