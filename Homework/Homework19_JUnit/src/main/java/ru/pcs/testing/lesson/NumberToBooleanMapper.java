package ru.pcs.testing.lesson;

public interface NumberToBooleanMapper {
    boolean map(int number);
}
