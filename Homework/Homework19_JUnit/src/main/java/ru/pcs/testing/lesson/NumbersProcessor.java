package ru.pcs.testing.lesson;

import java.util.ArrayList;
import java.util.List;

public class NumbersProcessor {
    private final NumberToBooleanMapper mapper;

    public NumbersProcessor(NumberToBooleanMapper mapper) {
        this.mapper = mapper;
    }

    List<Boolean> toBooleanWrapper(List<Integer> numbers) {
        List<Boolean> result = new ArrayList<>();
        for (Integer number : numbers) {
            result.add(mapper.map(number));
        }
        return result;
    }
}
