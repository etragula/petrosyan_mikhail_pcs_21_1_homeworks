package ru.pcs.testing.lesson;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        NumberUtil numberUtil = new NumberUtil();
        NumbersProcessor numbersProcessor = new NumbersProcessor(numberUtil);

        System.out.println(numbersProcessor.toBooleanWrapper(
                Arrays.asList(134, 169, 13, 21, 111)
        ));
//        boolean prime = numberUtil.isPrime(169);
        boolean prime = numberUtil.isPrime(31);
        System.out.println(prime);
    }
}
