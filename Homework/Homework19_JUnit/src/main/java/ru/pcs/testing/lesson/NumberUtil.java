package ru.pcs.testing.lesson;

public class NumberUtil implements NumberToBooleanMapper {
    public boolean isPrime(int number) {
        if (number == 0 || number == 1) throw new IncorrectNumberException();

        if (number == 2 || number == 3) return true;

        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) return false;
        }

        return true;
    }

    public int gcd(int a, int b) {
        while (b != 0) {
            int temp = a % b;
            a = b;
            b = temp;
        }
        return a;
    }

    @Override
    public boolean map(int number) {
        return isPrime(number);
    }
}
