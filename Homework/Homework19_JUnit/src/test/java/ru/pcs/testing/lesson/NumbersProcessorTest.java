package ru.pcs.testing.lesson;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@DisplayName("NumbersProcessorTest is working when")
@ExtendWith(MockitoExtension.class)
class NumbersProcessorTest {
    private NumbersProcessor processor;

    private static final List<Boolean> EXPECTED = Arrays.asList(true, true, false, true, false, false);

    @Mock
    private NumberToBooleanMapper mapper;

    @BeforeEach
    public void setUp() {
//        lenient().when(mapper.map(0)).thenThrow(IncorrectNumberException.class);
        lenient().when(mapper.map(7)).thenReturn(true);
        lenient().when(mapper.map(3)).thenReturn(true);
        lenient().when(mapper.map(10)).thenReturn(false);
        lenient().when(mapper.map(11)).thenReturn(true);
        lenient().when(mapper.map(21)).thenReturn(false);
        processor = new NumbersProcessor(mapper);
    }

    @ParameterizedTest(name = "return <true> on {0}")
    @MethodSource(value = "correctNumbers")
    public void map_to_correct_numbers(List<Integer> numbers) {
        assertEquals(EXPECTED, processor.toBooleanWrapper(numbers));
    }

    public static Stream<Arguments> correctNumbers() {
        return Stream.of(Arguments.of(Arrays.asList(7, 3, 10, 11, 21, 0)));
    }
}