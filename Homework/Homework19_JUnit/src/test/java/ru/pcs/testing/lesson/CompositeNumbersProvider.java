package ru.pcs.testing.lesson;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CompositeNumbersProvider implements ArgumentsProvider {

    private final static int NUMBER_COUNT = 5;
    private final static int HIGH = 100;
    private final static int LOW = 4;
    private final Random random = new Random();

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        List<Arguments> arguments =new ArrayList<>();
        for (int i = 0; i < NUMBER_COUNT; i++) {
            int first = LOW + random.nextInt(HIGH);
            int second = LOW + random.nextInt(HIGH);
            int composite = first * second;
            arguments.add(Arguments.of(composite));
        }
        return arguments.stream();
    }
}