package ru.pcs.testing.lesson;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@DisplayName("NumberUtil is working when")
class NumberUtilTest {
    private final NumberUtil numberUtil = new NumberUtil();

    @Nested
    @DisplayName("isPrime() working when")
    class ForIsPrime {
        @ParameterizedTest(name = "throws exceptions on {0}")
        @ValueSource(ints = {1, 0})
        public void test_Exception(int number) {
            assertThrows(IncorrectNumberException.class, () -> numberUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <true> on {0}")
        @ValueSource(ints = {17, 31, 41, 13})
        public void test_On_Primes(int number) {
            assertTrue(numberUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {121, 64, 169})
        public void test_On_Sqr(int number) {
            assertFalse(numberUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ArgumentsSource(value = CompositeNumbersProvider.class)
        public void test_On_Composite_Numbers(int compositeNumber) {
            assertFalse(numberUtil.isPrime(compositeNumber));
        }
    }

    @Nested
    @DisplayName("gcd() working")
    class ForGcd {

        @ParameterizedTest(name = "return gcd on {0}")
        @CsvSource(value = {"9,12,3", "18,12,6", "64,48,16"})
        public void gcd_on_numbers(int a, int b, int expected) {
            assertEquals(expected, numberUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "return gcd on {0}")
        @CsvSource(value = {"9,12,3", "18,12,6", "64,48,16"})
        public void test_gcd(int a, int b, int expected) {
            assertThat(numberUtil.gcd(a, b), is(equalTo(expected)));
        }
    }
}