package ru.pcs.testing.homework.repositories;


import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ru.pcs.testing.homework.models.Product;

import javax.sql.DataSource;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ProductRepositoryJdbcImplTest {

    private ProductRepository productRepository;
    private DataSource dataSource;

    @Test
    void embDataBaseTest() {
        dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("shop_db.sql")
                .build();

        productRepository = new ProductRepositoryJdbcImpl(dataSource);

        final Product expectedProduct = new Product(123426L, "Protein 2.0", 5400.0, 26);
        final Optional<Product> optionalProduct = productRepository.findById(123426L);

        final Product actualProduct = optionalProduct.get();
        assertNotNull(actualProduct);
        assertEquals(expectedProduct, actualProduct);
    }
}