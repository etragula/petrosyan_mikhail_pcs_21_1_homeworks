package homework.service;

import homework.errors.UserNotFoundException;
import homework.model.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static homework.service.UsersStorage.getUser;
import static org.junit.jupiter.api.Assertions.*;

class UserStorageTest {

    private final UsersServiceImp usersServiceImp = new UsersServiceImp();

    @ParameterizedTest
    @CsvSource(value = {
            "132435@mail.ru:qwerty1234",
            "mail@yandex.ru:13pf3143"},
            delimiter = ':'
    )
    @DisplayName("Проверка добавления объекта в список.")
    void shouldAddUserToStorage(String eMail, String password) {
        usersServiceImp.signUp(eMail, password);
        User expected = new User(eMail, password.toCharArray());

        usersServiceImp.signIn(eMail, password);
        User actual = getUser(expected);

        assertNotNull(actual);
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Проверка выброса ошибки при отсутствии User.")
    void shouldThrowUserNotFoundException() {
        String eMail = "null@mail.ru";
        char[] password = "null134134".toCharArray();

        User user = new User(null, password);
        assertNull(getUser(user));

        user = new User(eMail, null);
        assertNull(getUser(user));

        User finalUser = new User(eMail, password);

        assertThrows(
                UserNotFoundException.class,
                () -> getUser(finalUser)
        );
    }
}
