package homework.service;

import homework.errors.BadEmailException;
import homework.errors.BadPasswordException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertThrows;

class UserServiceImpTest {

    private final UsersServiceImp usersServiceImp = new UsersServiceImp();

    @ParameterizedTest
    @NullSource
    @EmptySource
    @ValueSource(strings = {"132435mail.ru"})
    @DisplayName("Проверка выброса ошибки при невалидном e-mail.")
    void shouldThrowBadEmailException(String eMail) {
        assertThrows(
                BadEmailException.class,
                () -> usersServiceImp.signUp(eMail, "somePassword")
        );
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    @ValueSource(strings = {
            "1243",
            "qwetyyy",
            "11wqeqw  5678"
    })
    @DisplayName("Проверка выброса ошибки при невалидном пароле.")
    void shouldThrowBadPasswordException(String password) {
        assertThrows(
                BadPasswordException.class,
                () -> usersServiceImp.signUp("132435@mail.ru", password)
        );
    }
}
