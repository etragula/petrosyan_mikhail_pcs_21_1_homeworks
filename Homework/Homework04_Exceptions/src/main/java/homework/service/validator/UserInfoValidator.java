package homework.service.validator;

import homework.errors.BadEmailException;
import homework.errors.BadPasswordException;

public class UserInfoValidator {

    private UserInfoValidator() {
    }

    public static boolean validateEmail(String eMail) throws BadEmailException {
        if (eMail == null || !eMail.contains("@")) throw new BadEmailException("Неверный e-mail адрес!");
        return true;
    }

    public static boolean validatePassword(String password) throws BadPasswordException {
        if (password == null || password.toCharArray().length < 7) {
            throw new BadPasswordException("Пароль должен содержать не менее 7-ми символов!");
        }

        if (!hasDigit(password) || !hasLetter(password) || hasOtherChar(password)) {
            throw new BadPasswordException("Пароль должен содержать буквы и цифры!");
        }
        return true;
    }

    private static boolean hasOtherChar(String password) {
        for (char c : password.toCharArray()) {
            if (!Character.isDigit(c) && !Character.isAlphabetic(c)) {
                return true;
            }
        }
        return false;
    }

    private static boolean hasDigit(String password) {
        for (char c : password.toCharArray()) {
            if (Character.isDigit(c)) {
                return true;
            }
        }
        return false;
    }

    private static boolean hasLetter(String password) {
        for (char c : password.toCharArray()) {
            if (Character.isAlphabetic(c)) {
                return true;
            }
        }
        return false;
    }
}
