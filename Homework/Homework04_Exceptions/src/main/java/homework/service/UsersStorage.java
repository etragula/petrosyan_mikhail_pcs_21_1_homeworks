package homework.service;

import homework.errors.UserNotFoundException;
import homework.model.User;

import java.util.ArrayList;
import java.util.List;

public class UsersStorage {

    private static final List<User> USERS_STORAGE = new ArrayList<>();

    private UsersStorage() {
    }

    public static boolean addUser(User user) {
        return USERS_STORAGE.add(user);
    }

    public static User getUser(User user) throws UserNotFoundException {
        if (user.getPassword() == null || user.getEMail() == null) return null;

        for (User us : USERS_STORAGE) {
            if (user.getEMail().equals(us.getEMail()))
                return us;
        }
        throw new UserNotFoundException("Пользователь не был найден!");
    }
}
