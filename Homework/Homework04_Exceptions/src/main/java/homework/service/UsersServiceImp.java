package homework.service;

import homework.model.User;

import static homework.service.UsersStorage.addUser;
import static homework.service.UsersStorage.getUser;
import static homework.service.validator.UserInfoValidator.validateEmail;
import static homework.service.validator.UserInfoValidator.validatePassword;

public class UsersServiceImp implements UsersService {

    @Override
    public void signUp(String email, String password) {
        if (validateEmail(email) && validatePassword(password)) {
            addUser(new User(email, password.toCharArray()));
        }
    }

    @Override
    public void signIn(String email, String password) {
        User user = null;
        if (validateEmail(email) && validatePassword(password)) {
            user = getUser(new User(email, password.toCharArray()));
        }

        if (user != null) {
            doSomething(user);
        }
    }

    private void doSomething(User user) {
        System.out.println("Добро пожаловать!");
    }
}
