package homework.errors;

public class BadPasswordException extends RuntimeException {

    public BadPasswordException(String message) {
        super(message);
    }
}
