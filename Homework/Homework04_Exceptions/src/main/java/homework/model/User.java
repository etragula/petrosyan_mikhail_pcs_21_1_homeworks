package homework.model;

import lombok.Data;
import lombok.AllArgsConstructor;

@Data
@AllArgsConstructor
public class User {

    private String eMail;
    private char[] password;
}
