package ru.pcs.spring.security.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.spring.security.dto.AccountDto;
import ru.pcs.spring.security.models.Account;
import ru.pcs.spring.security.repositories.AccountsRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountsServiceImpl implements AccountsService {

    private final AccountsRepository accountsRepository;

    @Override
    public List<AccountDto> getAllAccounts() {
        return AccountDto.from(accountsRepository.findAccountsByState(Account.State.CONFIRMED));
    }

    @Override
    public void deleteAccount(Long accountId) {
        final Account account = accountsRepository.getById(accountId);
        account.setState(Account.State.DELETED);
        accountsRepository.save(account);
    }
}
