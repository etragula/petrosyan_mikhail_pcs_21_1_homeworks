package ru.pcs.spring.security.services;

import ru.pcs.spring.security.dto.AccountDto;

import java.util.List;

public interface AccountsService {

    List<AccountDto> getAllAccounts();

    void deleteAccount(Long accountId);
}
