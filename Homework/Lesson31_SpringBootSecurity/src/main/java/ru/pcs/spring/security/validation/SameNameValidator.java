package ru.pcs.spring.security.validation;

import ru.pcs.spring.security.dto.SignUpForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SameNameValidator implements ConstraintValidator<NotSameNames, SignUpForm> {

    @Override
    public boolean isValid(SignUpForm signUpForm, ConstraintValidatorContext constraintValidatorContext) {
        return !signUpForm.getFirstName().equals(signUpForm.getLastName());
    }
}
