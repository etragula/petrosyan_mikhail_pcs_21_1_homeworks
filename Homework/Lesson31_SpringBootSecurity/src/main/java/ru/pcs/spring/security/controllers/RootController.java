package ru.pcs.spring.security.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 03.11.2021
 * 38. Spring MVC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

@Controller
@RequestMapping("/")
public class RootController {

    @GetMapping
    public String getRootPage() {
        return "redirect:/profile";
    }

//    @GetMapping("/logout")
//    public String logOut() {
//        return "redirect:/signIn";
//    }

}
