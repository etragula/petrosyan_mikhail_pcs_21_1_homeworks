package homework.ru.patterns.handler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.nio.file.StandardOpenOption.APPEND;

public abstract class BaseCarHandler implements CarHandler {
    protected CarHandler next;

    @Override
    public void setNextHandler(CarHandler handler) {
        this.next = handler;
    }

    protected void nextHandler(String carInfo) {
        next.handle(carInfo);
    }

    protected String[] splitCarInfo(String carInfo) {
        return carInfo.replaceAll("]", " ")
                .replaceAll("\\[", "")
                .trim()
                .split(" ");
    }

    protected void writeInfoToFile(String filePath, String carInfo) {
        try {
            Files.write(Paths.get(filePath),(carInfo + "\n").getBytes(), APPEND);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
