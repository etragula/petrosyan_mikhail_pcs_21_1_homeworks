package homework.ru.patterns;

import homework.ru.patterns.handler.CarHandler;
import homework.ru.patterns.strategies.FileReaderStrategy;

public class CarProcessor {

    private final String filePath;
    private final CarHandler firstHandler;
    private final FileReaderStrategy readStrategy;

    public CarProcessor(String filePath, CarHandler firstHandler, FileReaderStrategy readStrategy) {
        this.filePath = filePath;
        this.firstHandler = firstHandler;
        this.readStrategy = readStrategy;
    }

    public void processCarInfo() {
        readStrategy.readFromFile(filePath).forEach(firstHandler::handle);
    }
}
