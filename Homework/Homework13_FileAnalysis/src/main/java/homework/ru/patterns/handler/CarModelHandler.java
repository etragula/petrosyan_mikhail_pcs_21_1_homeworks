package homework.ru.patterns.handler;

public class CarModelHandler extends BaseCarHandler {

    private static final String FILE = "Homework/Homework13_FileAnalysis/src/main/resources/carModels.txt";

    @Override
    public void handle(String carInfo) {

        String carModel = splitCarInfo(carInfo)[1];

        writeInfoToFile(FILE, carModel);
        if (next != null) {
            nextHandler(carInfo);
        }
    }
}
