package homework.ru.patterns.handler;

public class CarMileageHandler extends BaseCarHandler {

    private static final String FILE = "Homework/Homework13_FileAnalysis/src/main/resources/carMileages.txt";

    @Override
    public void handle(String carInfo) {

        String carMileage = splitCarInfo(carInfo)[3];

        writeInfoToFile(FILE, carMileage);

        if (next != null) {
            nextHandler(carInfo);
        }
    }
}
