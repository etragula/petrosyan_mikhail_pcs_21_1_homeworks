package homework.ru.patterns.handler;

public class CarPriceHandler extends BaseCarHandler {

    private static final String FILE = "Homework/Homework13_FileAnalysis/src/main/resources/carPrices.txt";

    @Override
    public void handle(String carInfo) {

        String carPrice = splitCarInfo(carInfo)[4];

        writeInfoToFile(FILE, carPrice);

        if (next != null) {
            nextHandler(carInfo);
        }
    }
}
