package homework.ru.patterns.strategies;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamFileReader implements FileReaderStrategy {
    @Override
    public List<String> readFromFile(String filePath) {
        try {
            Stream<String> lines = Files.lines(Paths.get(filePath));
            List<String> stringList = lines.collect(Collectors.toList());
            lines.close();
            return stringList;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
