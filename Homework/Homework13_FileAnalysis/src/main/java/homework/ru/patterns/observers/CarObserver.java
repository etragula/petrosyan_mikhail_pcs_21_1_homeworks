package homework.ru.patterns.observers;

public interface CarObserver {

    void update(String carInfo);
}
