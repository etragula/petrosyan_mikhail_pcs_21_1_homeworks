package homework.ru.patterns;

import homework.ru.patterns.handler.*;
import homework.ru.patterns.strategies.FileReaderStrategy;
import homework.ru.patterns.strategies.StdFileReader;
import homework.ru.patterns.strategies.StreamFileReader;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;

import static java.nio.file.StandardOpenOption.WRITE;

public class App {

    private static final String FILE = "Homework/Homework13_FileAnalysis/src/main/resources/info.txt";

    public static void main(String[] args) {
        cleanFiles();

        CarNumberHandler carNumberHandler = new CarNumberHandler();
        CarModelHandler carModelHandler = new CarModelHandler();
        CarColourHandler carColourHandler = new CarColourHandler();
        CarMileageHandler carMileageHandler = new CarMileageHandler();
        CarPriceHandler carPriceHandler = new CarPriceHandler();

        carNumberHandler.setNextHandler(carModelHandler);
        carModelHandler.setNextHandler(carColourHandler);
        carColourHandler.setNextHandler(carMileageHandler);
        carMileageHandler.setNextHandler(carPriceHandler);

        FileReaderStrategy readStrategy = new StdFileReader();
        StreamFileReader streamFileReader = new StreamFileReader();

        CarProcessor carProcessor = new CarProcessor(FILE, carNumberHandler, readStrategy);
        carProcessor.processCarInfo();

        CarProcessor carProcessor2 = new CarProcessor(FILE, carNumberHandler, streamFileReader);
        carProcessor2.processCarInfo();
    }

    private static class CarProcessor {
        private final String filePath;
        private final CarHandler firstHandler;
        private final FileReaderStrategy readStrategy;

        public CarProcessor(String filePath, CarHandler firstHandler, FileReaderStrategy readStrategy) {
            this.filePath = filePath;
            this.firstHandler = firstHandler;
            this.readStrategy = readStrategy;
        }

        public void processCarInfo() {
            readStrategy.readFromFile(filePath).forEach(firstHandler::handle);
        }
    }

    private static void cleanFiles() {
        cleanFile("Homework/Homework13_FileAnalysis/src/main/resources/forbiddenCars.txt");
        cleanFile("Homework/Homework13_FileAnalysis/src/main/resources/carPrices.txt");
        cleanFile("Homework/Homework13_FileAnalysis/src/main/resources/carColours.txt");
        cleanFile("Homework/Homework13_FileAnalysis/src/main/resources/carModels.txt");
        cleanFile("Homework/Homework13_FileAnalysis/src/main/resources/carNumbers.txt");
        cleanFile("Homework/Homework13_FileAnalysis/src/main/resources/carMileages.txt");
    }

    private static void cleanFile(String filePath) {
        try {
            FileChannel fileChannel = FileChannel.open(Paths.get(filePath), WRITE);
            fileChannel.truncate(0);
            fileChannel.close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
