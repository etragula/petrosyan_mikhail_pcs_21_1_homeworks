package homework.ru.patterns.observers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.nio.file.StandardOpenOption.APPEND;

public class CarNumberObserver implements CarObserver {

    private static final String FILE = "Homework/Homework13_FileAnalysis/src/main/resources/forbiddenCars.txt";

    @Override
    public void update(String carInfo) {
        try {
            Files.write(Paths.get(FILE), (carInfo + "\n").getBytes(), APPEND);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
