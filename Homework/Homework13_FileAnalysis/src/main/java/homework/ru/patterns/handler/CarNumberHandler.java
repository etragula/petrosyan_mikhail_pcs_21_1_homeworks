package homework.ru.patterns.handler;

import homework.ru.patterns.observers.CarNumberObserver;
import homework.ru.patterns.observers.StolenCarObserver;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CarNumberHandler extends BaseCarHandler {

    private final StolenCarObserver stolenCarObserver = new StolenCarObserver();
    private final CarNumberObserver carNumberObserver = new CarNumberObserver();

    private static final String FILE = "Homework/Homework13_FileAnalysis/src/main/resources/carNumbers.txt";
    private static final String FILE_STOLEN_CARS = "Homework/Homework13_FileAnalysis/src/main/resources/stolenCars.txt";

    @Override
    public void handle(String carInfo) {
        String carNumber = splitCarInfo(carInfo)[0];

        if (isNumberValid(carNumber) && !isCarStolen(carNumber)) {
            writeInfoToFile(FILE, carNumber);
            if (next != null) nextHandler(carInfo);
        }
    }

    private boolean isCarStolen(String carNumber) {
        try {
            if (Files.readAllLines(Paths.get(FILE_STOLEN_CARS)).stream().anyMatch(s -> s.equalsIgnoreCase(carNumber))) {
                stolenCarObserver.update(carNumber);
                return true;
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return false;
    }

    private boolean isNumberValid(String carNumber) {
        if (!carNumber.matches("^[а-я]{1}[0-9]{3}[а-я]{2}[0-9]{2}$")) {
            carNumberObserver.update(carNumber);
            return false;
        }
        return true;
    }
}
