package homework.ru.patterns.strategies;

import java.util.List;

public interface FileReaderStrategy {
    List<String> readFromFile(String filePath);
}
