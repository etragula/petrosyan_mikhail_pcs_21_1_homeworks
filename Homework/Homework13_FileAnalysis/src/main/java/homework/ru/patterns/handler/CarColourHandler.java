package homework.ru.patterns.handler;

public class CarColourHandler extends BaseCarHandler {

    private static final String FILE = "Homework/Homework13_FileAnalysis/src/main/resources/carColours.txt";

    @Override
    public void handle(String carInfo) {

        String carColour = splitCarInfo(carInfo)[2];

        writeInfoToFile(FILE, carColour);
        if (next != null) {
            nextHandler(carInfo);
        }
    }
}
