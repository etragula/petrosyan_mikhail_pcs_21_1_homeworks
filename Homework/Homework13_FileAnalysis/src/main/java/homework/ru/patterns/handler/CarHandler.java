package homework.ru.patterns.handler;

public interface CarHandler {

    void setNextHandler(CarHandler handler);
    void handle(String carInfo);
}
