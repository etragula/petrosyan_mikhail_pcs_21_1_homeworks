package homework.ru.patterns.observers;

public class StolenCarObserver implements CarObserver {
    @Override
    public void update(String carInfo) {
        System.out.println("Была найдена угнанная машина с номерами - " + carInfo);
    }
}
