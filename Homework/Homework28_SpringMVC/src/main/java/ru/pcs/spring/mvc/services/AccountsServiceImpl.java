package ru.pcs.spring.mvc.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.spring.mvc.dto.AccountDto;
import ru.pcs.spring.mvc.repositories.AccountsRepository;

import java.util.List;

import static ru.pcs.spring.mvc.dto.AccountDto.from;

/**
 * 18.10.2021
 * 30. Java Web Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
@RequiredArgsConstructor
public class AccountsServiceImpl implements AccountsService {

    private final AccountsRepository accountsRepository;

    @Override
    public List<AccountDto> getAll() {
        return from(accountsRepository.findAll());
    }
}
