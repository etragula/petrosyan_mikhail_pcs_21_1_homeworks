package homework.reflection;

import homework.reflection.annotations.Max;
import homework.reflection.annotations.Min;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidatorTest3 {

    @Test
    void validateNotEmpty() {
        Validator validator = new Validator();

        assertThrows(
                IllegalArgumentException.class,
                () -> validator.validate(new User("Name", 66))
        );
        assertThrows(
                IllegalArgumentException.class,
                () -> validator.validate(new User("Name", 13))
        );
        validator.validate(new User("Man", 65));
    }

    private static class User {
        private String name;

        @Min(min = 14)
        @Max(max = 65)
        private int age;

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }
    }
}