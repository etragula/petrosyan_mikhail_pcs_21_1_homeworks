package homework.reflection;

import homework.reflection.annotations.MinMaxLength;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidatorTest2 {

    @Test
    void validateMinMaxLength() {
        Validator validator = new Validator();

        assertThrows(
                IllegalArgumentException.class,
                () -> validator.validate(new User("Na", 12))
        );
        assertThrows(
                IllegalArgumentException.class,
                () -> validator.validate(new User("NameName", 12))
        );
        validator.validate(new User("Man", 12));
    }

    private static class User {
        @MinMaxLength(min = 3, max = 7)
        private String name;

        private int age;

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }
    }
}