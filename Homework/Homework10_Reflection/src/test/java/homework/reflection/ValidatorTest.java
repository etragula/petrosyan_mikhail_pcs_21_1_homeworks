package homework.reflection;

import homework.reflection.annotations.NotEmpty;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidatorTest {

    @Test
    void validateNotEmpty() {
        Validator validator = new Validator();

        assertThrows(
                IllegalArgumentException.class,
                () -> validator.validate(new User("", 12))
        );
        validator.validate(new User("Man", 12));
    }

    private static class User {
        @NotEmpty
        private String name;

        private int age;

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }
    }
}