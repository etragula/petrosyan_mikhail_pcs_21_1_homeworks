package lesson.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args) throws NoSuchMethodException {
        Class<User> userClass = User.class;

        Field[] fields = userClass.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field.getType());
            System.out.println(field.getName());
        }

        System.out.println("------------------------------");

        Method[] methods = userClass.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method.getName());
            System.out.println(method.getReturnType());
        }

        System.out.println("------------------------------");

        Constructor<User>[] userConstructor = (Constructor<User>[]) userClass.getConstructors();

        for (Constructor<User> constructor : userConstructor) {
            System.out.println(constructor.getParameterTypes().length);
        }
    }
}
