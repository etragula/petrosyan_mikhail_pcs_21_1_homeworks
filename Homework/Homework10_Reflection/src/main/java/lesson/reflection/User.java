package lesson.reflection;

public class User {

    private String name;
    private int stepsCount;

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public void say() {
        System.out.println(name);
    }

    public int go(int steps) {
        this.stepsCount += steps;
        return this.stepsCount;
    }

    public String getName() {
        return name;
    }
}
