package lesson.framework;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        DocumentsFramework documentsFramework = new DocumentsFramework();
        Statement statement = documentsFramework.generate(Statement.class, "name", LocalDate.of(1961, 12, 20));
        Act act = documentsFramework.generate(Act.class, LocalDate.now(), "sadas", "asdasd");

        System.out.println(statement);
        System.out.println(act);


    }
}
