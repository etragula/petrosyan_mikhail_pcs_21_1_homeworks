package lesson.framework;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class DocumentsFramework {
    public <T> T generate(Class<T> tClass, Object... args) {
        List<Class<?>> argsTypes = new ArrayList<>();

        for (Object arg : args) {
            argsTypes.add(arg.getClass());
        }

        Class<?>[] argsTypesArray = new Class[argsTypes.size()];
        argsTypes.toArray(argsTypesArray);

        try {
            Constructor<T> constructor = tClass.getConstructor(argsTypesArray);
            T instance = constructor.newInstance(args);
            processDefualtValues(instance);
            return instance;
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private <T> void processDefualtValues(T doc) {
        Class<T> tClass = (Class<T>) doc.getClass();
        Field[] declaredFields = tClass.getDeclaredFields();

        for (Field declaredField : declaredFields) {
            DefaultValue defaultValue = declaredField.getAnnotation(DefaultValue.class);

            if (defaultValue != null) {
                String value = defaultValue.value();

                try {
                    declaredField.setAccessible(true);
                    declaredField.set(doc, value);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }
}
