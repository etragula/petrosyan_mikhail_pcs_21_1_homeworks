package lesson.framework;

import java.time.LocalDate;

public class Statement {

    private String name;
    private LocalDate birthDay;

    @DefaultValue("345235342")
    private String inn;
    @DefaultValue("Taxi")
    private String company;

    public Statement(String name) {
        this.name = name;
    }

    public Statement(String name, LocalDate birthDay) {
        this.name = name;
        this.birthDay = birthDay;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "Statement{" +
                "name='" + name + '\'' +
                ", birthDay=" + birthDay +
                ", inn='" + inn + '\'' +
                ", company='" + company + '\'' +
                '}';
    }
}
