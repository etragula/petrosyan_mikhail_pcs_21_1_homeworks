package homework.reflection;

import homework.reflection.annotations.Max;
import homework.reflection.annotations.Min;
import homework.reflection.annotations.MinMaxLength;
import homework.reflection.annotations.NotEmpty;

public class App {

    public static void main(String[] args) throws IllegalAccessException {
        Validator validator = new Validator();

        validator.validate(new User("Mike", 65));
    }

    private static class User {
        @NotEmpty
        @MinMaxLength(min = 3, max = 7)
        private String name;

        @Min(min = 14)
        @Max(max = 65)
        private int age;

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }
    }
}
