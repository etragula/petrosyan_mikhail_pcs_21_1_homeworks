package homework.reflection;

import homework.reflection.annotations.Max;
import homework.reflection.annotations.Min;
import homework.reflection.annotations.MinMaxLength;
import homework.reflection.annotations.NotEmpty;

import java.lang.reflect.Field;

public class Validator {

    public void validate(Object form) {
        Class<?> formClass = form.getClass();
        Field[] fields = formClass.getDeclaredFields();

        for (Field field : fields) {
            validateNotEmpty(field, form);
            validateLength(field, form);
            validateMax(field, form);
            validateMin(field, form);
        }
    }

    private void validateLength(Field field, Object form) {
        try {
            MinMaxLength length = field.getAnnotation(MinMaxLength.class);
            if (length != null) {
                int max = length.max();
                int min = length.min();
                field.setAccessible(true);

                String value = (String) field.get(form);
                if (value.length() < min || value.length() > max) throw new IllegalArgumentException();
            }
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void validateNotEmpty(Field field, Object form) {
        try {
            NotEmpty notEmpty = field.getAnnotation(NotEmpty.class);
            if (notEmpty != null) {
                field.setAccessible(true);
                String value = (String) field.get(form);
                if (value.isEmpty()) throw new IllegalArgumentException();
            }
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void validateMax(Field field, Object form) {
        try {
            Max max = field.getAnnotation(Max.class);
            if (max != null) {
                field.setAccessible(true);
                int annotationValue = max.max();
                int objectValue = (int) field.get(form);
                if (objectValue > annotationValue) throw new IllegalArgumentException();
            }
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void validateMin(Field field, Object form) {
        try {
            Min min = field.getAnnotation(Min.class);
            if (min != null) {
                field.setAccessible(true);
                int annotationValue = min.min();
                int objectValue = (int) field.get(form);
                if (objectValue < annotationValue) throw new IllegalArgumentException();
            }
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
