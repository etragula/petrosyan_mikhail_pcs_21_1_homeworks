package ru.pcs.jpa.hw;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.jpa.hw.models.File;
import ru.pcs.jpa.hw.repository.FileRepository;
import ru.pcs.jpa.hw.repository.FileRepositoryJPA;

import javax.persistence.EntityManager;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        EntityManager entityManager = sessionFactory.createEntityManager();
        FileRepository fileRepository = new FileRepositoryJPA(entityManager);

        final File file = File.builder()
                .originalFileName("test")
                .description("some test for file")
                .storageFileName("name")
                .size(1354134L)
                .mimeType("text")
                .build();

        fileRepository.save(file);
        final Optional<File> optionalFile = fileRepository.findByStorageName(file.getStorageFileName());
        System.out.println(optionalFile.get());
    }
}
