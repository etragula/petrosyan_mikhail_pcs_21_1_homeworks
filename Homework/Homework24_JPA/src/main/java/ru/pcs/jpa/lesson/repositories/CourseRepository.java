package ru.pcs.jpa.lesson.repositories;

import ru.pcs.jpa.lesson.models.Course;

import java.util.List;

public interface CourseRepository {
    void save(Course course);
    List<Course> findAllByLesson_name(String name);
}
