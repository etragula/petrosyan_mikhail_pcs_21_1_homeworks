package ru.pcs.jpa.lesson.app;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.jpa.lesson.models.Course;
import ru.pcs.jpa.lesson.models.Lesson;
import ru.pcs.jpa.lesson.repositories.CourseRepository;
import ru.pcs.jpa.lesson.repositories.CourseRepositoryJPA;
import ru.pcs.jpa.lesson.repositories.LessonRepository;
import ru.pcs.jpa.lesson.repositories.LessonRepositoryJPA;

import javax.persistence.EntityManager;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        EntityManager entityManager = sessionFactory.createEntityManager();
        CourseRepository courseRepository = new CourseRepositoryJPA(entityManager);
        LessonRepository lessonRepository = new LessonRepositoryJPA(entityManager);

        Lesson spring = Lesson.builder()
                .name("Spring")
                .build();
        Lesson jvm = Lesson.builder()
                .name("JVM")
                .build();
        Lesson jdbc = Lesson.builder()
                .name("JDBC")
                .build();

        Course java = Course.builder()
                .title("Java")
                .lessons(Arrays.asList(spring, jvm))
                .build();

        Course sql = Course.builder()
                .title("SQL")
                .lessons(Arrays.asList(jvm, jdbc))
                .build();
//
//        Lesson select = Lesson.builder()
//                .name("select")
//                .build();
//
//        Lesson update = Lesson.builder()
//                .name("update")
//                .build();
//
//        Lesson indexes = Lesson.builder()
//                .name("indexes")
//                .build();

        lessonRepository.save(spring);
        lessonRepository.save(jvm);
        lessonRepository.save(jdbc);
        courseRepository.save(java);
        courseRepository.save(sql);

        System.out.println(courseRepository.findAllByLesson_name("spring "));
    }
}
