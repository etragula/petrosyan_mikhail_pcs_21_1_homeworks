package ru.pcs.jpa.hw.repository;

import ru.pcs.jpa.hw.models.File;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.Optional;

public class FileRepositoryJPA implements FileRepository {

    private EntityManager entityManager;

    public FileRepositoryJPA(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(File file) {
        final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(file);
        transaction.commit();
    }

    @Override
    public Optional<File> findByStorageName(String storageFileName) {
        TypedQuery<File> query = entityManager.createQuery(
                "select file from File file where storageFileName=:storageFileName",
                File.class
        );
        query.setParameter("storageFileName", storageFileName);
        return Optional.of(query.getSingleResult());
    }
}
