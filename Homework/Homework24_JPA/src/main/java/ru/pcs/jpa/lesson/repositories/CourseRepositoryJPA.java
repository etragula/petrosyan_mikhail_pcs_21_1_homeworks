package ru.pcs.jpa.lesson.repositories;

import ru.pcs.jpa.lesson.models.Course;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.List;

public class CourseRepositoryJPA implements CourseRepository {

    private final EntityManager entityManager;

    public CourseRepositoryJPA(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(Course course) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(course);
        transaction.commit();
    }

    @Override
    public List<Course> findAllByLesson_name(String name) {
        TypedQuery<Course> query = entityManager.createQuery(
                "select course from Course course left join course.lessons lesson where lesson.name= :name",
                Course.class);
        query.setParameter("name", name);
        return query.getResultList();
    }
}
