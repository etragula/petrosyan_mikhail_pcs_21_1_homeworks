package ru.pcs.jpa.hw.repository;

import ru.pcs.jpa.hw.models.File;

import java.util.Optional;

public interface FileRepository {
    void save(File file);

    Optional<File> findByStorageName(String storageFileName);
}