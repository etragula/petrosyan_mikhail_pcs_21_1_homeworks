package ru.pcs.jpa.lesson.repositories;

import ru.pcs.jpa.lesson.models.Lesson;

public interface LessonRepository {
    void save(Lesson lesson);
}
