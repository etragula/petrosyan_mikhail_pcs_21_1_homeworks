package ru.pcs.jpa.lesson.repositories;

import ru.pcs.jpa.lesson.models.Lesson;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class LessonRepositoryJPA implements LessonRepository {

    private EntityManager entityManager;

    public LessonRepositoryJPA(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(Lesson lesson) {
        final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(lesson);
        transaction.commit();
    }
}
